import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

def resolve_action_game_course_haie(position, piste_course):
    # RG : 
    #     -si les 4 prochaines cases sont vides ('....') 
    #      alors RIGHT
    #     -sinon si le prochain obstacle est dans les 3 prochaines cases  
    #       alors LEFT
    #     -sinon si le prochain obstacle est dans les 2 prochaines cases
    #       alors UP
    if piste_course != 'GAME_OVER' :
        if '#' in piste_course[position:position+2] :
            print(f"Debug messages... {piste_course[position:position+2]}", file=sys.stderr, flush=True)
            action = "UP"
        elif '#' in piste_course[position:position+3] :
            print(f"Debug messages... {piste_course[position:position+3]}", file=sys.stderr, flush=True)
            action = "LEFT"
        elif '#' in piste_course[position:position+4] :
            print(f"Debug messages... {piste_course[position:position+4]}", file=sys.stderr, flush=True)
            action = "DOWN"
        else :
            print(f"Debug messages... {piste_course[position:position+5]}", file=sys.stderr, flush=True)
            action = "RIGHT"
    else : 
       action = "UP" 
    
    return action

player_idx = int(input())
nb_games = int(input())

# game loop
while True:
    for i in range(3):
        score_info = input()
    for i in range(nb_games):
        inputs = input().split()
        gpu = inputs[0]
        reg_0 = int(inputs[1])
        reg_1 = int(inputs[2])
        reg_2 = int(inputs[3])
        reg_3 = int(inputs[4])
        reg_4 = int(inputs[5])
        reg_5 = int(inputs[6])
        reg_6 = int(inputs[7])

    # LOG
    print(f"Debug messages... {gpu}", file=sys.stderr, flush=True)

    if player_idx == 0 :
        reg = reg_0
    elif player_idx == 1 :
        reg = reg_1
    else : 
        reg = reg_2    
    print(f"Debug messages... {reg}", file=sys.stderr, flush=True)
    

    action = resolve_action_game_course_haie(reg,gpu)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr, flush=True)
    print(action)
