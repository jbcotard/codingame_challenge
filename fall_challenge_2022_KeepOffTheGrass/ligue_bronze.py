import sys
import math
from dataclasses import dataclass
from random import randint
import random

ME = 1
OPP = 0
NONE = -1


@dataclass
class Point:
    x: int
    y: int


@dataclass
class Tile:
    x: int
    y: int
    scrap_amount: int
    owner: int
    units: int
    recycler: bool
    can_build: bool
    can_spawn: bool
    in_range_of_recycler: bool


@dataclass
class Zone:
    point_no: Point
    point_ne: Point
    point_so: Point
    point_se: Point
    #list_points: list[Point] 
    list_tiles: list[Tile]
    statut: int = 0
    targeted: int = 0
    nb_opp_units: int = 0
    nb_my_units: int = 0
    nb_opp_tiles: int = 0
    nb_my_tiles: int = 0
    nb_recycled_tiles: int = 0
    nb_neutral_tiles: int = 0


#def _is_spawnest(tile, tiles):

def _dist_from_m(tile, width, height):
    return math.dist((tile.x,tile.y),(width//2, height//2))

def _dist_from_tile(tile1, tile2):
    return math.dist((tile1.x,tile1.y),(tile2.x,tile2.y))

def _is_nearest_from_m(tile1, tile2, width, height):
    return _dist_from_m(tile1, width, height) < _dist_from_m(tile2, width, height) 

def _is_farest(tile1, tile2, tile):
    return _dist_from_tile(tile1, tile) > _dist_from_tile(tile2, tile) 

def _is_nearest(tile1, tile2, tile):
    return _dist_from_tile(tile1, tile) < _dist_from_tile(tile2, tile) 

def get_nearest_oppo_tile_from_middle(opp_tiles, width, height):
    nearest_tile = None
    for tile in opp_tiles:
        if not nearest_tile or _is_nearest_from_m(tile, nearest_tile, width, height):
            nearest_tile = tile
    return nearest_tile

def get_nearest_oppo_tile_from_tile(opp_tiles, a_tile):
    nearest_tile = None
    for tile in opp_tiles:
        if not nearest_tile or _is_nearest(tile, nearest_tile, a_tile):
            nearest_tile = tile
    return nearest_tile

def get_nearest_neutral_tile_from_tile(neutral_tiles, a_tile, targeted_tiles):
    nearest_tile = None
    for tile in neutral_tiles:
        if tile.scrap_amount > 0 and not tile in targeted_tiles  and (not nearest_tile or _is_nearest(tile, nearest_tile, a_tile)):
            nearest_tile = tile
    return nearest_tile

def get_farest_oppo_tile_from_tile(opp_tiles, a_tile):
    nearest_tile = None
    for tile in opp_tiles:
        if not nearest_tile or _is_farest(tile, nearest_tile, a_tile):
            nearest_tile = tile
    return nearest_tile

def identify_zone(tiles):
    my_point_no=Point(tiles[0].x,tiles[0].y)
    my_point_ne=Point(tiles[0].x,tiles[0].y)
    my_point_so=Point(tiles[0].x,tiles[0].y)
    my_point_se=Point(tiles[0].x,tiles[0].y)
    for tile in tiles: 
        if tile.x <= my_point_no.x and  tile.y <= my_point_no.y:
            my_point_no=Point(tile.x,tile.y)
        elif tile.x >= my_point_ne.x and  tile.y <= my_point_ne.y:
            my_point_ne=Point(tile.x,tile.y)
        elif tile.x <= my_point_so.x and  tile.y >= my_point_so.y:
            my_point_so=Point(tile.x,tile.y)
        elif tile.x >= my_point_se.x and  tile.y >= my_point_se.y:
            my_point_se=Point(tile.x,tile.y)    
    return Zone(point_no=my_point_no,point_ne=my_point_ne,point_so=my_point_so,point_se=my_point_se, list_tiles=[])

def init_map(width, height):
    map = []
    current_zone = None
    for j in range(height):
        for i in range(width):
            if i%4 == 0 and j%4 == 0:
                current_zone = Zone(point_no=Point(i,j),point_ne=Point(i+3,j),point_so=Point(i,j+3),point_se=Point(i+3,j+3), list_tiles=[])
                map.append(current_zone)
            #current_zone.list_points.append(Point(i,j))
    return map

def reset_map(map): 
    for zone in map:
        zone.targeted = 0
        zone.statut = 0
        zone.nb_opp_units = 0
        zone.nb_opp_tiles = 0
        zone.nb_neutral_tiles = 0
        zone.nb_recycled_tiles = 0
        zone.nb_my_units = 0
        zone.nb_my_tiles = 0
        zone.list_tiles = []
    return map

def update_map(map, tile):
    for zone in map:
        if zone.point_no.x <= tile.x \
        and tile.x <= zone.point_se.x \
        and zone.point_no.y <= tile.y \
        and tile.y <= zone.point_se.y:
            #if zone.point_no.x == 0 and zone.point_no.y == 0:
            #    print(f"Debug : zone ({zone.point_no.x},{zone.point_no.y})->({zone.point_se.x},{zone.point_se.y})", file=sys.stderr, flush=True)
            #    print(f"Debug : tile ({tile.x},{tile.y})={tile.owner}/{tile.scrap_amount}/{tile.units}", file=sys.stderr, flush=True)
            zone.list_tiles.append(tile)
            if tile.owner == NONE: 
                zone.statut += tile.scrap_amount
                zone.nb_neutral_tiles += 1
                zone.nb_recycled_tiles += 1 if tile.scrap_amount > 0 else 0
            elif tile.owner == OPP:
                zone.statut -= 1 + (1 * tile.units)
                zone.nb_opp_units += tile.units
                zone.nb_opp_tiles += 1
            else:
                zone.nb_my_units += tile.units
                zone.nb_my_tiles += 1



def get_statut_map(zone):
    return zone.statut


def get_best_list_neutral_tile_from_map(map):
    zones = list(map)
    #zones.sort(key=get_statut_map, reverse=True) 
    zones.sort(key=lambda x: (x.statut, x.nb_opp_units))
    best_zones = [zone for zone in zones if zone.targeted == 0]
    print(f"Debug : best_zones size = {len(best_zones)}", file=sys.stderr, flush=True)
    return best_zones[0].list_tiles if len(best_zones) > 0 else []


def get_best_list_opp_tile_from_map(map):
    best_zones = [zone for zone in map if zone.targeted == 0 and zone.nb_opp_units == 0 and zone.statut > 0]
    return best_zones[0].list_tiles if len(best_zones) > 0 else []

def my_tile_is_in_bests_zones(my_tile, map):
    zones = list(map)
    #zones.sort(key=get_statut_map, reverse=True) 
    zones.sort(key=lambda x: (x.statut, x.nb_opp_units))
    my_point = Point(my_tile.x, my_tile.y)
    return True if my_point in zones[0].list_tiles or my_point in zones[1].list_tiles or my_point in zones[2].list_tiles else False

                
def target_zone_in_map(map, tile):
    for zone in map:
        if zone.point_no.x <= tile.x \
        and tile.x <= zone.point_se.x \
        and zone.point_no.y <= tile.y \
        and tile.y <= zone.point_se.y:
            zone.targeted = 1


def _display_map(map):
    # DEBUG MAP
    msg_log = "["
    y_prec = 0
    for zone in map: 
        if y_prec != zone.point_no.y:
            msg_log += "]"
            print(f"{msg_log}", file=sys.stderr, flush=True)
            msg_log = "["
            y_prec = zone.point_no.y
        msg_log += f"({zone.point_no.x},{zone.point_no.y}):{zone.targeted}-{is_current_zone_undercontrol(zone)},"
    msg_log += "]"
    print(f"{msg_log}", file=sys.stderr, flush=True)
    print(f"Debug : map size = {len(map)}", file=sys.stderr, flush=True)

def get_tile_with_point_from_tiles(x, y, tiles):
    tile_found = None
    for tile in tiles:
        if tile.x == x and tile.y == y:
            tile_found = tile
            break
    return tile_found

def get_nearest_list_neutral_tile_from_map(tile, map):
    min_dist = -1
    min_zone = None
    for zone in map:
        dist =  _dist_from_tile(tile, zone.point_no)
        if zone.targeted == 0 and (min_dist == -1 or dist < min_dist):
            min_zone = zone
            min_dist = dist
    #if min_zone:
    #    print(f"Debug : min zone ({min_zone.point_no.x},{min_zone.point_no.y}) : {min_zone.targeted}", file=sys.stderr, flush=True) 
    return min_zone.list_tiles if min_zone else []
            

def is_tile_blocked_in_left(my_tile, tiles):
    return my_tile.x == 0 or \
    get_tile_with_point_from_tiles(my_tile.x-1, my_tile.y, tiles).scrap_amount == 0 or \
    (my_tile.x > 1 and get_tile_with_point_from_tiles(my_tile.x-2, my_tile.y, tiles).scrap_amount == 0) 

def is_tile_blocked_in_right(my_tile, tiles):
    return my_tile.x == width-1 or \
    get_tile_with_point_from_tiles(my_tile.x+1, my_tile.y, tiles).scrap_amount == 0 or \
    (my_tile.x < width-2 and get_tile_with_point_from_tiles(my_tile.x+2, my_tile.y, tiles).scrap_amount == 0) 

def is_tile_blocked_in_up(my_tile, tiles):
    return my_tile.y == 0 or \
    get_tile_with_point_from_tiles(my_tile.x, my_tile.y-1, tiles).scrap_amount == 0 or \
    (my_tile.y > 1 and get_tile_with_point_from_tiles(my_tile.x, my_tile.y-2, tiles).scrap_amount == 0) 

def is_tile_blocked_in_down(my_tile, tiles):
    return my_tile.y == height-1 or \
    get_tile_with_point_from_tiles(my_tile.x, my_tile.y+1, tiles).scrap_amount == 0 or \
    (my_tile.y < height-2 and get_tile_with_point_from_tiles(my_tile.x, my_tile.y+2, tiles).scrap_amount == 0) 

def is_tile_blocked(my_tile, tiles):
    return is_tile_blocked_in_left(my_tile, tiles) \
    and is_tile_blocked_in_right(my_tile, tiles) \
    and is_tile_blocked_in_up(my_tile, tiles) \
    and is_tile_blocked_in_down(my_tile, tiles)

def is_colon_blocked(x, tiles):
    is_blocked = True
    for tile in tiles:
        if tile.x == x and (tile.owner != NONE or tile.scrap_amount > 0):
            is_blocked = False
    return is_blocked

def is_line_blocked(y, tiles):
    is_blocked = True
    for tile in tiles:
        if tile.y == y and (tile.owner != NONE or tile.scrap_amount > 0):
            is_blocked = False
    return is_blocked

def is_bloc_blocked(my_tile, tiles):
    return (my_tile.y == 0 and is_line_blocked(y+2, tiles)) \
        or (my_tile.y == height-1 and is_line_blocked(y-2, tiles)) \
        or (my_tile.x == 0 and is_colon_blocked(x+2, tiles)) \
        or (my_tile.x == width-1 and is_colon_blocked(x-2, tiles))

def get_current_zone(tile, map):
    for zone in map:
        if zone.point_no.x <= tile.x \
        and tile.x <= zone.point_se.x \
        and zone.point_no.y <= tile.y \
        and tile.y <= zone.point_se.y:
            return zone

def get_next_zone(tile, map):
    min_dist = -1
    min_zone = None
    for zone in map:
        dist = _dist_from_tile(tile, Point(zone.point_no.x+2,zone.point_no.y+2))
        if (min_dist == -1 or dist < min_dist) and is_current_zone_interesting(zone):
            min_zone = zone
            min_dist = dist
    return min_zone

def is_current_zone_undercontrol(current_zone):
    # 80%
    return (16 - current_zone.nb_my_tiles - current_zone.nb_recycled_tiles) <= 4

def is_current_zone_interesting(current_zone):
    return current_zone.nb_opp_units == 0 and current_zone.nb_recycled_tiles <= 4

def get_tiles_current_zone_if_not_undercontrol(tile, map):
    current_zone = get_current_zone(tile, map)
    return [t for t in current_zone.list_tiles if t.owner != ME and t.scrap_amount > 0] if not is_current_zone_undercontrol(current_zone) and is_current_zone_interesting(current_zone) else []

def get_tiles_next_zone_if_not_undercontrol(tile, map):
    next_zone = get_next_zone(tile, map)
    return [t for t in next_zone.list_tiles if t.owner != ME and t.scrap_amount > 0] if  next_zone and not is_current_zone_undercontrol(next_zone)  else []

def is_targeted_tile(tile, list_targeted_tiles):
    is_targeted = False
    for targeted_tile in list_targeted_tiles:
        if tile.x == targeted_tile.x and tile.y == targeted_tile.y:
            is_targeted = True
    return is_targeted

def get_tile(tiles, x, y):
    tile_found = None
    for tile in tiles:
        if tile.x == x and tile.y == y:
            tile_found = tile
            break 
    return tile_found

def get_tile_next_side_can_controlled(tile, tiles, width, height):
    tile_next_side = None
    msg = ""
    if tile.y > 0:
        tile_next_up = get_tile(tiles, tile.x, tile.y-1)
        if not tile_next_side and tile_next_up and \
        ((tile_next_up.owner == NONE and tile_next_up.scrap_amount > 0) or (tile_next_up.owner == OPP and tile_next_up.units == 0)):
            tile_next_side = tile_next_up
            msg = "up"
    
    if tile.y < height-1:
        tile_next_down = get_tile(tiles, tile.x, tile.y+1)
        if (not tile_next_side and tile_next_down and \
        ((tile_next_down.owner == NONE and tile_next_down.scrap_amount > 0) or (tile_next_down.owner == OPP and tile_next_down.units == 0))) \
        or (tile_next_side and tile_next_down and tile_next_down.owner == OPP and tile_next_down.units == 0 and tile_next_side.owner == NONE ):
            tile_next_side = tile_next_down
            msg = "down"

    if tile.x > 0:
        tile_next_left = get_tile(tiles, tile.x-1, tile.y)
        if (not tile_next_side and tile_next_left and \
        ((tile_next_left.owner == NONE and tile_next_left.scrap_amount > 0) or (tile_next_left.owner == OPP and tile_next_left.units == 0))) \
        or (tile_next_side and tile_next_left and tile_next_left.owner == OPP and tile_next_left.units == 0 and tile_next_side.owner == NONE ):
            tile_next_side = tile_next_left
            msg = "left"

    if tile.x < width-1:
        tile_next_right = get_tile(tiles, tile.x+1, tile.y)
        if (not tile_next_side and tile_next_right and \
        ((tile_next_right.owner == NONE and tile_next_right.scrap_amount > 0) or (tile_next_right.owner == OPP and tile_next_right.units == 0))) \
        or (tile_next_side and tile_next_right and tile_next_right.owner == OPP and tile_next_right.units == 0 and tile_next_side.owner == NONE ):
            tile_next_side = tile_next_right
            msg = "right"

    print(f"Debug : tile=({tile.x},{tile.y}) next_side {msg} =({tile_next_side}) ", file=sys.stderr, flush=True)
    return tile_next_side

###############################################################################################

width, height = [int(i) for i in input().split()]
map = init_map(width, height)

print(f"Debug : map width={width}, height={height}", file=sys.stderr, flush=True)

# game loop
num_step = 0
is_protected_my_zone = False
my_origin_zone = None
opp_origin_zone = None
while True:
    tiles = []
    my_units = []
    opp_units = []
    my_recyclers = []
    opp_recyclers = []
    opp_tiles = []
    my_tiles = []
    neutral_tiles = []

    map = reset_map(map)

    my_matter, opp_matter = [int(i) for i in input().split()]
    for y in range(height):
        for x in range(width):
            # owner: 1 = me, 0 = foe, -1 = neutral
            # recycler, can_build, can_spawn, in_range_of_recycler: 1 = True, 0 = False
            scrap_amount, owner, units, recycler, can_build, can_spawn, in_range_of_recycler = [int(k) for k in input().split()]
            tile = Tile(x, y, scrap_amount, owner, units, recycler == 1, can_build == 1, can_spawn == 1, in_range_of_recycler == 1)

            tiles.append(tile)

            update_map(map, tile)

            if tile.owner == ME:
                my_tiles.append(tile)
                if tile.units > 0:
                    my_units.append(tile)
                elif tile.recycler:
                    my_recyclers.append(tile)
            elif tile.owner == OPP:
                opp_tiles.append(tile)
                if tile.units > 0:
                    opp_units.append(tile)
                elif tile.recycler:
                    opp_recyclers.append(tile)
            else:
                neutral_tiles.append(tile)



    actions = []

    targeted_tiles = []


    # DEBUG MAP
    print(f"Debug : before decision", file=sys.stderr, flush=True)
    _display_map(map)



    if num_step == 0:
        my_origin_zone = identify_zone(my_tiles)
        opp_origin_zone = identify_zone(my_tiles)
        my_borne_zone = 0
        first_step = False
        #actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, opp_units[0].x, opp_units[0].y))
        #actions.append('MOVE {} {} {} {} {}'.format(1, my_units[1].x, my_units[1].y, opp_units[1].x, opp_units[1].y))
        #actions.append('MOVE {} {} {} {} {}'.format(1, my_units[2].x, my_units[2].y, opp_units[2].x, opp_units[2].y))
        #actions.append('MOVE {} {} {} {} {}'.format(1, my_units[3].x, my_units[3].y, opp_units[3].x, opp_units[3].y))
        
        actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, 1, 1))
        if len(my_units) > 1: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[1].x, my_units[1].y, width-2, 1))
        if len(my_units) > 2: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[2].x, my_units[2].y, 1, height-2))
        if len(my_units) > 3: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[3].x, my_units[3].y, width-2, height-2))

    elif num_step == 1:
        actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, 1, 1))
        if len(my_units) > 1: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[1].x, my_units[1].y, width-2, 1))
        if len(my_units) > 2: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[2].x, my_units[2].y, 1, height-2))
        if len(my_units) > 3: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[3].x, my_units[3].y, width-2, height-2))
        
    elif num_step == 2:
        actions.append('BUILD {} {}'.format(my_tiles[0].x, my_tiles[0].y))
        actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, 1, 1))
        if len(my_units) > 1: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[1].x, my_units[1].y, width-2, 1))
        if len(my_units) > 2: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[2].x, my_units[2].y, 1, height-2))
        if len(my_units) > 3: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[3].x, my_units[3].y, width-2, height-2))

    elif num_step == 3:
        actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, 1, 1))
        if len(my_units) > 1: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[1].x, my_units[1].y, width-2, 1))
        if len(my_units) > 2: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[2].x, my_units[2].y, 1, height-2))
        if len(my_units) > 3: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[3].x, my_units[3].y, width-2, height-2))

    elif num_step == 4:
        actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, 1, 1))
        if len(my_units) > 1: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[1].x, my_units[1].y, width-2, 1))
        if len(my_units) > 2: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[2].x, my_units[2].y, 1, height-2))
        if len(my_units) > 3: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[3].x, my_units[3].y, width-2, height-2))

    elif num_step == 5:
        actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, 1, 1))
        if len(my_units) > 1: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[1].x, my_units[1].y, width-2, 1))
        if len(my_units) > 2: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[2].x, my_units[2].y, 1, height-2))
        if len(my_units) > 3: 
            actions.append('MOVE {} {} {} {} {}'.format(1, my_units[3].x, my_units[3].y, width-2, height-2))
    else: 
        for tile in my_tiles[::-1]:
            if tile.can_spawn:
                if (is_tile_blocked(tile, tiles)):
                    print(f"Debug : tile ({tile.x},{tile.y}) blocked ", file=sys.stderr, flush=True)
                #amount = 0 # TODO: pick amount of robots to spawn here
                #amount = tile.scrap_amount
                #amount = 1 if len(my_units) < 5 else 0
                #amount = 1 if my_matter > 30 and (len(my_tiles) > 5 and my_tile_is_in_bests_zones(tile, map)) and not is_tile_blocked(tile, tiles) else 0
                amount = 1 if my_matter > 30 and not is_tile_blocked(tile, tiles) and not is_bloc_blocked(tile, tiles) else 0
                #amount = 1 if (my_matter > 20 and _dist_from_tile(tile,get_nearest_oppo_tile_from_tile(opp_tiles,tile)) < 2 ) or my_matter > 30 else 0
                if amount > 0:
                    actions.append('SPAWN {} {} {}'.format(amount, tile.x, tile.y))
            if tile.can_build:
                #should_build = False # TODO: pick whether to build recycler here
                #should_build = True if tile.scrap_amount > 2 else False
                #should_build = True if len(my_recyclers) < 2 else False 
                #should_build = True if my_matter < 20 else False 
                #should_build = False
                #should_build = True if len(my_tiles) * 2 < len(opp_tiles) and len(my_recyclers) < 5 else False 
                opp_unit_nearest = get_nearest_oppo_tile_from_tile(opp_units,tile)
                should_build = True if len(my_recyclers) < 3 and my_matter < 30 and  ( opp_unit_nearest and _dist_from_tile(tile,opp_unit_nearest) < 2) else False
                if should_build:
                    actions.append('BUILD {} {}'.format(tile.x, tile.y))

        tile = get_nearest_oppo_tile_from_middle(opp_tiles, width, height)
        
        if tile:
            my_nearest_tile = get_nearest_oppo_tile_from_tile(my_tiles,tile)
            dist_nearest_oppo_tile_my_tile = _dist_from_tile(tile,my_nearest_tile)
            #print(f"Debug : nearest oppo tile : ({tile.x},{tile.y}) - dist={dist_nearest_oppo_tile_my_tile}", file=sys.stderr, flush=True)
            if dist_nearest_oppo_tile_my_tile < 2 and len(my_recyclers) < 2:
                actions.append('BUILD {} {}'.format(my_nearest_tile.x, my_nearest_tile.y))
            #else:
            #    actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, tile.x, tile.y))

        for num, tile in enumerate(my_units):
            # target = None # TODO: pick a destination tile
            # target = Point(x=width//2, y=height//2) if tile.x != width//2 else Point(x=width, y=height) 
            


            #if num < 4 and len(opp_units) > 0:
            #    opp_tile = random.choice(opp_units)
            #    c_x = opp_tile.x
            #    c_y = opp_tile.y
            #elif num == 5:
            #    c_x = width if tile.x == 0 else 0
            #    c_y = tile.y
            #elif num == 6:
            #    c_x = tile.x
            #    c_y = height if tile.y == 0 else 0
            
            #elif num%2 == 1: 
            #    c_x = randint(0, width//2)
            #    c_y = randint(0, height-1)
            #else:
            #    c_x = randint(width//2, width-1)
            #    c_y = randint(0, height-1)

            
            # si la zone de mon unité : 
            # - n'est pas completement occupé par moi
            # - n'est pas majoritairement en herbe
            # - les cases libres ne sont pas inaccessibles
            # [- le nombre de mes unités dans la même zone n'est pas trop grand] à voir car si bloqué alors RG inutile...
            # alors action MOVE vers une case NEUTE ou à OPP accessible de la zone

            # sinon exploration vers la zone d'à côté


            tile_next_side_can_controlled = get_tile_next_side_can_controlled(tile, tiles, width, height)
            if tile_next_side_can_controlled:
                print(f"Debug : unit : ({tile.x},{tile.y}) - next side tile ({tile_next_side_can_controlled.x},{tile_next_side_can_controlled.y}):{tile_next_side_can_controlled.owner}", file=sys.stderr, flush=True)
                target_zone_in_map(map, tile_next_side_can_controlled)
                targeted_tiles.append(tile_next_side_can_controlled)
                c_x = tile_next_side_can_controlled.x
                c_y = tile_next_side_can_controlled.y
            else: 
                list_tiles_currentzone_to_be_controlled  = get_tiles_current_zone_if_not_undercontrol(tile, map)
                list_tiles_nextzone_to_be_controlled  = [t for t in get_tiles_next_zone_if_not_undercontrol(tile, map) if not is_targeted_tile(t, targeted_tiles)]
                #list_tiles_current_zone_if_zone_interesting = [t for t in list_tiles_currentzone_to_be_controlled if t.units == 0]
                list_tiles_not_targeted = [t for t in list_tiles_currentzone_to_be_controlled if not is_targeted_tile(t, targeted_tiles)]
                tile_currentzone_to_be_controlled = random.choice(list_tiles_not_targeted) if len(list_tiles_not_targeted) > 0 else None
                if tile_currentzone_to_be_controlled:
                    check_current_tile = get_tile(tiles, tile_currentzone_to_be_controlled.x, tile_currentzone_to_be_controlled.y)
                    print(f"Debug : unit : ({tile.x},{tile.y}) - currentzone tile from map ({tile_currentzone_to_be_controlled.x},{tile_currentzone_to_be_controlled.y}):{tile_currentzone_to_be_controlled.owner} - and from tiles {check_current_tile.owner}", file=sys.stderr, flush=True)
                    target_zone_in_map(map, tile_currentzone_to_be_controlled)
                    targeted_tiles.append(tile_currentzone_to_be_controlled)
                    c_x = tile_currentzone_to_be_controlled.x
                    c_y = tile_currentzone_to_be_controlled.y

                elif len(list_tiles_nextzone_to_be_controlled) > 0:
                    tile_nextzone_to_be_controlled = random.choice(list_tiles_nextzone_to_be_controlled)
                    print(f"Debug : unit : ({tile.x},{tile.y}) - nextzone tile ({tile_nextzone_to_be_controlled.x},{tile_nextzone_to_be_controlled.y}):{tile_nextzone_to_be_controlled.owner}", file=sys.stderr, flush=True)
                    target_zone_in_map(map, tile_nextzone_to_be_controlled)
                    targeted_tiles.append(tile_nextzone_to_be_controlled)
                    c_x = tile_nextzone_to_be_controlled.x
                    c_y = tile_nextzone_to_be_controlled.y
                #nearest_list_neutral_tiles = get_nearest_list_neutral_tile_from_map(tile, map)
                #print(f"Debug : nearest neutre liste ({[t for t in nearest_list_neutral_tiles if t.owner == NONE and t.scrap_amount > 0]})", file=sys.stderr, flush=True)
                #nearest_neutral_tile = random.choice([t for t in nearest_list_neutral_tiles if (t.owner == NONE or not t in my_units) and t.scrap_amount > 0]) if len(nearest_list_neutral_tiles) > 0 else None
                #if nearest_neutral_tile:
                #    print(f"Debug : unit : ({tile.x},{tile.y}) - nearest neutre tile ({nearest_neutral_tile.x},{nearest_neutral_tile.y}):{nearest_neutral_tile.owner}", file=sys.stderr, flush=True)
                #    target_zone_in_map(map,nearest_neutral_tile)
                #    c_x = nearest_neutral_tile.x
                #    c_y = nearest_neutral_tile.y
                else:
                    # si une zone est sans unite opp , alors go
                    bestest_list_opp_tiles = get_best_list_opp_tile_from_map(map)
                    bestest_opp_tile = random.choice([tile for tile in bestest_list_opp_tiles if tile.owner != ME and tile.scrap_amount > 0]) if len(bestest_list_opp_tiles) > 0 else None
                    if bestest_opp_tile:
                        print(f"Debug : unit : ({tile.x},{tile.y}) - bestest opp tile ({bestest_opp_tile.x},{bestest_opp_tile.y})", file=sys.stderr, flush=True)
                        target_zone_in_map(map,bestest_opp_tile)
                        targeted_tiles.append(bestest_opp_tile)
                        c_x = bestest_opp_tile.x
                        c_y = bestest_opp_tile.y
                    else: 
                    #if _dist_from_tile(tile, nearest_oppo_tile) < 2: 
                    #    c_x = nearest_oppo_tile.x
                    #    c_y = nearest_oppo_tile.y
                    #else:
                        bestest_list_neutral_tiles = get_best_list_neutral_tile_from_map(map)
                        bestest_list_neutral_tiles_filter = [tile for tile in bestest_list_neutral_tiles if tile.owner != ME and tile.scrap_amount > 0]
                        bestest_neutral_tile = random.choice(bestest_list_neutral_tiles_filter) if len(bestest_list_neutral_tiles_filter) > 0 else None
                        if bestest_neutral_tile:
                            print(f"Debug : unit : ({tile.x},{tile.y}) - bestest neutre tile ({bestest_neutral_tile.x},{bestest_neutral_tile.y})", file=sys.stderr, flush=True)
                            target_zone_in_map(map,bestest_neutral_tile)
                            targeted_tiles.append(bestest_neutral_tile)
                            c_x = bestest_neutral_tile.x
                            c_y = bestest_neutral_tile.y

                        #nearest_neutral_tile = get_nearest_neutral_tile_from_tile(neutral_tiles, tile, targeted_tiles)
                        #if nearest_neutral_tile:
                        #    targeted_tiles.append(nearest_neutral_tile)
                        #    c_x = nearest_neutral_tile.x
                        #    c_y = nearest_neutral_tile.y
                        else:
                            nearest_oppo_tile = get_nearest_oppo_tile_from_tile(opp_tiles, tile)
                            target_zone_in_map(map,nearest_oppo_tile)
                            targeted_tiles.append(nearest_oppo_tile)
                            print(f"Debug : unit : ({tile.x},{tile.y}) - nearest opp tile ({nearest_oppo_tile.x},{nearest_oppo_tile.y})", file=sys.stderr, flush=True)
                            c_x = nearest_oppo_tile.x
                            c_y = nearest_oppo_tile.y


            """ if len(my_tiles) < len(opp_tiles):
                nearest_oppo_tile = get_nearest_oppo_tile_from_tile(opp_tiles, tile)
                c_x = nearest_oppo_tile.x
                c_y = nearest_oppo_tile.y
            else:
                #nearest_neutral_tile = get_nearest_oppo_tile_from_tile(neutral_tiles, tile)
                #c_x = nearest_neutral_tile.x
                #c_y = nearest_neutral_tile.y
                if num%4 == 0: 
                    nearest_oppo_tile = get_nearest_oppo_tile_from_tile(opp_tiles, tile)
                    c_x = nearest_oppo_tile.x
                    c_y = nearest_oppo_tile.y
                elif num%4 == 1: 
                    #farest_oppo_tile = get_farest_oppo_tile_from_tile(opp_tiles, tile)
                    #c_x = farest_oppo_tile.x
                    #c_y = farest_oppo_tile.y
                    c_x = width//2
                    c_y = height//2

                elif num%4 == 2: 
                    farest_neutral_tile = get_farest_oppo_tile_from_tile(neutral_tiles, tile)
                    c_x = farest_neutral_tile.x
                    c_y = farest_neutral_tile.y
                else:
                    #farest_oppo_tile = get_farest_oppo_tile_from_tile(opp_tiles, tile)
                    #farest_oppo_tile = get_farest_oppo_tile_from_tile(neutral_tiles, tile)
                    nearest_neutral_tile = get_nearest_oppo_tile_from_tile(neutral_tiles, tile)
                    c_x = nearest_neutral_tile.x
                    c_y = nearest_neutral_tile.y """

            target = Point(c_x, c_y)
            if target:
                #amount = 0 # TODO: pick amount of units to move
                amount = 1
                actions.append('MOVE {} {} {} {} {}'.format(amount, tile.x, tile.y, target.x, target.y))


    #print(f"Debug : targeted_tiles={targeted_tiles}", file=sys.stderr, flush=True)

    # DEBUG MAP
    print(f"Debug : after decision", file=sys.stderr, flush=True)
    _display_map(map)

    num_step += 1

    # To debug: print("Debug messages...", file=sys.stderr, flush=True)
    print(';'.join(actions) if len(actions) > 0 else 'WAIT')
