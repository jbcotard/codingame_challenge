import sys
import math
from dataclasses import dataclass
from random import randint
import random

ME = 1
OPP = 0
NONE = -1

@dataclass
class Target:
    x: int
    y: int

@dataclass
class Tile:
    x: int
    y: int
    scrap_amount: int
    owner: int
    units: int
    recycler: bool
    can_build: bool
    can_spawn: bool
    in_range_of_recycler: bool

def _dist_from_m(tile, width, height):
    return math.dist((tile.x,tile.y),(width//2, height//2))

def _dist_from_tile(tile1, tile2):
    return math.dist((tile1.x,tile1.y),(tile2.x,tile2.y))

def _is_nearest_from_m(tile1, tile2, width, height):
    return _dist_from_m(tile1, width, height) < _dist_from_m(tile2, width, height) 

def _is_farest(tile1, tile2, tile):
    return _dist_from_tile(tile1, tile) > _dist_from_tile(tile2, tile) 

def _is_nearest(tile1, tile2, tile):
    return _dist_from_tile(tile1, tile) < _dist_from_tile(tile2, tile) 

def get_nearest_oppo_tile_from_middle(opp_tiles, width, height):
    nearest_tile = None
    for tile in opp_tiles:
        if not nearest_tile or _is_nearest_from_m(tile, nearest_tile, width, height):
            nearest_tile = tile
    return nearest_tile

def get_nearest_oppo_tile_from_tile(opp_tiles, a_tile):
    nearest_tile = None
    for tile in opp_tiles:
        if not nearest_tile or _is_nearest(tile, nearest_tile, a_tile):
            nearest_tile = tile
    return nearest_tile

def get_farest_oppo_tile_from_tile(opp_tiles, a_tile):
    nearest_tile = None
    for tile in opp_tiles:
        if not nearest_tile or _is_farest(tile, nearest_tile, a_tile):
            nearest_tile = tile
    return nearest_tile

width, height = [int(i) for i in input().split()]

# game loop
while True:
    tiles = []
    my_units = []
    opp_units = []
    my_recyclers = []
    opp_recyclers = []
    opp_tiles = []
    my_tiles = []
    neutral_tiles = []

    my_matter, opp_matter = [int(i) for i in input().split()]
    for y in range(height):
        for x in range(width):
            # owner: 1 = me, 0 = foe, -1 = neutral
            # recycler, can_build, can_spawn, in_range_of_recycler: 1 = True, 0 = False
            scrap_amount, owner, units, recycler, can_build, can_spawn, in_range_of_recycler = [int(k) for k in input().split()]
            tile = Tile(x, y, scrap_amount, owner, units, recycler == 1, can_build == 1, can_spawn == 1, in_range_of_recycler == 1)

            tiles.append(tile)

            if tile.owner == ME:
                my_tiles.append(tile)
                if tile.units > 0:
                    my_units.append(tile)
                elif tile.recycler:
                    my_recyclers.append(tile)
            elif tile.owner == OPP:
                opp_tiles.append(tile)
                if tile.units > 0:
                    opp_units.append(tile)
                elif tile.recycler:
                    opp_recyclers.append(tile)
            else:
                neutral_tiles.append(tile)

    actions = []

    for tile in my_tiles:
        if tile.can_spawn:
            #amount = 0 # TODO: pick amount of robots to spawn here
            #amount = tile.scrap_amount
            amount = 1 if len(my_units) < 5 else 0
            if amount > 0:
                actions.append('SPAWN {} {} {}'.format(amount, tile.x, tile.y))
        if tile.can_build:
            #should_build = False # TODO: pick whether to build recycler here
            #should_build = True if tile.scrap_amount > 2 else False
            #should_build = True if len(my_recyclers) < 2 else False 
            #should_build = True if my_matter < 20 else False 
            should_build = False
            if should_build:
                actions.append('BUILD {} {}'.format(tile.x, tile.y))

    tile = get_nearest_oppo_tile_from_middle(opp_tiles, width, height)
    
    if tile:
        my_nearest_tile = get_nearest_oppo_tile_from_tile(my_tiles,tile)
        dist_nearest_oppo_tile_my_tile = _dist_from_tile(tile,my_nearest_tile)
        print(f"Debug : nearest oppo tile : ({tile.x},{tile.y}) - dist={dist_nearest_oppo_tile_my_tile}", file=sys.stderr, flush=True)
        if dist_nearest_oppo_tile_my_tile < 2 and len(my_recyclers) < 2:
            actions.append('BUILD {} {}'.format(my_nearest_tile.x, my_nearest_tile.y))
        #else:
        #    actions.append('MOVE {} {} {} {} {}'.format(1, my_units[0].x, my_units[0].y, tile.x, tile.y))

    for num, tile in enumerate(my_units):
        # target = None # TODO: pick a destination tile
        # target = Target(x=width//2, y=height//2) if tile.x != width//2 else Target(x=width, y=height) 
        


        #if num < 4 and len(opp_units) > 0:
        #    opp_tile = random.choice(opp_units)
        #    c_x = opp_tile.x
        #    c_y = opp_tile.y
        #elif num == 5:
        #    c_x = width if tile.x == 0 else 0
        #    c_y = tile.y
        #elif num == 6:
        #    c_x = tile.x
        #    c_y = height if tile.y == 0 else 0
        
        #elif num%2 == 1: 
        #    c_x = randint(0, width//2)
        #    c_y = randint(0, height-1)
        #else:
        #    c_x = randint(width//2, width-1)
        #    c_y = randint(0, height-1)

        if num%2 == 1: 
            nearest_oppo_tile = get_nearest_oppo_tile_from_tile(opp_tiles, tile)
            c_x = nearest_oppo_tile.x
            c_y = nearest_oppo_tile.y
        else:
            #farest_oppo_tile = get_farest_oppo_tile_from_tile(opp_tiles, tile)
            #farest_oppo_tile = get_farest_oppo_tile_from_tile(neutral_tiles, tile)
            nearest_tile = get_nearest_oppo_tile_from_tile(neutral_tiles, tile)
            c_x = nearest_tile.x
            c_y = nearest_tile.y


        target = Target(c_x, c_y)
        if target:
            #amount = 0 # TODO: pick amount of units to move
            amount = 1
            actions.append('MOVE {} {} {} {} {}'.format(amount, tile.x, tile.y, target.x, target.y))

    # To debug: print("Debug messages...", file=sys.stderr, flush=True)
    print(';'.join(actions) if len(actions) > 0 else 'WAIT')
