import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

def recherche_epave_contenant_max_eau(liste_epaves):
    epave_max_eau = {}
    max_eau = 0
    for epave in liste_epaves:
        if epave['extra'] > max_eau:
            epave_max_eau = epave
            max_eau = epave['extra'] 
    
    return epave_max_eau
    
def recherche_epave_la_plus_proche(my_reaper, liste_epaves):
    epave_plus_proche = {}
    distance_plus_proche = 6000
    for epave in liste_epaves:
        distance = math.sqrt(((epave['x'] - my_reaper['x']) ** 2) + ((epave['y'] - my_reaper['y']) ** 2)) 
        if distance_plus_proche >= distance:
            distance_plus_proche = distance
            epave_plus_proche = epave
    return epave_plus_proche
    
def recherche_unit_ennemi_la_plus_proche(my_reaper, liste_epaves):
    epave_plus_proche = {}
    distance_plus_proche = 6000
    for epave in liste_epaves:
        distance = math.sqrt(((epave['x'] - my_reaper['x']) ** 2) + ((epave['y'] - my_reaper['y']) ** 2)) 
        if distance_plus_proche >= distance:
            distance_plus_proche = distance
            epave_plus_proche = epave
    return epave_plus_proche

def recherche_unit_la_plus_proche(my_reaper, liste_units):
    unit_plus_proche = {}
    distance_plus_proche = 6000
    for unit in liste_units:
        distance = math.sqrt(((unit['x'] - my_reaper['x']) ** 2) + ((unit['y'] - my_reaper['y']) ** 2)) 
        if distance_plus_proche >= distance:
            distance_plus_proche = distance
            unit_plus_proche = unit
    return unit_plus_proche   
    
def recherche_epave_la_plus_proche_max_eau(my_reaper, liste_units):
    unit_plus_proche = {}
    distance_plus_proche = 6000
    max_eau = 0
    for unit in liste_units:
        distance = math.sqrt(((unit['x'] - my_reaper['x']) ** 2) + ((unit['y'] - my_reaper['y']) ** 2)) 
        if (distance_plus_proche >= distance) or ((2000 >= distance) and (unit['extra'] > max_eau) ):
            distance_plus_proche = distance
            unit_plus_proche = unit
            max_eau = unit['extra']
    return unit_plus_proche  

def calcule_distance(my_unit,unit):
    return math.sqrt(((unit['x'] - my_unit['x']) ** 2) + ((unit['y'] - my_unit['y']) ** 2)) 

def is_reapers_au_meme_endroit(liste_reapers):
    is_proche = False
    my_reaper = [unit for unit in liste_reapers if unit['player'] == 0][0]
    the_other_reaper = [unit for unit in liste_reapers if unit['player'] != 0]
    distance1 = calcule_distance(my_reaper,the_other_reaper[0])
    distance2 = calcule_distance(my_reaper,the_other_reaper[1])
    if (distance1 <= 2000) and (distance2 <= 2000):
        is_proche = True
    return is_proche
    


# game loop
while True:
    my_score = int(input())
    enemy_score_1 = int(input())
    enemy_score_2 = int(input())
    my_rage = int(input())
    enemy_rage_1 = int(input())
    enemy_rage_2 = int(input())
    unit_count = int(input())
    liste_units = []
    for i in range(unit_count):
        unit_id, unit_type, player, mass, radius, x, y, vx, vy, extra, extra_2 = input().split()
        unit_id = int(unit_id)
        unit_type = int(unit_type)
        player = int(player)
        mass = float(mass)
        radius = int(radius)
        x = int(x)
        y = int(y)
        vx = int(vx)
        vy = int(vy)
        extra = int(extra)
        extra_2 = int(extra_2)
        liste_units.append({"unit_id": unit_id, "unit_type": unit_type, "player": player, "mass": mass, "radius": radius, "x": x, "y": y, "vx": vx, "vy": vy, "extra": extra, "extra_2": extra_2})
        
    
    liste_epaves = [epave for epave in liste_units if epave['unit_type'] == 4]
    liste_tankers = [epave for epave in liste_units if epave['unit_type'] == 3]
    liste_reapers = [unit for unit in liste_units if unit['unit_type'] == 0]
    liste_reapers_ennemy  = [unit for unit in liste_units if unit['unit_type'] == 0 and unit['player'] != 0]
    
    #epave_cible = recherche_epave_contenant_max_eau(liste_epaves)
    my_units = [unit for unit in liste_units if unit['player'] == 0]
    my_reaper = my_units[0]
    my_destroyer = my_units[1]
    my_doof = my_units[2]
    #epave_cible = recherche_unit_la_plus_proche(my_reaper, liste_epaves)
    #epave_cible = recherche_epave_la_plus_proche_max_eau(my_reaper, liste_epaves)
    
    epave_cible = recherche_epave_la_plus_proche(my_reaper,liste_epaves)
    if not is_reapers_au_meme_endroit(liste_reapers):
        epave_cible = recherche_epave_contenant_max_eau(liste_epaves)
    tanker_cible = recherche_unit_la_plus_proche(my_destroyer, liste_tankers)
    #doof_cible = recherche_unit_ennemi_la_plus_proche(my_doof,liste_reapers)
    doof_cible = liste_reapers_ennemy[0] if enemy_score_1 >= enemy_score_2 else liste_reapers_ennemy[1] 
    
    #print(f"{liste_epaves}", file=sys.stderr)
    #print(f"{liste_tankers}", file=sys.stderr)
    print(f"{epave_cible}", file=sys.stderr)
    print(f"{tanker_cible}", file=sys.stderr)
    print(f"{my_rage}", file=sys.stderr)
    
    
    # decisions reaper
    if len(liste_epaves) > 0 and epave_cible:
        distance = calcule_distance(my_reaper, epave_cible)
        boost = 200
        if distance >= 1500:
            boost = 300
        elif distance < 500:
            boost = 50
        print(f"{epave_cible['x']} {epave_cible['y']} {boost}")
    else: 
        #print("WAIT")
        #print(f"{my_destroyer['x']} {my_destroyer['y']} 200")
        #print(f"0 0 200")
        print(f"{my_destroyer['x']+400} {my_destroyer['y']+400} 200")
    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

     # decisions DESTROYER
    if my_rage > 100 and is_reapers_au_meme_endroit(liste_reapers):
        print(f"SKILL {my_reaper['x']} {my_reaper['y']}")
    elif len(liste_tankers) > 0 and tanker_cible:
        print(f"{tanker_cible['x']} {tanker_cible['y']} 200")
    else: 
        print("WAIT")
      
    # decisions DOOF
    #if my_rage==300:
    #    print(f"SKILL {my_reaper['x']+400} {my_reaper['y']+400}")
    if len(liste_tankers) > 0 and doof_cible:
        print(f"{doof_cible['x']} {doof_cible['y']} 300")
    else: 
        print("WAIT")