#!/bin/bash

RESULT_EXPECTED="281"
RESULT=$(python3 solve_part_two.py exemple2.txt | grep RESULT)

[ "${RESULT_EXPECTED}" == "${RESULT#*:}" ] && printf "test ✅" || printf "test ❌"
echo ""