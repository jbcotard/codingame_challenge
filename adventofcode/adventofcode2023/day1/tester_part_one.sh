#!/bin/bash

RESULT_EXPECTED="142"
RESULT=$(python3 solve_part_one.py exemple.txt | grep RESULT)

[ "${RESULT_EXPECTED}" == "${RESULT#*:}" ] && printf "test ✅" || printf "test ❌"
echo ""