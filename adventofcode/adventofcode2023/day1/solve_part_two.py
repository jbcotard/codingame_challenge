import sys
import re

list_digit_litteral = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']

def is_digit_literal(str) -> bool:
    if str in list_digit_litteral:
        return True
    else :
        return False
def get_digit_litteral(str):
    if str == 'one':
        return 1
    elif str == 'two':
        return 2
    elif str == 'three':
        return 3
    elif str == 'four':
        return 4
    elif str == 'five':
        return 5
    elif str == 'six':
        return 6
    elif str == 'seven':
        return 7
    elif str == 'eight':
        return 8
    elif str == 'nine':
        return 9

def get_number(str):
    i = 0
    # list_digit = []
    # for digit in list_digit_litteral:
    #     if digit in str:
    #         list_digit.append(digit)

    # print(list_digit)
    # if list_digit and len(list_digit) > 1:
    #     i = int(get_digit_litteral(list_digit[0]) + get_digit_litteral(list_digit[-1]))
    # elif list_digit and len(list_digit) == 1:
    #     i = int(get_digit_litteral(list_digit[0]) +get_digit_litteral(list_digit[0]))
    


    return i

# function to extract the substring between the last digit and the end
def get_index_last_digit_in_string(string):
    for i in range(len(string)-1, -1, -1):
        if string[i].isdigit():
            return i
        
def get_index_first_digit_in_string(string):
    for i in range(len(string)):
        if string[i].isdigit():
            return i

# extract the number in litteral form
def extract_number_literal(string):
    #list_digit = []
    # for digit in list_digit_litteral:
    #     if digit in string:
    #         list_digit.append(digit)

    nombres_lit = {
        'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5, 'six': 6, 'seven': 7, 'eight': 8, 'nine': 9
    }

    nombres = re.findall(r'(?:one|two|three|four|five|six|seven|eight|nine)', string)
    liste_nombres_lit = []
    liste_nombres = []
    # for nombre in nombres:
    #     if nombre in nombres_lit:
    #         liste_nombres.append(nombres_lit[nombre])

    i = 0

    while i < len(nombres):
        nombre = nombres[i]
        if nombre in nombres_lit:
            liste_nombres.append(nombres_lit[nombre])
        else:
            i += 1
            continue

        # Vérifie si le nombre suivant commence par le nombre actuel (recouvrement)
        if i + 1 < len(nombres) and nombres[i + 1].startswith(nombre):
            i += 1
        i += 1

    liste_nombres_lit.append(liste_nombres)
    #print(f"test {nombres}")

    return liste_nombres_lit[0]

def get_last_digit_in_string(string):
    for i in range(len(string)-1, -1, -1):
        if string[i].isdigit():
            return string[i]
        
def get_first_digit_in_string(string):
    for i in range(len(string)):
        if string[i].isdigit():
            return string[i]

def get_number_from_string(string):

    #print(f"{string[0]}...{string[-1]} ")
    # si la chaine commence et se termine par un chiffre
    if string[0].isdigit() and string[-1].isdigit():
        return int(string[0]+string[-1])
    # si la chaine commence par un chiffre
    elif string[0].isdigit():
        list_number_found = extract_number_literal(string[get_index_last_digit_in_string(string):])
        #print(list_number_found)
        if list_number_found:
            return int(string[0] + str(list_number_found[-1]))
        else :
            return int(string[0] + str(get_last_digit_in_string(string)))
    # si la chaine se termine par un chiffre
    elif string[-1].isdigit():
        list_number_found = extract_number_literal(string[:get_index_first_digit_in_string(string)])
        if list_number_found:
            return int(str(list_number_found[0]) + string[-1])
        else :
            return int(get_first_digit_in_string(string) + string[-1])
    # si la chaine ne contient aucun chiffre
    elif not any(char.isdigit() for char in string):
        list_number_found = extract_number_literal(string)
        #print(list_number_found)
        if list_number_found:
            #print(f"{list_number_found[0][0]} - {list_number_found[-1][0]}")
            return int(str(list_number_found[0]) + str(list_number_found[-1]))
        else :
            return 0
    else:
        #print(string)
        list_number_found = extract_number_literal(string[:get_index_first_digit_in_string(string)])
        #print(list_number_found)
        starter = get_first_digit_in_string(string)
        if list_number_found:
            starter = str(list_number_found[0])
        
        ender = get_last_digit_in_string(string)
        list_number_found = extract_number_literal(string[get_index_last_digit_in_string(string):])
        #print(f"{list_number_found} - {len(list_number_found[0])} ")
        if list_number_found and len(list_number_found) > 1:
            ender = str(list_number_found[-1])
        elif list_number_found and len(list_number_found) == 1:
            ender = str(list_number_found[0])
        return int(starter + ender)

def check_number_in_string(string):
    if string[0].isdigit() and string[-1].isdigit():
        return True
    elif string[0].isdigit():
        for char in string[1:]:
            if char.isalpha():
                return True
    elif string[-1].isdigit():
        for char in string[:-1]:
            if char.isalpha():
                return True
    else:
        for char in string:
            if char.isalpha():
                return True
    return False

if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

total = 0
i = 0
with open(sys.argv[1]) as f:
    for line in f:
        if line.strip():
            #print(f"line: {line.strip()}.")
            current_number = get_number_from_string(line.strip())
            #if i < 800 and i > 790:
            print(f"{i} - {line.strip()} - {current_number}")
            i += 1
            #print(current_number)
            total += current_number


message = f"{total}"
print(f"RESULT:{message}")