import sys

if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

total = 0
with open(sys.argv[1]) as f:
    for line in f:
        if line.strip():
            #print(line)
            extract_digit = ''.join([a for a in line if a.isdigit()])
            #print(extract_digit)
            if int(extract_digit) > 99:
                #print(extract_digit[1:] + extract_digit[:-1])
                total += int(extract_digit[0] + extract_digit[-1])
            elif int(extract_digit) > 9:
                total += int(extract_digit)
            else:
                total += int(extract_digit + extract_digit)




message = f"{total}"
print(f"RESULT:{message}")
