import sys
import os
import re

# Check if the DEBUG environment variable is set
DEBUG = os.getenv("DEBUG")

# fonction pour affiche un message de debug lorsque un flag DEBUG est défini
def debug_message(message):
    if DEBUG:
        print(message)


# fonction pour séparer les subsets dont le separateurs est ";" et où il peut y en avoir plusieurs
def split_subsets(subsets):
    subsets_list = subsets.split(";")
    return subsets_list


max_red_cubes = 12
max_green_cubes = 13
max_blue_cubes = 14


if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

sum_game_id_valid = 0

with open(sys.argv[1]) as f:
    for line in f:
        if line.strip():
            debug_message(line.strip())
            pattern = r"Game (\d+): (.*)"
            match = re.search(pattern, line.strip())
            if match:
                game_id = match.group(1)
                subsets = match.group(2)  
                #debug_message(f"Game {game_id}: {subsets}")
                subsets_list = split_subsets(subsets)

                game_ok = True
                for subset in subsets_list:
                    red_cubes = 0
                    blue_cubes = 0
                    green_cubes = 0
                    #debug_message(f"Subset: {subset}")
                    cubes = subset.split(",")
                    #debug_message(f"Cubes: {cubes}")
                    for cube in cubes:
                        #debug_message(f"Cube: {cube}")
                        color = cube.strip().split(" ")[1]
                        nb = cube.strip().split(" ")[0]
                        #debug_message(f"Color: {color} -> nb: >{nb}<")
                        if color == "red":
                            red_cubes += int(nb)
                        elif color == "green":
                            green_cubes += int(nb)
                        elif color == "blue":
                            blue_cubes += int(nb)
                    debug_message(f" >>Red cubes: {red_cubes}, Green cubes: {green_cubes}, Blue cubes: {blue_cubes}")
                    if red_cubes > max_red_cubes or green_cubes > max_green_cubes or blue_cubes > max_blue_cubes:
                        game_ok = False
                    
                if game_ok:    
                    debug_message(f"Game {game_id} valid")
                    sum_game_id_valid += int(game_id)
                else:
                    debug_message(f"Game {game_id} invalid")



message = sum_game_id_valid
print(f"RESULT:{message}")
