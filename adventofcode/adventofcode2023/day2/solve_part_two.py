import sys
import os
import re

# Check if the DEBUG environment variable is set
DEBUG = os.getenv("DEBUG")

# fonction pour affiche un message de debug lorsque un flag DEBUG est défini
def debug_message(message):
    if DEBUG:
        print(message)


# fonction pour séparer les subsets dont le separateurs est ";" et où il peut y en avoir plusieurs
def split_subsets(subsets):
    subsets_list = subsets.split(";")
    return subsets_list


# fonction qui verifie qu'un nombre donné a plusieurs occurrences dans une liste de nombres
def check_multiple_occurrences(liste_nombres, nombre):
    return liste_nombres.count(nombre) > 1


if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

sum_power = 0

with open(sys.argv[1]) as f:
    for line in f:
        if line.strip():
            debug_message(line.strip())
            pattern = r"Game (\d+): (.*)"
            match = re.search(pattern, line.strip())
            if match:
                game_id = match.group(1)
                subsets = match.group(2)  
                #debug_message(f"Game {game_id}: {subsets}")
                subsets_list = split_subsets(subsets)

                game_ok = True
                max_red_cubes = 0
                max_green_cubes = 0
                max_blue_cubes = 0
                red_cubes = []
                green_cubes = []
                blue_cubes = []
                for subset in subsets_list:

                    #debug_message(f"Subset: {subset}")
                    cubes = subset.split(",")
                    #debug_message(f"Cubes: {cubes}")
                    for cube in cubes:
                        #debug_message(f"Cube: {cube}")
                        color = cube.strip().split(" ")[1]
                        nb = cube.strip().split(" ")[0]
                        #debug_message(f"Color: {color} -> nb: >{nb}<")
                        if color == "red":
                            red_cubes.append(int(nb))
                        elif color == "green":
                            green_cubes.append(int(nb))
                        elif color == "blue":
                            blue_cubes.append(int(nb))
                    #debug_message(f" >>Red cubes: {red_cubes}, Green cubes: {green_cubes}, Blue cubes: {blue_cubes}")
                    #if red_cubes > max_red_cubes or green_cubes > max_green_cubes or blue_cubes > max_blue_cubes:
                    #    game_ok = False
                max_red_cubes = max(red_cubes)
                debug_message(f"Max red cubes: {max_red_cubes} - {red_cubes} - {red_cubes.count(max_red_cubes)} - {check_multiple_occurrences(red_cubes, max_red_cubes)}")
                max_green_cubes = max(green_cubes)
                debug_message(f"Max green cubes: {max_green_cubes} - {green_cubes}  - {green_cubes.count(max_green_cubes)} - {check_multiple_occurrences(green_cubes, max_green_cubes)}")
                max_blue_cubes = max(blue_cubes)
                debug_message(f"Max blue cubes: {max_blue_cubes} - {blue_cubes}  - {blue_cubes.count(max_blue_cubes)} - {check_multiple_occurrences(blue_cubes, max_blue_cubes)}")
                if not check_multiple_occurrences(red_cubes, max_red_cubes) or not check_multiple_occurrences(green_cubes, max_green_cubes) or not check_multiple_occurrences(blue_cubes, max_red_cubes) :
                    game_ok = False
                    
                if game_ok:    
                    debug_message(f"Game {game_id} valid")
                    sum_power += int(max_red_cubes * max_blue_cubes * max_green_cubes)
                else:
                    debug_message(f"Game {game_id} invalid")



message = sum_power
print(f"RESULT:{message}")
