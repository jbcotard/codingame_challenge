import sys
import os

# Check if the DEBUG environment variable is set
DEBUG = os.getenv("DEBUG")

# fonction pour affiche un message de debug lorsque un flag DEBUG est défini
def debug_message(message):
    if DEBUG:
        print(message)

if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

first_list = []
second_list = []

with open(sys.argv[1]) as f:
    for line in f:
        debug_message(line)
        if line.strip():
            places = line.split()
            first_list.append(places[0])
            second_list.append(places[1])

first_list.sort()
second_list.sort()
total = 0
for i in range (len(first_list)):
    total += abs(int(first_list[i]) - int(second_list[i]))


message = total
print(f"RESULT:{message}")
