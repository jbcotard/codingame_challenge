import sys
import os

# Check if the DEBUG environment variable is set
DEBUG = os.getenv("DEBUG")

# fonction pour affiche un message de debug lorsque un flag DEBUG est défini
def debug_message(message):
    if DEBUG:
        print(message)

def is_levels_increase(report) -> bool:
    increasing = True
    for i in range(len(report)):
        if i < (len(report) - 1)  and int(report[i]) >= int(report[i+1]):
            increasing = False
    return increasing

def count_levels_not_increase(report) -> int:
    nb_not_increasing = 0
    for i in range(len(report)):
        if i < (len(report) - 2) and (int(report[i]) >= int(report[i+2]) or int(report[i]) >= int(report[i+1])):
            nb_not_increasing += 1
        elif i < (len(report) - 1)  and int(report[i]) >= int(report[i+1]):
            nb_not_increasing += 1
    return nb_not_increasing

def index_levels_not_increase(report):
    liste_index_invalid = []
    for i in range(len(report)):
        if i < (len(report) - 2) and (int(report[i]) >= int(report[i+2]) or int(report[i]) >= int(report[i+1])):
            liste_index_invalid.append(i)
        elif i < (len(report) - 1)  and int(report[i]) >= int(report[i+1]):
            liste_index_invalid.append(i)
    return liste_index_invalid

def is_levels_decrease(report) -> bool:
    decreasing = True
    for i in range(len(report)):
        if i < (len(report) - 1) and int(report[i]) <= int(report[i+1]):
            decreasing = False
    return decreasing

def count_levels_not_decrease(report) -> int:
    nb_not_decreasing = 0
    for i in range(len(report)):
        if i < (len(report) - 2) and (int(report[i]) <= int(report[i+2]) or int(report[i]) <= int(report[i+1])):
            nb_not_decreasing += 1
        elif i < (len(report) - 1) and int(report[i]) <= int(report[i+1]):
            nb_not_decreasing += 1
    return nb_not_decreasing

def index_levels_not_decrease(report):
    liste_index_invalid = []
    for i in range(len(report)):
        if i < (len(report) - 2) and (int(report[i]) <= int(report[i+2]) or int(report[i]) <= int(report[i+1])):
            liste_index_invalid.append(i)
        elif i < (len(report) - 1) and int(report[i]) <= int(report[i+1]):
            liste_index_invalid.append(i)
    return liste_index_invalid

def is_adjacent_levels_difference_valid(report) -> bool:
    diff_valid = True
    for i in range(len(report)):
        if i < (len(report) - 1):
            diff = abs(int(report[i]) - int(report[i+1]))
            if diff < 1 or diff > 3:
                #debug_message(f"{report[i]}-{report[i+1]}={diff} ; diff > 0 and diff < 4 {diff > 0} {diff < 4}")
                diff_valid = False
    return diff_valid

def count_adjacent_levels_difference_invalid(report) -> int:
    diff_invalid = 0
    for i in range(len(report)):
        if i < (len(report) - 1):
            diff = abs(int(report[i]) - int(report[i+1]))
            if diff < 1 or diff > 3:
                #debug_message(f"{report[i]}-{report[i+1]}={diff} ; diff > 0 and diff < 4 {diff > 0} {diff < 4}")
                diff_invalid += 1
    return diff_invalid

def index_adjacent_levels_difference_invalid(report):
    index_diff_invalid = []
    for i in range(len(report)):
        if i < (len(report) - 1):
            diff = abs(int(report[i]) - int(report[i+1]))
            if diff < 1 or diff > 3:
                index_diff_invalid.append(i)
    return index_diff_invalid


if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

reports = []

with open(sys.argv[1]) as f:
    for line in f:
        #debug_message(line)
        if line.strip():
            reports.append(line)

#debug_message(reports)

count_report_safe = 0
for report in reports:
    #debug_message(report)
    levels = report.split()
    if (is_levels_decrease(levels) or is_levels_increase(levels)) and is_adjacent_levels_difference_valid(levels):
        #debug_message(" --> SAFE")
        count_report_safe += 1
    else : 
        #debug_message(f" --> UNSAFE : [increase:{is_levels_increase(levels)};decrease:{is_levels_decrease(levels)};diff-valid:{is_adjacent_levels_difference_valid(levels)}]")
        #if (((count_levels_not_increase(levels) <= 1 or count_levels_not_decrease(levels) <= 1) and count_adjacent_levels_difference_invalid(levels) == 1)
        #or ((count_levels_not_increase(levels) == 1 or count_levels_not_decrease(levels) == 1) and count_adjacent_levels_difference_invalid(levels) == 0)):
            #debug_message(f"{count_levels_not_increase(levels)};{count_levels_not_decrease(levels)};{count_adjacent_levels_difference_invalid(levels)}")
            #count_report_safe += 1
            #debug_message(f" --> UNSAFE : {levels} -> SAFE")
    #count_report_safe += 1 if (is_levels_decrease(levels) or is_levels_increase(levels)) and is_adjacent_levels_difference_valid(levels) else 0

        if ((count_levels_not_increase(levels) == 1 and count_adjacent_levels_difference_invalid(levels) == 0) 
        or (count_levels_not_decrease(levels) == 1 and count_adjacent_levels_difference_invalid(levels) == 0)
        or (is_levels_decrease(levels) and count_adjacent_levels_difference_invalid(levels) == 1)
        or (is_levels_increase(levels) and count_adjacent_levels_difference_invalid(levels) == 1)
        or (count_levels_not_increase(levels) == 1 and count_adjacent_levels_difference_invalid(levels) == 1 and index_levels_not_increase(levels) == index_adjacent_levels_difference_invalid(levels))
        or (count_levels_not_decrease(levels) == 1 and count_adjacent_levels_difference_invalid(levels) == 1 and index_levels_not_decrease(levels) == index_adjacent_levels_difference_invalid(levels))):
            count_report_safe += 1
            debug_message(f" --> UNSAFE : {levels} -> SAFE  ## {count_levels_not_increase(levels)} {count_levels_not_decrease(levels)} {count_adjacent_levels_difference_invalid(levels)} ##")
        else:
            debug_message(f" --> UNSAFE : {levels} -> UNSAFE  ## {count_levels_not_increase(levels)} {count_levels_not_decrease(levels)} {count_adjacent_levels_difference_invalid(levels)} ##")

message = count_report_safe
print(f"RESULT:{message}")
