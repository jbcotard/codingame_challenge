#!/bin/bash

RESULT_EXPECTED="4"
RESULT=$(python3 solve_part_two.py exemple.txt | grep RESULT)

[ "${RESULT_EXPECTED}" == "${RESULT#*:}" ] && printf "test ✅" || printf "test ❌"
echo ""