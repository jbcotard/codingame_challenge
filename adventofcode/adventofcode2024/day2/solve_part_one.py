import sys
import os

# Check if the DEBUG environment variable is set
DEBUG = os.getenv("DEBUG")

# fonction pour affiche un message de debug lorsque un flag DEBUG est défini
def debug_message(message):
    if DEBUG:
        print(message)

def is_levels_increase(report) -> bool:
    increasing = True
    for i in range(len(report)):
        if i < (len(report) - 1)  and int(report[i]) >= int(report[i+1]):
            increasing = False
    return increasing

def is_levels_decrease(report) -> bool:
    decreasing = True
    for i in range(len(report)):
        if i < (len(report) - 1) and int(report[i]) <= int(report[i+1]):
            decreasing = False
    return decreasing

def is_adjacent_levels_difference_valid(report) -> bool:
    diff_valid = True
    for i in range(len(report)):
        if i < (len(report) - 1):
            diff = abs(int(report[i]) - int(report[i+1]))
            if diff < 1 or diff > 3:
                debug_message(f"{report[i]}-{report[i+1]}={diff} ; diff > 0 and diff < 4 {diff > 0} {diff < 4}")
                diff_valid = False
    return diff_valid

if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

reports = []

with open(sys.argv[1]) as f:
    for line in f:
        #debug_message(line)
        if line.strip():
            reports.append(line)

#debug_message(reports)

count_report_safe = 0
for report in reports:
    debug_message(report)
    levels = report.split()
    if (is_levels_decrease(levels) or is_levels_increase(levels)) and is_adjacent_levels_difference_valid(levels):
        debug_message(" --> SAFE")
    else : 
        debug_message(f" --> UNSAFE : [increase:{is_levels_increase(levels)};decrease:{is_levels_decrease(levels)};diff-valid:{is_adjacent_levels_difference_valid(levels)}]")
    count_report_safe += 1 if (is_levels_decrease(levels) or is_levels_increase(levels)) and is_adjacent_levels_difference_valid(levels) else 0


message = count_report_safe
print(f"RESULT:{message}")
