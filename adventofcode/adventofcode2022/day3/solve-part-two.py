priorities = {
    'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8, 'i': 9,
    'j': 10, 'k': 11, 'l': 12, 'm': 13, 'n': 14, 'o': 15, 'p': 16, 'q': 17,
    'r': 18, 's': 19, 't': 20, 'u': 21, 'v': 22, 'w': 23, 'x': 24, 'y': 25,
    'z': 26,
    'A': 27, 'B': 28, 'C': 29, 'D': 30, 'E': 31, 'F': 32, 'G': 33, 'H': 34,
    'I': 35, 'J': 36,'K': 37, 'L': 38, 'M': 39, 'N': 40, 'O': 41, 'P': 42,
    'Q': 43, 'R': 44, 'S': 45, 'T': 46,'U': 47, 'V': 48, 'W': 49, 'X': 50,
    'Y': 51, 'Z': 52
    }

liste_groups = []
liste_rucksacks = []
index = 0
with open('input.txt') as f:
    for line in f:
        if line.strip():
            liste_rucksacks.append(line)

        if index < 2:
            index += 1
        else:
            liste_groups.append(liste_rucksacks)
            liste_rucksacks = []
            index = 0
#print(len(liste_groups))
#print(liste_groups)

total_priorities  = 0
list_items = []
for group in liste_groups:
    non_trouve = True
    for item in group[0]:
        if item in group[1] and item in group[2] and non_trouve:
            total_priorities += priorities[item]
            list_items.append(item)
            non_trouve = False
print(f"nb groupes = {len(liste_groups)};nb items trouves = {len(list_items)}")
print(total_priorities)
