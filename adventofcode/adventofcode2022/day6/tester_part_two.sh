#!/bin/bash

tests_input=("exemple.txt" "exemple2.txt" "exemple3.txt" "exemple4.txt" "exemple5.txt")
tests_result_expected=(19 23 23 29 26)
for i in "${!tests_input[@]}";
do 
    echo "------------------------------------------"
    echo "TEST $i : ${tests_input[i]} -> ${tests_result_expected[i]}"
    RESULT_EXPECTED="${tests_result_expected[i]}"
    RESULT=$(python solve_part_two.py "${tests_input[i]}" | grep RESULT)

    [ "${RESULT_EXPECTED}" == "${RESULT#*:}" ] && printf "...test ✅" || printf "...test ❌"
    echo ""
done;