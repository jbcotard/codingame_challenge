import sys

if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

data = ""
with open(sys.argv[1], encoding="UTF-8") as f:
    for line in f:
        if line.strip():
            data = line
marker = ""
index_marker = 0

maker_trouve = False
while not maker_trouve:
    marker = data[index_marker:index_marker+14]
    print(f"debug : {marker} - {set(marker)} - {index_marker}")
    if len(set(marker)) == 14:
        maker_trouve = True
    else:
        index_marker += 1

print(f"debug : final {marker} - {index_marker+14}")

message = str(index_marker+14)
print(f"RESULT:{message}")
