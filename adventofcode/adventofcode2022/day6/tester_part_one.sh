#!/bin/bash

tests_input=("exemple.txt" "exemple2.txt" "exemple3.txt" "exemple4.txt" "exemple5.txt")
tests_result_expected=(7 5 6 10 11)
for i in "${!tests_input[@]}";
do 
    echo "------------------------------------------"
    echo "TEST $i : ${tests_input[i]} -> ${tests_result_expected[i]}"
    RESULT_EXPECTED="${tests_result_expected[i]}"
    RESULT=$(python solve_part_one.py "${tests_input[i]}" | grep RESULT)

    [ "${RESULT_EXPECTED}" == "${RESULT#*:}" ] && printf "...test ✅" || printf "...test ❌"
    echo ""
done;
