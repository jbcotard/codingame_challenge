
elves_calories = []
calories = 0
elf = 1
with open('input.txt') as f:
    for line in f:
        #t=(not line.strip().isdigit())
        #print(f"{line} - {t}")
        if not line.strip().isdigit() :
            elves_calories.append({"elf": elf, "calories": calories})
            calories = 0
            elf += 1
        else:
            calories += int(line)

print(elves_calories)

elves_calories_sorted = sorted(elves_calories, key=lambda i: i['calories'], reverse=True)
print(elves_calories_sorted)


sum_top3_max_calories = elves_calories_sorted[0]['calories'] + elves_calories_sorted[1]['calories'] + elves_calories_sorted[2]['calories']
elf_top3_max_calories = str(elves_calories_sorted[0]['elf']) + "," + str(elves_calories_sorted[1]['elf']) + "," + str(elves_calories_sorted[2]['elf'])


print(f"the Top 3 of Elves carrying the most Calories is {elf_top3_max_calories} - {sum_top3_max_calories}")