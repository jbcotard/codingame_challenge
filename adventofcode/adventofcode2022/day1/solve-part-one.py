
elves_calories = []
calories = 0
with open('input.txt') as f:
    for line in f:
        if line == '\n':
            elves_calories.append(calories)
            calories = 0
        else:
            calories += int(line)

max_calories = 0
elf_max_calories = 1

for elf, calories in enumerate(elves_calories):
    if calories > max_calories:
        elf_max_calories = elf + 1
        max_calories = calories

print(f"the Elf carrying the most Calories is {elf_max_calories} - {max_calories}")