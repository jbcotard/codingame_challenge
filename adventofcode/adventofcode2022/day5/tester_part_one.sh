#!/bin/bash

RESULT_EXPECTED="CMZ"
RESULT=$(python solve_part_one.py exemple.txt | grep RESULT)

[ "${RESULT_EXPECTED}" == "${RESULT#*:}" ] && printf "test ✅" || printf "test ❌"