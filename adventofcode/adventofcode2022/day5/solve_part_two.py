import sys

# print('DEBUG > Number of arguments:', len(sys.argv), 'arguments.')
# print('DEBUG > Argument List:', str(sys.argv))

if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)


liste_crates_lines = []
liste_index_stacks = []
liste_moves = []
with open(sys.argv[1]) as f:
    for line in f:
        if line.strip():
            if line.strip().startswith('['):
                # liste line crates 
                liste_crates_lines.append(line)
            elif line.strip().startswith('1'):
                # liste index  stack
                liste_index_stacks = line.split()
            else:
                # move operation
                liste_moves.append(line)

nb_stacks = len(liste_crates_lines[0])/4
stacks = {}
for index_stack in liste_index_stacks:
    stacks[f"{index_stack}"] = []

for crates_line in liste_crates_lines:
    for stack_key, stack_value in stacks.items():
        offset = (int(stack_key) - 1) * 4
        crate = crates_line[offset:offset+3]
        stack_value.append(crate) if crate.strip() else {}


for move in liste_moves:
    m, nb_crates, f, stack_from, t, stack_to = move.split()
    # on recupere la liste des crates à deplacer de la stack "from"
    liste_crates_to_move = []
    for i in range(int(nb_crates)):
        liste_crates_to_move.append(stacks[stack_from].pop(0))
    # on inverse la liste des crates à deplacer (RG)
    liste_crates_to_move.reverse()
    # on insere la liste des crates à deplacer dans la stack "to"
    for crate in liste_crates_to_move:
        stacks[stack_to].insert(0, crate)

print(stacks)

message = ""
for stack_key, stack_value in stacks.items():
    # selection du crate le plus haut dans la stack
    # et on prend la valeur entre les crochets (ex crate=[M] -> M)
    message += stack_value[0][1]

print(f"RESULT:{message}")
