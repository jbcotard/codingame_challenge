#!/bin/bash

RESULT_EXPECTED="xxx"
RESULT=$(python solve_part_two.py exemple.txt | grep RESULT)

[ "${RESULT_EXPECTED}" == "${RESULT#*:}" ] && printf "test ✅" || printf "test ❌"
echo ""