import sys
import os

# Check if the DEBUG environment variable is set
DEBUG = os.getenv("DEBUG")

# fonction pour affiche un message de debug lorsque un flag DEBUG est défini
def debug_message(message):
    if DEBUG:
        print(message)

if len(sys.argv) != 2:
    print("ERR > fichier input absent ")
    sys.exit(1)

with open(sys.argv[1]) as f:
    for line in f:
        if line.strip():
            debug_message(line)

message = "xxx"
print(f"RESULT:{message}")
