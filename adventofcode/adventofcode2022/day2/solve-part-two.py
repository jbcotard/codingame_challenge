
# A = rock ; B, Y = paper ; C = scissor; X = lose, Y = draw, Z = win
combinaisons = {
    'A X': 3, # 0 + ?(on deduit scissor soit 3)
    'A Y': 4, # 3 + ?(on deduit rock soit 1)
    'A Z': 8, # 6 + ?(on deduit paper soit 2)
    'B X': 1, # 0 + ?(on deduit rock soit 1)
    'B Y': 5, # 3 + ?(on deduit paper soit 2)
    'B Z': 9, # 6 + ?(on deduit scissor soit 3)
    'C X': 2, # 0 + ?(on deduit paper soit 2)
    'C Y': 6, # 3 + ?(on deduit scissor soit 3)
    'C Z': 7 # 6 + ?(on deduit rock soit 1)
    }

turn_points = 0
total_points = 0
with open('input.txt') as f:
    for line in f:
        if line.strip():
            turn_points = combinaisons[line.strip()]
            total_points += turn_points

print(f"total score : {total_points}")