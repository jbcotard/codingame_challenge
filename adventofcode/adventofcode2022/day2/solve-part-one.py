
# A, X = rock ; B, Y = paper ; C, Z = scissor
combinaisons = {
    'A X': 4, # 1 + 3
    'A Y': 8, # 2 + 6
    'A Z': 3, # 3 + 0
    'B X': 1, # 1 + 0
    'B Y': 5, # 2 + 3
    'B Z': 9, # 3 + 6
    'C X': 7, # 1 + 6
    'C Y': 2, # 2 + 0
    'C Z': 6 # 3 + 3
    }

turn_points = 0
total_points = 0
with open('input.txt') as f:
    for line in f:
        if line.strip():
            turn_points = combinaisons[line.strip()]
            total_points += turn_points

print(f"total score : {total_points}")