
def is_fully_contained(pair_assignement) -> bool: 
    job_first_elf = line.split(',')[0]
    job_second_elf = line.split(',')[1]
    start_job_first_elf = int(job_first_elf.split('-')[0])
    end_job_first_elf = int(job_first_elf.split('-')[1])
    start_job_second_elf = int(job_second_elf.split('-')[0])
    end_job_second_elf = int(job_second_elf.split('-')[1])

    return (
        (
            (start_job_first_elf <= start_job_second_elf)
            and (end_job_second_elf <= end_job_first_elf)
        )
        or
        (
            (start_job_second_elf <= start_job_first_elf)
            and (end_job_first_elf <= end_job_second_elf)
        )
    )


nb_job_fully_contained = 0
job_fully_contained = []
with open('input.txt') as f:
    for line in f:
        if line.strip() and is_fully_contained(line.strip()):
            job_fully_contained.append(line.strip())
            nb_job_fully_contained += 1

print(f"assignement fully contained : {job_fully_contained}")
print(f"nb fully contained assignement : {nb_job_fully_contained}")
