def is_overlap(pair_assignement) -> bool: 
    job_first_elf = line.split(',')[0]
    job_second_elf = line.split(',')[1]
    start_job_first_elf = int(job_first_elf.split('-')[0])
    end_job_first_elf = int(job_first_elf.split('-')[1])
    start_job_second_elf = int(job_second_elf.split('-')[0])
    end_job_second_elf = int(job_second_elf.split('-')[1])

    return not (
        (end_job_first_elf < start_job_second_elf)
        or
        (end_job_second_elf < start_job_first_elf)
    )


nb_job_overlap = 0
job_overlap = []
with open('input.txt') as f:
    for line in f:
        if line.strip() and is_overlap(line.strip()):
            job_overlap.append(line.strip())
            nb_job_overlap += 1

print(f"overlapping assignement : {job_overlap}")
print(f"nb overlapping assignement  : {nb_job_overlap}")