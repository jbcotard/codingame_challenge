import sys
import math
import random
import numpy as np
import pandas as pd


# Grab the pellets as fast as you can!

# width: size of the grid
# height: top left corner is (x=0, y=0)
width, height = [int(i) for i in input().split()]
for i in range(height):
    row = input()  # one line of the grid: space " " is floor, pound "#" is wall

# game loop
while True:
    my_pac = []
    my_score, opponent_score = [int(i) for i in input().split()]
    visible_pac_count = int(input())  # all your pacs and enemy pacs in sight
    for i in range(visible_pac_count):
        # pac_id: pac number (unique within a team)
        # mine: true if this pac is yours
        # x: position in the grid
        # y: position in the grid
        # type_id: unused in wood leagues
        # speed_turns_left: unused in wood leagues
        # ability_cooldown: unused in wood leagues
        pac_id, mine, x, y, type_id, speed_turns_left, ability_cooldown = input().split()
        pac_id = int(pac_id)
        mine = mine != "0"

        x = int(x)
        y = int(y)
        speed_turns_left = int(speed_turns_left)
        ability_cooldown = int(ability_cooldown)
        if mine:
            my_pac.append([pac_id, x, y])
    visible_pellet_count = int(input())  # all pellets in sight
    pastilles = []
    for i in range(visible_pellet_count):
        # value: amount of points this pellet is worth
        x, y, value = [int(j) for j in input().split()]
        pastilles.append([x, y, value])

    my_pac_numpy = np.array(my_pac)
    my_pac_df = pd.DataFrame(my_pac_numpy,
                        columns = ['id', 'x', 'y'])
    my_pac_df_blocked = my_pac_df[my_pac_df.duplicated(['x', 'y'])]
    pastilles_numpy = np.array(pastilles)
    pastilles_df = pd.DataFrame(pastilles_numpy,
                        columns = ['x', 'y', 'value'])
    masque_super_pastille = pastilles_df["value"] == 10
    super_pastilles = pastilles_df[masque_super_pastille]
    result = ""
    for i, pac_id in enumerate(my_pac_df['id']):
        if i > 0:
            result += " | " 

        if len(my_pac_df_blocked) > 0 and my_pac_df_blocked['id'].isin([pac_id]):
            index_pastille_cible = len(pastilles_df) - 1
            result_x = pastilles_df.iloc[index_pastille_cible]["x"]
            result_y = pastilles_df.iloc[index_pastille_cible]["y"]
            result += f"MOVE {pac_id} {result_x} {result_y}"
        else:
            if len(super_pastilles) > (0 + i):
                result_x = super_pastilles.iloc[0 + i]["x"]
                result_y = super_pastilles.iloc[0 + i]["y"]
                result += f"MOVE {pac_id} {result_x} {result_y}"
            else :
                #index_pastille_cible = random.randint(0, len(pastilles_df)/2)
                index_pastille_cible = 0 + i 
                result_x = pastilles_df.iloc[index_pastille_cible]["x"]
                result_y = pastilles_df.iloc[index_pastille_cible]["y"]
                result += f"MOVE {pac_id} {result_x} {result_y}"
    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    # MOVE <pacId> <x> <y>
    print(result)
