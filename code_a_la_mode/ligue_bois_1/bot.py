import sys
import math
import random

##############################################################

# util code

# Cells
BLUEBERRIES_CRATE = "B"
ICE_CREAM_CRATE = "I"
WINDOW = "W"
EMPTY_TABLE = "#"
DISHWASHER = "D"
FLOOR_CELL = "."
PLAYER = "0"
OPPONENT = "1"
STRAWBERRIES_CRATE = "S"
CHOPPING_BOARD = "C"
DOUGH_CRATE = "H"
OVEN = "O"

# Items
NONE = "NONE"
DISH = "DISH"
ICE_CREAM = "ICE_CREAM"
BLUEBERRIES = "BLUEBERRIES"
CLOCHE = "CLOCHE"
CHOPPED_STRAWBERRIES = "CHOPPED_STRAWBERRIES"
STRAWBERRIES = "STRAWBERRIES"
CHOPPER = "CHOPPER"
CROISSANT = "CROISSANT"
DOUGH = "DOUGH"
COOKER = "COOKER"
DROPPED_CROISSANT = "DROPPED_CROISSANT"
DROP_CROISSANT = "DROP_CROISSANT"

def log(x):
    print(x, file=sys.stderr)

def item_to_name(item):
    name = FLOOR_CELL
    if item is DISH:
        name = DISHWASHER
    elif item is ICE_CREAM:
        name = ICE_CREAM_CRATE
    elif item is BLUEBERRIES:
        name = BLUEBERRIES_CRATE
    elif item is COOKER:
        name = OVEN
    elif item is DOUGH:
        name = DOUGH_CRATE
    elif item is CLOCHE:
        name = WINDOW
    return name

def getItemByName(name):
    item = NONE
    if name is DISHWASHER:
        item = DISH
    elif name is ICE_CREAM_CRATE:
        item = ICE_CREAM
    elif name is BLUEBERRIES_CRATE:
        item = BLUEBERRIES
    elif name is WINDOW:
        item = CLOCHE
    elif name is STRAWBERRIES_CRATE:
        item = STRAWBERRIES
    elif name is CHOPPING_BOARD:
        item = CHOPPER
    elif name is DOUGH_CRATE:
        item = DOUGH
    elif name is OVEN:
        item = COOKER
    return item
    
def is_player_near_tile(player, tile_near_player):
    is_near = False
    distance = 0
    # calcul distance des x
    if player.x < tile_near_player.x:
        distance = tile_near_player.x - player.x
    else :
        distance = player.x - tile_near_player.x
    # calcul distance des y
    if player.y < tile_near_player.y:
        distance += tile_near_player.y - player.y
    else :
        distance += player.y - tile_near_player.y
    # si l'item est accessible sans mouvement  
    if distance == 0:
        is_near = True
    return is_near
    
def is_player_near_tile_in_one_move(player, tile_near_player):
    is_near = False
    distance = 0
    # calcul distance des x
    if player.x < tile_near_player.x:
        distance = tile_near_player.x - player.x
    else :
        distance = player.x - tile_near_player.x
    # calcul distance des y
    if player.y < tile_near_player.y:
        distance += tile_near_player.y - player.y
    else :
        distance += player.y - tile_near_player.y
    # si la distance entre le player et l'item est accessible en 1 mouvement    
    if distance <= 4:
        is_near = True
    return is_near
    
def get_ditance(x1, x2):
    distance = 0
    # calcul distance des x
    if x1 < x2:
        distance = x2 - x1
    else :
        distance = x1 - x2
    return distance

def is_valid_move(x, y, kitchen):
    liste_tiles = [tile for tile in kitchen if tile.x == x and tile.y == y]
    is_valid = False
    if len(liste_tiles) > 0:
        tile = liste_tiles[0]
        if tile.name is FLOOR_CELL:
            is_valid =  True
     
    return is_valid 
    
def get_move(player, item, kitchen):
    x = player.x
    y = player.y
    
    distance_y = get_ditance(player.y, item.y)
    nb_move = 0
    # gestion des y
    if player.y < item.y:
        for i in range(1,4):
            if is_valid_move(player.x, player.y + i, kitchen):
                nb_move += 1
                y = player.y + i
    else :
        for i in range(1,4):
            if is_valid_move(player.x, player.y - i, kitchen):
                nb_move += 1
                y = player.y - i
    # gestion des x
    if nb_move <= 4 and player.x < item.x:
        for i in range(nb_move,4):
            if is_valid_move(player.x + i, y, kitchen):
                nb_move += 1
                x = player.x + i
    else :
        for i in range(nb_move,4):
            if is_valid_move(player.x - i, y, kitchen):
                nb_move += 1
                x = player.x - i
    return x, y

class Player:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.item = NONE

class Tile:
    def __init__(self, x, y, name):
        self.x = x
        self.y = y
        self.name = name
        self.item = NONE

    def __repr__(self):
        return "Tile: " + str(self.x) + ", " + str(self.y)

class Customer:
    def __init__(self, item, award):
        self.item = item
        self.award = award
        
class Game:
    def __init__(self):
        self.player = Player()
        self.opponent = Player()
        self.kitchen = []
        self.customers = []
        self.dropped_items = []
    
    def add_customer(self, customer_item, customer_award): 
        self.customers.append(Customer(customer_item, customer_award))
    
    def reset_customer(self):
        self.customers = []
        
    def add_kitchen_line(self, line, kitchen_line):
        for x, tileChar in enumerate(kitchen_line):
            self.addTile(x, line, tileChar)
        
    def addTile(self, x, y, tileChar):
        self.kitchen.append(Tile(x, y, tileChar))
            
    def updatePlayer(self, x, y, item):
        self.player.x = x
        self.player.y = y
        self.player.item = item

    def updateOpponent(self, x, y, item):
        self.opponent.x = x
        self.opponent.y = y
        self.opponent.item = item
        
    def get_customer_best_award(self):
        if len(self.customers) > 0:
            max_award = max([customer.award for customer in self.customers])
            return random.choice([customer for customer in self.customers if customer.award == max_award])
        return None
        
    def getTileByName(self, name):
        for t in self.kitchen:
            if t.name == name:
                return t

    def getTileByItem(self, item):
        #for t in [tile for tile in self.kitchen if tile.item != NONE]:
        for t in [tile for tile in self.kitchen]:
            if t.item == item:
                return t
                
    def getTileByCoords(self, x, y):
        for t in self.kitchen:
            if t.x == x and t.y == y:
                return t

    def updateItemInKitchen(self):
        for tile in [tile for tile in game.kitchen if tile.name is not EMPTY_TABLE and tile.name is not FLOOR_CELL]:
            tile.item = getItemByName(tile.name)

    
    def add_tile_to_list(self, tile_x, tile_y, list_tiles):
        tile_near_item = self.getTileByCoords(tile_x, tile_y)
        if tile_near_item.name is FLOOR_CELL or tile_near_item.name is PLAYER or tile_near_item.name is OPPONENT:
            list_tiles.append(tile_near_item) 
        return list_tiles

    def get_tiles_near_item(self, item):
        tile = self.getTileByItem(item)
        #log(f" >> {tile} {tile.name} {tile.item}")
        liste_tiles_near_item = []
        # gestion des cases diag
        if tile.x > 0 and tile.y < 6:
            liste_tiles_near_item = self.add_tile_to_list(tile.x - 1, tile.y + 1, liste_tiles_near_item)
            
        if tile.y > 0 and tile.x < 10:
            liste_tiles_near_item = self.add_tile_to_list(tile.x + 1, tile.y - 1, liste_tiles_near_item)
        
        # gestion des cellules du bord haut et gauche
        if tile.x > 0 and tile.y > 0:
            liste_tiles_near_item = self.add_tile_to_list(tile.x - 1, tile.y - 1, liste_tiles_near_item)
            liste_tiles_near_item = self.add_tile_to_list(tile.x - 1, tile.y, liste_tiles_near_item)
            liste_tiles_near_item = self.add_tile_to_list(tile.x, tile.y - 1, liste_tiles_near_item)
        elif tile.x > 0:
            liste_tiles_near_item = self.add_tile_to_list(tile.x - 1, tile.y , liste_tiles_near_item)
        elif tile.y > 0:
            liste_tiles_near_item = self.add_tile_to_list(tile.x, tile.y - 1, liste_tiles_near_item)

        # gestion des bords bas et droit        
        if tile.x < 10 and tile.y < 6:
            liste_tiles_near_item = self.add_tile_to_list(tile.x + 1, tile.y + 1, liste_tiles_near_item)
            liste_tiles_near_item = self.add_tile_to_list(tile.x + 1, tile.y, liste_tiles_near_item)
            liste_tiles_near_item = self.add_tile_to_list(tile.x, tile.y + 1, liste_tiles_near_item)
        elif tile.x < 10:
            liste_tiles_near_item = self.add_tile_to_list(tile.x + 1, tile.y, liste_tiles_near_item)
        elif tile.y < 6:
            liste_tiles_near_item = self.add_tile_to_list(tile.x, tile.y + 1, liste_tiles_near_item)
        return liste_tiles_near_item   

    def get_tiles_near_tile(self, tile):
        #tile = self.getTileByItem(item)
        #log(f" >> {tile} {tile.name} {tile.item}")
        liste_tiles_near_item = []
        # gestion des cases diag
        if tile.x > 0 and tile.y < 6:
            liste_tiles_near_item = self.add_tile_to_list(tile.x - 1, tile.y + 1, liste_tiles_near_item)
            
        if tile.y > 0 and tile.x < 10:
            liste_tiles_near_item = self.add_tile_to_list(tile.x + 1, tile.y - 1, liste_tiles_near_item)
        
        # gestion des cellules du bord haut et gauche
        if tile.x > 0 and tile.y > 0:
            liste_tiles_near_item = self.add_tile_to_list(tile.x - 1, tile.y - 1, liste_tiles_near_item)
            liste_tiles_near_item = self.add_tile_to_list(tile.x - 1, tile.y, liste_tiles_near_item)
            liste_tiles_near_item = self.add_tile_to_list(tile.x, tile.y - 1, liste_tiles_near_item)
        elif tile.x > 0:
            liste_tiles_near_item = self.add_tile_to_list(tile.x - 1, tile.y , liste_tiles_near_item)
        elif tile.y > 0:
            liste_tiles_near_item = self.add_tile_to_list(tile.x, tile.y - 1, liste_tiles_near_item)

        # gestion des bords bas et droit        
        if tile.x < 10 and tile.y < 6:
            liste_tiles_near_item = self.add_tile_to_list(tile.x + 1, tile.y + 1, liste_tiles_near_item)
            liste_tiles_near_item = self.add_tile_to_list(tile.x + 1, tile.y, liste_tiles_near_item)
            liste_tiles_near_item = self.add_tile_to_list(tile.x, tile.y + 1, liste_tiles_near_item)
        elif tile.x < 10:
            liste_tiles_near_item = self.add_tile_to_list(tile.x + 1, tile.y, liste_tiles_near_item)
        elif tile.y < 6:
            liste_tiles_near_item = self.add_tile_to_list(tile.x, tile.y + 1, liste_tiles_near_item)
        return liste_tiles_near_item       

    def is_dish_recipe_in(self, recipe, list_item):
        is_dish_recipe = False
        for item in list_item:
            #log(item)
            if item.startswith(DISH) and recipe in item:
                is_dish_recipe = True
        return is_dish_recipe
        

    def is_recipe_not_ready(self, liste_recipes, recipe):
        is_not_ready = True
        # Ready dans les cas suivants : 
        # soit la recette "recipe" ne fait pas partie de la recette
        # soit la recette fait parti de la main du chef dans une assiette
        # soit la recette a ete deposee sur une table
        #log(f" ...{recipe} in commande {(recipe not in liste_recipes )}")
        #log(f" ...{'-'.join([DISH,recipe])} in player_items {(self.player.item.startswith(DISH) and recipe in self.player.item)}")
        #log(f"... {'-'.join([DISH,recipe])} in dropped_items {self.is_dish_recipe_in(recipe, self.dropped_items)}")
        #if ((recipe not in liste_recipes )
        #   or ("-".join([DISH,recipe]) in self.player.item) 
        #    or ("-".join([DISH,recipe])  in self.dropped_items)
        #    ):
        if ((recipe not in liste_recipes )
            or (self.player.item.startswith(DISH) and recipe in self.player.item) 
            or (self.is_dish_recipe_in(recipe, self.dropped_items))
            ):
            is_not_ready = False
        #log(f"{recipe} ready ? {not is_not_ready}")
        return is_not_ready

    def is_all_recipes_ready(self, recipes):
        is_all_ready = True
        for i in recipes:
            if i not in self.player.item:
                is_all_ready = False
        return is_all_ready
##############################################################

# main

game = Game()

num_all_customers = int(input())
for i in range(num_all_customers):
    # customer_item: the food the customer is waiting for
    # customer_award: the number of points awarded for delivering the food
    customer_item, customer_award = input().split()
    customer_award = int(customer_award)
    game.add_customer(customer_item, customer_award)
    
for i in range(7):
    kitchen_line = input()
    game.add_kitchen_line(i, kitchen_line)
    #log(kitchen_line)

drop_tile_x, drop_tile_y = 0, 0
if (game.getTileByCoords(5,2).name == EMPTY_TABLE):
    drop_tile_x, drop_tile_y = 5, 2
elif (game.getTileByCoords(6,0).name == EMPTY_TABLE):
    drop_tile_x, drop_tile_y = 6, 0
elif (game.getTileByCoords(6,6).name == EMPTY_TABLE):
    drop_tile_x, drop_tile_y = 6, 6
elif (game.getTileByCoords(0,0).name == EMPTY_TABLE):
    drop_tile_x, drop_tile_y = 0, 0

# game loop
while True:
    turns_remaining = int(input())
    player_x, player_y, player_item = input().split()
    player_x = int(player_x)
    player_y = int(player_y)
    game.updatePlayer(player_x, player_y, player_item)
    partner_x, partner_y, partner_item = input().split()
    partner_x = int(partner_x)
    partner_y = int(partner_y)
    game.updateOpponent(partner_x, partner_y, partner_item)
    num_tables_with_items = int(input())  # the number of tables in the kitchen that currently hold an item
    game.dropped_items = []
    for i in range(num_tables_with_items):
        table_x, table_y, item = input().split()
        table_x = int(table_x)
        table_y = int(table_y)
        #log(f"table {table_x} {table_y} {item}")
        game.getTileByCoords(table_x, table_y).item = item
        game.dropped_items.append(item)
    # oven_contents: ignore until wood 1 league
    oven_contents, oven_timer = input().split()
    #log(f"four :  {oven_contents}")
    oven_timer = int(oven_timer)
    game.reset_customer()
    num_customers = int(input())  # the number of customers currently waiting for food
    for i in range(num_customers):
        customer_item, customer_award = input().split()
        customer_award = int(customer_award)
        game.add_customer(customer_item, customer_award)

    # Mise à jour état du jeu
    game.updateItemInKitchen()

    # ACTION
    # ---------------------------------------

    #log("kitchen : ")
    #for tile in [tile for tile in game.kitchen if tile.name is not EMPTY_TABLE and tile.name is not FLOOR_CELL]:
    #    log(f" {tile} {tile.name} {tile.item}")

    #log("commandes : ")
    #for i in game.customers:
    #    log(f"{i.item} {i.award}")


    

    # recherche du client avec la plus grande recompense 
    # exemple customer.item = DISH-CHOPPED_STRAWBERRIES-ICE-SCREAM-BLUBERRIES
    customer = game.get_customer_best_award()
    log(f"commande client {customer.item}")
    log(f"preparation chef {game.player.item}")
    #customers_croissant = [customer for customer in game.customers if CROISSANT in customer.item and CHOPPED_STRAWBERRIES not in customer.item]
    #if len(customers_croissant) > 0:
    #    max_award = max([customer.award for customer in customers_croissant])
    #    customer = random.choice([customer for customer in customers_croissant if customer.award == max_award])

    #customers_chopped_strawberries = [customer for customer in game.customers if CROISSANT not in customer.item and CHOPPED_STRAWBERRIES in customer.item]
    #if len(customers_chopped_strawberries) > 0:
    #    max_award = max([customer.award for customer in customers_chopped_strawberries])
    #    customer = random.choice([customer for customer in customers_chopped_strawberries if customer.award == max_award])

    liste_recettes = customer.item.split('-')

    items2 = []
    recipe_in_progress = False
    if game.is_recipe_not_ready(liste_recettes, CROISSANT):
        recipe_in_progress = True
        #log("...CROISSANT in progress..")
        items2.append(DISH)
        
        if CROISSANT not in game.player.item:
            items2.append(COOKER)
            if oven_contents == NONE:
                items2.append(DOUGH)
        #log(f"{items2}")

    if game.is_recipe_not_ready(liste_recettes, CHOPPED_STRAWBERRIES) and not recipe_in_progress:
        recipe_in_progress = True
        #log("...CHOPPED_STRAWBERRIES in progress..")
        if CROISSANT in liste_recettes:
            items2.append(DROPPED_CROISSANT)
            items2.append(DROPPED_CROISSANT)
        else :
            items2.append(DISH)
        if CHOPPED_STRAWBERRIES not in game.player.item:
            items2.append(CHOPPER)
        if STRAWBERRIES not in game.player.item:
            items2.append(STRAWBERRIES)
        if CROISSANT in game.player.item:
            items2.append(DROP_CROISSANT)

    if not recipe_in_progress:
        if not game.is_all_recipes_ready(customer.item):
            #log("...autre recette in progress..")
            customer_items = customer.item
            if DISH in game.player.item:
                customer_items = customer_items.replace(DISH, "")
            if CROISSANT in customer_items:
                customer_items = customer_items.replace(CROISSANT, "")
            if CHOPPED_STRAWBERRIES in customer_items:
                customer_items = customer_items.replace(CHOPPED_STRAWBERRIES, "")    
            for i in customer_items.split("-"):
                if i not in game.player.item:
                    items2.append(i)
            if CROISSANT in liste_recettes and CROISSANT not in game.player.item:
                items2.append(DROPPED_CROISSANT)
        else :
            items2.append(CLOCHE)
    

#    if CROISSANT in customer.item:
#        if CROISSANT in game.player.item:
#            customer_items2 = customer.item.replace(CROISSANT, "")
#            customer_items = customer_items2.replace("DISH-", "")
#            items2.append(CLOCHE)
#            items = customer_items.split('-')
#            for i in items:
#                items2.append(i)
#            items2.append(DISH)
#        elif oven_contents != NONE:
#            customer_items2 = customer.item.replace(CROISSANT, "")
#            customer_items = customer_items2.replace("DISH-", "")
#            items2.append(CLOCHE)
#            items = customer_items.split('-')
#            for i in items:
#                items2.append(i)
#            items2.append(DISH)
#            items2.append(COOKER)
#        else : 
#            customer_items2 = customer.item.replace(CROISSANT, "")
#            customer_items = customer_items2.replace("DISH-", "")
#            items2.append(CLOCHE)
#            items = customer_items.split('-')
#            for i in items:
#                items2.append(i)
#            items2.append(DISH)
#            items2.append(COOKER)
#            items2.append(DOUGH)



    # l'ordre des actions 
    # STRAWBERRIES -> CHOPPING_BOARD (=> CHOPPED_STRAWBERRIES) -> depot CHOPPED_STRAWBERRIES -> DISH 
    # -> recup CHOPPED_STRAWBERRIES -> ICE-SCREAM | BLUBERRIES -> WINDOW
    
    #elif CHOPPED_STRAWBERRIES in customer.item:
    #    if CHOPPED_STRAWBERRIES in game.player.item:
    #        customer_items2 = customer.item.replace(CHOPPED_STRAWBERRIES, "")
    #        customer_items = customer_items2.replace("DISH-", "")
    #        items2.append(CLOCHE)
    #        items = customer_items.split('-')
    #        for i in items:
    #            items2.append(i)
    #        items2.append(DISH)
    #    else : 
    #        customer_items2 = customer.item.replace(CHOPPED_STRAWBERRIES, "")
    #        customer_items = customer_items2.replace("DISH-", "")
    #        items2.append(CLOCHE)
    #        items = customer_items.split('-')
    #        for i in items:
    #            items2.append(i)
    #        items2.append(CHOPPED_STRAWBERRIES)
    #        items2.append(DISH)
    #        items2.append(CHOPPER)
    #        items2.append(STRAWBERRIES)
    #else : 
    #    customer_items = customer.item.replace(DISH, "")
    #    items2.append(CLOCHE)
    #    for i in customer_items:
    #        items2.append(i)
    #    items2.append(DISH)



    # recherche de l'ingrédient manquant dans les mains du chef
    # remplacement CHOPPED_STRAWBERRIES -> STRAWBERRIES-CHOPPING_BOARD
    #customer_items2 = customer.item.replace("CHOPPED_STRAWBERRIES", "DISH-CHOPPING_BOARD-STRAWBERRIES")
    #customer_items = customer_items2.replace("DISH-","")
    #items = customer_items.split('-')
    

    
    #items2 = []
    #items2.append(CLOCHE)
    #for i in items:
    #    items2.append(i)
    
  
    #tile_drop_chopped_strawberries = game.getTileByCoords(6, 0)
    #if tile_drop_chopped_strawberries.name == EMPTY_TABLE:

        #remplacement CHOPPED_STRAWBERRIES -> STRAWBERRIES-CHOPPING_BOARD
    player_items = game.player.item.replace(CHOPPED_STRAWBERRIES, CHOPPER)
    for item in items2:
        if player_items is NONE or item not in player_items:
            if item == DROPPED_CROISSANT:
                tile = game.getTileByCoords(drop_tile_x, drop_tile_y)
            elif item == DROP_CROISSANT:
                tile = game.getTileByCoords(drop_tile_x, drop_tile_y)
            else :
                tile = game.getTileByItem((item))
            #log(f" >> item à recuperer : {item}")
    #else:
    #    tile = tile_drop_chopped_strawberries
    
    #log(f" >> choix {tile} {tile.name} {tile.item}")
    
    # recherche coordonnées pour acceder à l'ingrédient
    #liste_tiles_near_item = game.get_tiles_near_item(tile.item)
    liste_tiles_near_item = game.get_tiles_near_tile(tile)
    #for i in liste_tiles_near_item:
    #    log(f" floor proche tile {i}")
    
    is_USE_action = False
    is_MOVE_action = False
    
    # si coordonnées chef ok, alors recuperation ingredient
    liste_tiles_near_player = [tile_near_player for tile_near_player in liste_tiles_near_item if is_player_near_tile(game.player, tile_near_player)]
    #for i in liste_tiles_near_player:
    #    log(f" floor proche tile et player {i}")

    if len(liste_tiles_near_player) > 0:
        is_USE_action = True
        print(f"USE {tile.x} {tile.y}") 
         
    # si chef éloigné (1 seul mouvement), alors recherche du chemin
    if not is_USE_action:
        tile = random.choice(liste_tiles_near_item)
        print(f"MOVE {tile.x} {tile.y}") 

 