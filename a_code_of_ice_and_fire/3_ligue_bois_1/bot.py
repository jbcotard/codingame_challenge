import sys
import math
import random

# MAP SIZE
WIDTH = 12
HEIGHT = 12

# OWNER
ME = 0
OPPONENT = 1

# BUILDING TYPE
HQ = 0
MINE = 1

# TYPE Cell
CELL_VOID = "#"
CELL_NEUTRAL = "."
CELL_OWN_ACTIVE = "O"
CELL_OWN_INACTIVE = "o"
CELL_OPPONENT_ACTIVE = "X"
CELL_OPPONENT_INACTIVE = "x"

def log(x):
    print(x, file=sys.stderr)



class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return str(self.x) + ", " + str(self.y) 

class Cell:
    def __init__(self, x, y, type_cell):
        self.pos = Position(x, y)
        self.type_cell = type_cell

    def __repr__(self):
        return "Cell: " + str(self.pos) + ", " + str(self.type_cell)


class Unit:
    def __init__(self, owner, id, level, x, y):
        self.owner = owner
        self.id = id
        self.level = level
        self.pos = Position(x, y)

    def __repr__(self):
        return "Unit: " + str(self.owner) + ", " + str(self.id) + ", " + str(self.level) + ", " + str(self.pos)

class Building:
    def __init__(self, owner, type, x, y):
        self.owner = owner
        self.type = type
        self.pos = Position(x, y)

class Minespot:
    def __init__(self, x, y):
        self.pos = Position(x, y)
    def __repr__(self):
        return "Minespot: " + str(self.pos.x) + ", " + str(self.pos.y) 

class Game:
    def __init__(self):
        self.buildings = []
        self.units = []
        self.actions = []
        self.board = []
        self.minespots = []
        self.gold = 0
        self.income = 0
        self.opponent_gold = 0
        self.opponent_income = 0
        self.epsilon = 0.2


    def get_my_HQ(self):
        for b in self.buildings:
            if b.type == HQ and b.owner == ME:
                return b


    def get_opponent_HQ(self):
        for b in self.buildings:
            if b.type == HQ and b.owner == OPPONENT:
                return b

    def get_type_cell_on_board(self, x, y):
        type_cell = CELL_NEUTRAL
        pos = Position(x, y)
        for cell_in_board in self.board:
            if cell_in_board.pos.x == pos.x and cell_in_board.pos.y == pos.y :
                type_cell = cell_in_board.type_cell
        return type_cell

    def get_available_position(self, unit):
        available_pos = []
        if unit.pos.y < 11 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1) != CELL_VOID :
            available_pos.append(Position(unit.pos.x, unit.pos.y + 1))
        if unit.pos.x < 11 and self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y) != CELL_VOID :
            available_pos.append(Position(unit.pos.x + 1, unit.pos.y))
        if unit.pos.y > 0 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y - 1) != CELL_VOID :
            available_pos.append(Position(unit.pos.x, unit.pos.y - 1))
        if unit.pos.x > 0 and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) != CELL_VOID :
            available_pos.append(Position(unit.pos.x - 1, unit.pos.y))
        return available_pos

    def get_free_position(self, unit):
        free_pos = []
        if unit.pos.y < 11 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1) == CELL_NEUTRAL :
            free_pos.append(Position(unit.pos.x, unit.pos.y + 1))
        if unit.pos.x < 11 and self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y) == CELL_NEUTRAL :
            free_pos.append(Position(unit.pos.x + 1, unit.pos.y))
        if unit.pos.y > 0 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y - 1) == CELL_NEUTRAL :
            free_pos.append(Position(unit.pos.x, unit.pos.y - 1))
        if unit.pos.x > 0 and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) == CELL_NEUTRAL :
            free_pos.append(Position(unit.pos.x - 1, unit.pos.y))
        return free_pos

    def get_valid_position(self, unit):
        #available_pos = self.get_available_position(unit)
        available_pos = self.get_free_position(unit)
        if len(available_pos) == 0:
            available_pos = self.get_available_position(unit)
        pos = random.choice(available_pos)
        return pos

    def get_valid_position2(self, unit):
        next_x = unit.pos.x
        next_y = unit.pos.y

        #log(f"{unit.pos.x} {unit.pos.y}")
        #if unit.pos.y < 11:
        #    log(self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1))
        #if unit.pos.x < 11:
        #    log(self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y))

        if self.get_my_HQ().pos.x == 0 and unit.pos.y < 11 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1) != CELL_VOID :
            next_y = unit.pos.y + 1
        elif self.get_my_HQ().pos.x == 0 and unit.pos.x < 11 and self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y) != CELL_VOID :
            next_x = unit.pos.x + 1
        elif self.get_my_HQ().pos.x == 0 and unit.pos.y > 0 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y - 1) != CELL_VOID :
            next_y = unit.pos.y - 1
        elif self.get_my_HQ().pos.x == 0 and unit.pos.x > 0 and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) != CELL_VOID :
            next_x = unit.pos.x - 1
        elif self.get_my_HQ().pos.x == 11 and unit.pos.y > 0 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y - 1) != CELL_VOID :
            next_y = unit.pos.y - 1
        elif self.get_my_HQ().pos.x == 11 and unit.pos.x > 0 and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) != CELL_VOID and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) != CELL_OWN_ACTIVE:
            next_x = unit.pos.x - 1
        elif self.get_my_HQ().pos.x == 11 and unit.pos.y < 11 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1) != CELL_VOID :
            next_y = unit.pos.y + 1
        elif self.get_my_HQ().pos.x == 11 and unit.pos.x < 11 and self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y) != CELL_VOID :
            next_x = unit.pos.x + 1


        return Position(next_x, next_y)

    def get_center_pos(self):
        pos = Position(5, 5)
        if self.get_type_cell_on_board(5, 5) == CELL_VOID:
            if self.get_type_cell_on_board(6, 5) == CELL_VOID:
                pos = Position(6, 5) 
            elif self.get_type_cell_on_board(6, 6) == CELL_VOID:
                pos = Position(6, 6) 
            elif self.get_type_cell_on_board(5, 6) == CELL_VOID:
                pos = Position(5, 6)
            else:
                pos = Position(4, 4) 
        return pos

    def move_units(self):
        center_pos = self.get_center_pos()
        for i, unit in enumerate(self.get_my_units()):
            # the first unit have to go to the opponent HQ
            if i == 0:
                next_pos = center_pos
            elif random.random() < self.epsilon: # explore!
                next_pos = self.get_valid_position(unit)
            else:
                next_pos = self.get_valid_position2(unit)
            self.actions.append(f'MOVE {unit.id} {next_pos.x} {next_pos.y}')


    def get_train_position(self):
        hq = self.get_my_HQ()

        if hq.pos.x == 0:
            return Position(0, 1)
        return Position(11, 10)

    def get_my_units(self):
        return [unit for unit in self.units if unit.owner == ME]

    def train_units(self):
        #train_pos = self.get_train_position()
        # to train the first unit
        if len(self.get_my_units()) < 4:
            train_pos = self.get_train_position()
            if self.gold > 20:
                self.actions.append(f'TRAIN 1 {train_pos.x} {train_pos.y}')

        # with unit already trained once
        for unit in self.get_my_units():
            next_pos = self.get_valid_position(unit)
            if self.gold > 50 and unit.level == 1 and  len(self.get_my_units()) < 12:
                self.actions.append(f'TRAIN 2 {next_pos.x} {next_pos.y}')
                #self.actions.append(f'TRAIN 2 {train_pos.x} {train_pos.y}')

        #train_pos = self.get_train_position()

        #if self.gold > 30:
        #    self.actions.append(f'TRAIN 1 {train_pos.x} {train_pos.y}')


    def init(self):
        # Unused in Wood 3
        number_mine_spots = int(input())
        for i in range(number_mine_spots):
            x, y = [int(j) for j in input().split()]
            self.minespots.append(Minespot(x,y)) 


    def update(self):
        self.units.clear()
        self.buildings.clear()
        self.actions.clear()
        self.board.clear()

        self.gold = int(input())
        self.income = int(input())
        self.opponent_gold = int(input())
        self.opponent_income = int(input())

        for y in range(12):
            line = input()
            log(line)
            for x in range(12):
                self.board.append(Cell(x, y, line[x]))

        building_count = int(input())
        for i in range(building_count):
            owner, building_type, x, y = [int(j) for j in input().split()]
            self.buildings.append(Building(owner, building_type, x, y))

        unit_count = int(input())
        for i in range(unit_count):
            owner, unit_id, level, x, y = [int(j) for j in input().split()]
            self.units.append(Unit(owner, unit_id, level, x, y))

    def is_minespot_free_on_my_territory(self, minespot):
        is_free_and_on_my_territory = False

        type_cell = self.get_type_cell_on_board(minespot.pos.x, minespot.pos.y) 

        if type_cell == CELL_OWN_ACTIVE or type_cell == CELL_OWN_INACTIVE:
            
            if len([building for building in self.buildings if building.type == MINE and (building.pos.x != minespot.pos.x or building.pos.y != minespot.pos.y)]) == 0:
                is_free_and_on_my_territory = True
            
        return is_free_and_on_my_territory

    def build_buildings(self):
        list_minespot_free = [ minespot_free for minespot_free in self.minespots if self.is_minespot_free_on_my_territory(minespot_free)]

        if len(list_minespot_free) > 0 and self.gold > (20 + (4 * len(self.buildings))):
            self.actions.append(f'BUILD MINE {list_minespot_free[0].pos.x} {list_minespot_free[0].pos.y}')

    def build_output(self):
        # TODO "core" of the AI
        self.train_units()
        self.move_units()
        self.build_buildings()


    def output(self):
        if self.actions:
            print(';'.join(self.actions))
        else:
            print('WAIT')


g = Game()

g.init()
while True:
    g.update()
    g.build_output()
    g.output()