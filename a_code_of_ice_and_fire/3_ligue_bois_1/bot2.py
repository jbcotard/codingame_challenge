import sys
import math
import random

# MAP SIZE
WIDTH = 12
HEIGHT = 12

# OWNER
ME = 0
OPPONENT = 1

# BUILDING TYPE
HQ = 0
MINE = 1

# TYPE Cell
CELL_VOID = "#"
CELL_NEUTRAL = "."
CELL_OWN_ACTIVE = "O"
CELL_OWN_INACTIVE = "o"
CELL_OPPONENT_ACTIVE = "X"
CELL_OPPONENT_INACTIVE = "x"

def log(x):
    print(x, file=sys.stderr)



class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return str(self.x) + ", " + str(self.y) 

class Cell:
    def __init__(self, x, y, type_cell):
        self.pos = Position(x, y)
        self.type_cell = type_cell

    def __repr__(self):
        return "Cell: " + str(self.pos) + ", " + str(self.type_cell)


class Unit:
    def __init__(self, owner, id, level, x, y):
        self.owner = owner
        self.id = id
        self.level = level
        self.pos = Position(x, y)

    def __repr__(self):
        return "Unit: " + str(self.owner) + ", " + str(self.id) + ", " + str(self.level) + ", " + str(self.pos)

class MovingUnit:
    def __init__(self, id, x, y):
        self.id = id
        self.target = Position(x, y)

    def __repr__(self):
        return "MovingUnit: " + str(self.id)  + ", " + str(self.target)


class Building:
    def __init__(self, owner, type, x, y):
        self.owner = owner
        self.type = type
        self.pos = Position(x, y)

class Minespot:
    def __init__(self, x, y):
        self.pos = Position(x, y)
    def __repr__(self):
        return "Minespot: " + str(self.pos.x) + ", " + str(self.pos.y) 

class Game:
    def __init__(self, max_units_l1=4, max_units_l2=4, max_units_l3=1, min_gold_train_l1=20, min_gold_train_l2=40, min_gold_train_l3=200, min_gold_build=15, tx_exploration=0.2, tx_hq=0.5, tx_base=0.1, tx_minespot=0.2):
        self.buildings = []
        self.units = []
        self.actions = []
        self.board = []
        self.minespots = []
        self.moving_units = []
        self.gold = 0
        self.income = 0
        self.opponent_gold = 0
        self.opponent_income = 0
        self.max_units_l1 = max_units_l1
        self.max_units_l2 = max_units_l2
        self.max_units_l3 = max_units_l3
        self.min_gold_train_l1 = min_gold_train_l1
        self.min_gold_train_l2 = min_gold_train_l2
        self.min_gold_train_l3 = min_gold_train_l3
        self.min_gold_build = min_gold_build
        self.tx_hq = tx_hq
        self.tx_base = tx_base
        self.tx_minespot = tx_minespot
        self.tx_exploration = tx_exploration


    def get_my_HQ(self):
        for b in self.buildings:
            if b.type == HQ and b.owner == ME:
                return b


    def get_opponent_HQ(self):
        for b in self.buildings:
            if b.type == HQ and b.owner == OPPONENT:
                return b

    def get_type_cell_on_board(self, x, y):
        type_cell = CELL_NEUTRAL
        pos = Position(x, y)
        for cell_in_board in self.board:
            if cell_in_board.pos.x == pos.x and cell_in_board.pos.y == pos.y :
                type_cell = cell_in_board.type_cell
        return type_cell

    def get_available_position(self, unit):
        available_pos = []
        if unit.pos.y < 11 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1) != CELL_VOID :
            available_pos.append(Position(unit.pos.x, unit.pos.y + 1))
        if unit.pos.x < 11 and self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y) != CELL_VOID :
            available_pos.append(Position(unit.pos.x + 1, unit.pos.y))
        if unit.pos.y > 0 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y - 1) != CELL_VOID :
            available_pos.append(Position(unit.pos.x, unit.pos.y - 1))
        if unit.pos.x > 0 and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) != CELL_VOID :
            available_pos.append(Position(unit.pos.x - 1, unit.pos.y))
        return available_pos

    def get_free_position(self, unit):
        free_pos = []
        if unit.pos.y < 11 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1) == CELL_NEUTRAL :
            free_pos.append(Position(unit.pos.x, unit.pos.y + 1))
        if unit.pos.x < 11 and self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y) == CELL_NEUTRAL :
            free_pos.append(Position(unit.pos.x + 1, unit.pos.y))
        if unit.pos.y > 0 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y - 1) == CELL_NEUTRAL :
            free_pos.append(Position(unit.pos.x, unit.pos.y - 1))
        if unit.pos.x > 0 and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) == CELL_NEUTRAL :
            free_pos.append(Position(unit.pos.x - 1, unit.pos.y))
        return free_pos

    def get_position_for_exploration(self, unit):
        available_pos = [cell_in_board.pos for cell_in_board in self.board if cell_in_board.type_cell == CELL_NEUTRAL]
        if len(available_pos) == 0:
            available_pos = self.get_available_position(unit)
        pos = random.choice(available_pos)
        return pos

    def get_valid_position2(self, unit):
        next_x = unit.pos.x
        next_y = unit.pos.y

        #log(f"{unit.pos.x} {unit.pos.y}")
        #if unit.pos.y < 11:
        #    log(self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1))
        #if unit.pos.x < 11:
        #    log(self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y))

        if self.get_my_HQ().pos.x == 0 and unit.pos.y < 11 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1) != CELL_VOID :
            next_y = unit.pos.y + 1
        elif self.get_my_HQ().pos.x == 0 and unit.pos.x < 11 and self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y) != CELL_VOID :
            next_x = unit.pos.x + 1
        elif self.get_my_HQ().pos.x == 0 and unit.pos.y > 0 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y - 1) != CELL_VOID :
            next_y = unit.pos.y - 1
        elif self.get_my_HQ().pos.x == 0 and unit.pos.x > 0 and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) != CELL_VOID :
            next_x = unit.pos.x - 1
        elif self.get_my_HQ().pos.x == 11 and unit.pos.y > 0 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y - 1) != CELL_VOID :
            next_y = unit.pos.y - 1
        elif self.get_my_HQ().pos.x == 11 and unit.pos.x > 0 and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) != CELL_VOID and self.get_type_cell_on_board(unit.pos.x - 1, unit.pos.y) != CELL_OWN_ACTIVE:
            next_x = unit.pos.x - 1
        elif self.get_my_HQ().pos.x == 11 and unit.pos.y < 11 and self.get_type_cell_on_board(unit.pos.x, unit.pos.y + 1) != CELL_VOID :
            next_y = unit.pos.y + 1
        elif self.get_my_HQ().pos.x == 11 and unit.pos.x < 11 and self.get_type_cell_on_board(unit.pos.x + 1, unit.pos.y) != CELL_VOID :
            next_x = unit.pos.x + 1


        return Position(next_x, next_y)

    def get_center_pos(self):
        pos = Position(5, 5)
        if self.get_type_cell_on_board(5, 5) == CELL_VOID:
            if self.get_type_cell_on_board(6, 5) == CELL_VOID:
                pos = Position(6, 5) 
            elif self.get_type_cell_on_board(6, 6) == CELL_VOID:
                pos = Position(6, 6) 
            elif self.get_type_cell_on_board(5, 6) == CELL_VOID:
                pos = Position(5, 6)
            else:
                pos = Position(4, 4) 
        return pos

    def is_unit_moving(self, unit):
        is_moving = False
        if unit.id in [unit.id for unit in self.moving_units]:
            is_moving = True    
        return is_moving
    
    def get_target_position(self, unit):
        target = None
        for moving_unit in self.moving_units:
            if moving_unit.id == unit.id:
                target = moving_unit.target    
        return target
        
    def move_units(self):

        for i, unit in enumerate(self.get_my_units()):
            next_pos = None
            if self.is_unit_moving(unit):
                log('moving')
                next_pos = self.get_target_position(unit)
            else :
                if random.random() < self.tx_exploration: # explore!
                    log('explore')
                    next_pos = self.get_position_for_exploration(unit)
                elif random.random() < self.tx_hq: # move to the hq ennemy
                    log('hq ')
                    hq = self.get_opponent_HQ()
                    next_pos = Position(hq.pos.x, hq.pos.y)
                elif random.random() < self.tx_base: # move to the base
                    log('base')
                    hq = self.get_my_HQ()
                    if hq.pos.x == 0:
                        next_pos = Position(1, 0)
                    else: 
                        next_pos = Position(10, 11)
                elif random.random() < self.tx_minespot: # move to a minspot
                    log('minespot')                
                    list_minespot_free = [ minespot_free for minespot_free in self.minespots if self.is_minespot_free_on_my_territory(minespot_free)]
                    if len(list_minespot_free) > 0:
                        minespot = random.choice(list_minespot_free)
                        next_pos = minespot.pos
                else :
                    next_pos = self.get_valid_position2(unit)
            if next_pos is not None:
                self.moving_units.append(MovingUnit(unit.id, next_pos.x, next_pos.y))
                self.actions.append(f'MOVE {unit.id} {next_pos.x} {next_pos.y}')

    def get_train_position(self):
        hq = self.get_my_HQ()

        if hq.pos.x == 0:
            return Position(0, 1)
        return Position(11, 10)

    def get_my_units(self):
        return [unit for unit in self.units if unit.owner == ME]

    def train_units(self):
        # all units trained near base
        train_pos = self.get_train_position()


        # unit level 3
        if len([units_l3 for units_l3 in self.get_my_units() if units_l3.level == 3]) < self.max_units_l3 and self.gold > self.min_gold_train_l3:
            self.actions.append(f'TRAIN 3 {train_pos.x} {train_pos.y}')

        # unit level 2
        if len([units_l2 for units_l2 in self.get_my_units() if units_l2.level == 2]) < self.max_units_l2 and self.gold > self.min_gold_train_l2:
            self.actions.append(f'TRAIN 2 {train_pos.x} {train_pos.y}')

        # unit level 1
        if len([units_l1 for units_l1 in self.get_my_units() if units_l1.level == 1]) < self.max_units_l1 and self.gold > self.min_gold_train_l1:
            self.actions.append(f'TRAIN 1 {train_pos.x} {train_pos.y}')



    def init(self):
        # Unused in Wood 3
        number_mine_spots = int(input())
        for i in range(number_mine_spots):
            x, y = [int(j) for j in input().split()]
            self.minespots.append(Minespot(x,y)) 


    def update(self):
        self.units.clear()
        self.buildings.clear()
        self.actions.clear()
        self.board.clear()

        self.gold = int(input())
        self.income = int(input())
        self.opponent_gold = int(input())
        self.opponent_income = int(input())

        for y in range(12):
            line = input()
            log(line)
            for x in range(12):
                self.board.append(Cell(x, y, line[x]))

        building_count = int(input())
        for i in range(building_count):
            owner, building_type, x, y = [int(j) for j in input().split()]
            self.buildings.append(Building(owner, building_type, x, y))

        unit_count = int(input())
        for i in range(unit_count):
            owner, unit_id, level, x, y = [int(j) for j in input().split()]
            self.units.append(Unit(owner, unit_id, level, x, y))
        
        log(self.moving_units)
        moving_units = []
        for moving_unit in self.moving_units:
            for unit in [unit for unit in self.units if unit.id == moving_unit.id]:
                if unit.pos.x != moving_unit.target.x or unit.pos.y != moving_unit.target.y:
                    moving_units.append(moving_unit)
        self.moving_units.clear()
        self.moving_units = moving_units
        log(self.moving_units)
                    
            
            
    def is_minespot_free_on_my_territory(self, minespot):
        is_free_and_on_my_territory = False

        type_cell = self.get_type_cell_on_board(minespot.pos.x, minespot.pos.y) 

        if type_cell == CELL_OWN_ACTIVE or type_cell == CELL_OWN_INACTIVE:
            
            if len([building for building in self.buildings if building.type == MINE and (building.pos.x != minespot.pos.x or building.pos.y != minespot.pos.y)]) == 0:
                is_free_and_on_my_territory = True
            
        return is_free_and_on_my_territory

    def build_buildings(self):
        list_minespot_free = [ minespot_free for minespot_free in self.minespots if self.is_minespot_free_on_my_territory(minespot_free)]

        if len(list_minespot_free) > 0 and self.gold > self.min_gold_build:
            self.actions.append(f'BUILD MINE {list_minespot_free[0].pos.x} {list_minespot_free[0].pos.y}')

    def build_output(self):
        # TODO "core" of the AI
        self.train_units()
        self.move_units()
        self.build_buildings()


    def output(self):
        if self.actions:
            print(';'.join(self.actions))
        else:
            print('WAIT')


g = Game(max_units_l1=20, max_units_l2=20, max_units_l3=1, min_gold_train_l1=20, min_gold_train_l2=25, min_gold_train_l3=200, min_gold_build=15, tx_exploration=0.2, tx_hq=0.2, tx_base=0.2, tx_minespot=0.4)

g.init()
while True:
    g.update()
    g.build_output()
    g.output()