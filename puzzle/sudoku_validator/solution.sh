# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.


check_unique_chars() {
    input="$1"
    # Utiliser un tableau associatif pour compter les occurrences
    declare -A count

    # Parcourir chaque caractère de la chaîne
    for char in $(echo $input | tr ' ' '\n'); do
        ((count[$char]++))
    done

    # Vérifier si au moins un caractère a plus d'une occurrence
    for key in "${!count[@]}"; do
        if [ "${count[$key]}" -gt 1 ]; then
            #echo "Pas ok : le caractère '$key' apparaît ${count[$key]} fois."
            return 1
        fi
    done

    #echo "Ok : tous les caractères sont uniques."
    return 0
}

check_duplicates() {
    local input="$1"
    local words=($input)
    local word_count=()

    # Compter les occurrences de chaque mot
    #for word in "${words[@]}"; do
    #    ((word_count[word]++))
    #done

    if [[ $string == *" "* ]]; then
        for char in $(echo $input | tr ' ' '\n'); do
            ((word_count[$char]++))
        done
    else 
        for ((i=0; i<${#input}; i++)); do
            char="${input:$i:1}"
            ((word_count[$char]++))
        done
    fi

    #echo "Debug messages... ${input} - ${word_count[@]}" >&2
    
    # Vérifier si au moins un mot apparaît plus d'une fois
    for count in "${word_count[@]}"; do
        if ((count > 1)); then
           #return "true"
           return 0
        fi
    done
    
    #return "false"
    return 1
}

declare -a lignes
declare -a colonnes
n=""
lignes=("" "" "" "" "" "" "" "" "" "")
for (( i=0; i<9; i++ )); do
    read -r -a myArray
    echo "Debug input ... ${myArray[@]}" >&2
    #lignes+=("${myArray}")
    #colonnes[i]+=(myArray[@])
    for (( j=0; j<9; j++ )); do
        n+="${myArray[$((j))]} "
        #echo "Debug messages... $n" >&2
    done
    n+=","
    #colonnes+=($n)
    
done
IFS=',' read -r -a colonnes <<< "$n"


for j in "${!colonnes[@]}"; do
    string=$(echo "${colonnes[j]}" | sed 's/^[ \t]*//;s/[ \t]*$//')
    string=$(echo "${string}" | tr -d ' ' )
    #echo "Debug ... ${string}" >&2
    for (( i=0 ; i < ${#string} ; i++ )); do
        lignes[$i]+="${string:$i:1} "
        #echo "Debug lignes... lignes[$i]=${lignes[$i]}" >&2
    done
done


#echo "Debug lignes... ${lignes[@]}" >&2
#echo "Debug colones... ${colonnes[@]}" >&2
#echo "Debug colone 1... ${colonnes[0]}" >&2

for i in "${!lignes[@]}"; do
    echo "Debug input reversed ... ${lignes[i]}" >&2
done

# Write an answer using echo
# To debug: echo "Debug messages..." >&2


#echo "Debug messages... $(check_duplicates '6 8 5 5 7 1 4 9 3')" >&2

solution="true"
for i in "${!colonnes[@]}"; do
    #echo "check ${colonnes[i]} ... " >&2
    #check_duplicates "${colonnes[i]}"
    #echo "check ${colonnes[i]} ... $?" >&2
    #check_unique_chars "${colonnes[i]}"
    #echo "check ${colonnes[i]} ... $?" >&2

    if ! check_unique_chars "${colonnes[i]}" ; then
        echo "check ${colonnes[i]} ... false" >&2
        solution="false"
    fi;
done

for i in "${!lignes[@]}"; do

    #check_duplicates "${lignes[i]}"
    #echo "check ${lignes[i]} ... $?" >&2
    #check_unique_chars "${lignes[i]}"
    #echo "check ${lignes[i]} ... $?" >&2

    if ! check_unique_chars "${lignes[i]}" ; then
        echo "check ${lignes[i]} ... false" >&2
        solution="false"
    fi;
done


for (( i=0; i<9; i++ )); do
    n=""
    n+="${string:$i:1} "
done

n=""
n+=${colonnes[0]:0:6}
n+=${colonnes[1]:0:6}
n+=${colonnes[2]:0:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;

n=""
n+=${colonnes[0]:6:6}
n+=${colonnes[1]:6:6}
n+=${colonnes[2]:6:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;

n=""
n+=${colonnes[0]:12:6}
n+=${colonnes[1]:12:6}
n+=${colonnes[2]:12:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;

n=""
n+=${colonnes[3]:0:6}
n+=${colonnes[4]:0:6}
n+=${colonnes[5]:0:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;

n=""
n+=${colonnes[3]:6:6}
n+=${colonnes[4]:6:6}
n+=${colonnes[5]:6:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;

n=""
n+=${colonnes[3]:12:6}
n+=${colonnes[4]:12:6}
n+=${colonnes[5]:12:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;


n=""
n+=${colonnes[6]:0:6}
n+=${colonnes[7]:0:6}
n+=${colonnes[8]:0:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;

n=""
n+=${colonnes[6]:6:6}
n+=${colonnes[7]:6:6}
n+=${colonnes[8]:6:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;

n=""
n+=${colonnes[6]:12:6}
n+=${colonnes[7]:12:6}
n+=${colonnes[8]:12:6}
echo "Debug input zone ... ${n}" >&2
if ! check_unique_chars "${n}" ; then
    echo "check ${n} ... false" >&2
    solution="false"
fi;


echo "$solution"