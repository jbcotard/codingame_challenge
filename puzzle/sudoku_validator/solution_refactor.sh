#!/bin/bash

check_unique_chars() {
    input="$1"
    # Utiliser un tableau associatif pour compter les occurrences
    declare -A count

    # Parcourir chaque caractère de la chaîne
    for char in $(echo $input | tr ' ' '\n'); do
        ((count[$char]++))
    done

    # Vérifier si au moins un caractère a plus d'une occurrence
    for key in "${!count[@]}"; do
        if [ "${count[$key]}" -gt 1 ]; then
            #echo "Pas ok : le caractère '$key' apparaît ${count[$key]} fois."
            return 1
        fi
    done

    #echo "Ok : tous les caractères sont uniques."
    return 0
}

declare -a lignes colonnes zones
n=""
lignes=("" "" "" "" "" "" "" "" "" "")
for (( i=0; i<9; i++ )); do
    read -r -a myArray
    echo "Debug input ... ${myArray[@]}" >&2
    for (( j=0; j<9; j++ )); do
        n+="${myArray[$((j))]} "
    done
    n+=","
    
done
IFS=',' read -r -a colonnes <<< "$n"

# Construire les colonnes
for j in "${!colonnes[@]}"; do
    string=$(echo "${colonnes[j]}" | sed 's/^[ \t]*//;s/[ \t]*$//')
    string=$(echo "${string}" | tr -d ' ' )
    for (( i=0 ; i < ${#string} ; i++ )); do
        lignes[$i]+="${string:$i:1} "
    done
done

# Construire les zones
for ((i=0; i<9; i+=3)); do
    for ((j=0; j<9; j+=3)); do
        zone="${colonnes[i]:j:3}${colonnes[i+1]:j:3}${colonnes[i+2]:j:3}"
        zones+=("$zone")
    done
done

for i in "${!lignes[@]}"; do
    echo "Debug input reversed ... ${lignes[i]}" >&2
done

# Valider les lignes, colonnes et zones
solution="true"
for group in "${lignes[@]}" "${colonnes[@]}" "${zones[@]}"; do
    if ! check_unique_chars "$group"; then
        solution="false"
    fi
done

echo "$solution"
