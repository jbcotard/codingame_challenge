#!/bin/bash

# Define color codes
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color


# Function to run a test case
run_test() {
    local test_name=$1
    local expected_result=$2
    local input=$3

    echo -e "\n--- Running test: $test_name ---"
    result=$(./solution.sh <<< "$input")

    if [ "$result" == "$expected_result" ]; then
        echo -e "${GREEN}PASS${NC}: Test '$test_name' passed"
    else
        echo -e "${RED}FAIL${NC}: Test '$test_name' failed"
        echo "Expected: $expected_result"
        echo "Got: $result"
    fi
}


# test 1 - OK 
run_test "test 1 grille valide" "true" "1 2 3 4 5 6 7 8 9
4 5 6 7 8 9 1 2 3
7 8 9 1 2 3 4 5 6
9 1 2 3 4 5 6 7 8
3 4 5 6 7 8 9 1 2
6 7 8 9 1 2 3 4 5
8 9 1 2 3 4 5 6 7
2 3 4 5 6 7 8 9 1
5 6 7 8 9 1 2 3 4
"

# test 2 - KO
run_test "test 2 grille invalide" "false" "1 2 3 4 5 6 7 8 9
4 5 6 7 8 9 1 2 3
7 8 9 1 2 3 4 5 6
9 1 2 3 4 5 6 7 8
3 4 5 6 7 8 9 1 2
6 7 8 9 1 2 3 4 5
8 9 1 2 3 4 5 6 7
2 3 4 5 6 7 8 9 1
5 6 7 8 9 1 2 3 1
"
