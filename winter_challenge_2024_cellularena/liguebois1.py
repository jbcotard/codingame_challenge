import sys
import math

# Grow and multiply your organisms to end up larger than your opponent.

class Case:
    def __init__(self, x, y, type_case, owner):
        self.x = x
        self.y = y
        self.type_case = type_case  # WALL, ROOT, BASIC, TENTACLE, HARVESTER, SPORER, A, B, C, D, EMPTY
        self.owner = owner # 1 if your organ, 0 if enemy organ, -1 if neither
        self.id = 0
        self.root_id = -1
        self.parent_id = -1

    def __str__(self):
        return f"Case({self.x}, {self.y}): {self.type_case}, {self.owner}"

# Création d'un plateau de jeu
class GameGrid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.cases = [[Case(x, y, 'EMPTY', -1) for y in range(height)] for x in range(width)]
        self.my_a = 0
        self.my_b = 0
        self.my_c = 0
        self.my_d = 0
        self.roots = []

    def get_case(self, x, y):
        return self.cases[x][y]

    def set_origin_root(self, x, y):
        self.roots.append(self.cases[x][y])

    def set_my_protein(self, my_a, my_b, my_c, my_d):
        self.my_a = my_a
        self.my_b = my_b
        self.my_c = my_c
        self.my_d = my_d

    def is_sporer(self, x, y) -> bool:
        return self.cases[x][y].type_case == 'SPORER'

    def is_root(self, x, y) -> bool:
        return self.cases[x][y].type_case == 'ROOT'

    def get_my_roots(self) : #-> []Case:
        return [case for row in self.cases for case in row if case.type_case == 'ROOT' and case.owner == 1]

    def get_my_organs(self) : #-> []Case:
        return [case for row in self.cases for case in row if case.id > -1 and case.owner == 1]

    def get_proteins(self, type_protein) : #-> []Case:
        return [case for row in self.cases for case in row if case.type_case == type_protein]       

    def get_my_last_organ(self, root):
        my_last_organ = None
        my_last_organ_id = -1
        for organ in self.get_my_organs():
            if organ.id > my_last_organ_id and organ.root_id == root.root_id:
                my_last_organ = organ
                my_last_organ_id = organ.id
        return my_last_organ

    def get_free_proteins_nearest_root(self, root, liste_proteins):
        free_proteins_nearest_root = []
        
        for protein in liste_proteins:
            root_nearest_protein = self.get_case_nearest(protein, self.get_my_roots())
            x = protein.x
            y = protein.y
            if (
                (x>0 and y>0 and self.cases[x-1][y-1].type_case != 'HARVESTER')
                and (x>0 and y>0 and self.cases[x][y-1].type_case != 'HARVESTER')
                and (x<self.width and y>0 and self.cases[x+1][y-1].type_case != 'HARVESTER')
                and (x>0 and y>0 and self.cases[x-1][y].type_case != 'HARVESTER')
                and (x<self.width and y>0 and self.cases[x+1][y].type_case != 'HARVESTER')
                and (x>0 and y<self.height and self.cases[x-1][y+1].type_case != 'HARVESTER')
                and (x>0 and y<self.height and self.cases[x][y+1].type_case != 'HARVESTER')
                and (x<self.width and y<self.height and self.cases[x+1][y+1].type_case != 'HARVESTER')
                and root_nearest_protein.id == root.id
            ):
                free_proteins_nearest_root.append(protein)
        return free_proteins_nearest_root

    def get_sporer_near(self, x, y):
        sporer = None
        if x > 0 and y > 0 and self.get_case(x-1,y-1).type_case == 'SPORER':
            sporer = self.get_case(x-1,y-1)
        elif x > 0 and y > 0 and self.get_case(x,y-1).type_case == 'SPORER':
            sporer = self.get_case(x,y-1)
        elif x < self.width and y > 0 and self.get_case(x+1,y-1).type_case == 'SPORER':
            sporer = self.get_case(x+1,y-1)
        elif x > 0 and y > 0 and self.get_case(x-1,y).type_case == 'SPORER':
            sporer = self.get_case(x-1,y)
        elif x < self.width and y < self.height and self.get_case(x+1,y).type_case == 'SPORER':
            sporer = self.get_case(x+1,y)
        elif x < self.width and y < self.height and self.get_case(x+1,y+1).type_case == 'SPORER':
            sporer = self.get_case(x+1,y+1)
        elif x < self.width and y < self.height and self.get_case(x,y+1).type_case == 'SPORER':
            sporer = self.get_case(x,y+1)
        elif x > 0 and y < self.height and self.get_case(x-1,y+1).type_case == 'SPORER':
            sporer = self.get_case(x-1,y+1)
        return sporer

    # si la proteine la plus proche est plus éloignée que le nombre de ressources de basic pour y aller
    #    alors on veut spore à 2 cases de la proteine pour grow un harvester à côté de la protéine
    # et/ou si le root adverse le plus proche est "à portée", 
    #    alors on veut spore le plus proche possible pour grow une tentacle à côté du root adverse 
    def can_grow_sporer(self) -> bool:
        # ressources ok pour sporer et root
        # et placement possible c-a-d le sporer peut envoyer un root à l'autre côté de la grid
        return ((self.my_a > 0 and self.my_b > 1 and self.my_c > 0 and self.my_d > 1) 
            #and self.get_empty_case_nearest(self.roots[0].x,self.roots[0].y)
            )
    
    def can_grow_harvester(self) -> bool:
        # ressources ok 
        # et une proteine est à portée 
        # et pas de havester deja grow sur cette proteine
        return (self.my_c > 0 and self.my_d > 0) 

    def can_grow_tentacle(self) -> bool:
        # ressources ok 
        # et un root adverse est à proximité
        # et pas de tentacle deja grow pour ce root
        return (self.my_b > 0 and self.my_c > 0) 

    def can_grow_root(self):
        # ressources ok 
        # et sporer existant
        return (self.my_a > 0 and self.my_b > 0 and self.my_c > 0 and self.my_d > 0) 

    def can_grow_basic(self):
        # ressources ok 
        return (self.my_a > 0)

    def get_empty_case_nearest(self, x, y):
        empty_case_nearest = None
        #if x > 0 and y > 0 and self.get_case(x-1,y-1).type_case == 'EMPTY':
        #    empty_case_nearest = self.get_case(x-1,y-1)
        if x > 0 and y > 0 and self.get_case(x,y-1).type_case == 'EMPTY':
            empty_case_nearest = self.get_case(x,y-1)
        #elif x < self.width and y > 0 and self.get_case(x+1,y-1).type_case == 'EMPTY':
        #    empty_case_nearest = self.get_case(x+1,y-1)
        elif x > 0 and y > 0 and self.get_case(x-1,y).type_case == 'EMPTY':
            empty_case_nearest = self.get_case(x-1,y)
        elif x < self.width and y < self.height and self.get_case(x+1,y).type_case == 'EMPTY':
            empty_case_nearest = self.get_case(x+1,y)
        #elif x < self.width and y < self.height and self.get_case(x+1,y+1).type_case == 'EMPTY':
        #    empty_case_nearest = self.get_case(x+1,y+1)
        elif x < self.width and y < self.height and self.get_case(x,y+1).type_case == 'EMPTY':
            empty_case_nearest = self.get_case(x,y+1)
        #elif x > 0 and y < self.height and self.get_case(x-1,y+1).type_case == 'EMPTY':
        #    empty_case_nearest = self.get_case(x-1,y+1)
        return empty_case_nearest

    def get_empty_cases_nearest_target(self, x, y, target_x, target_y):
        empty_case_nearest = None
        liste_cases = []
        #if x > 0 and y > 0 and self.get_case(x-1,y-1).type_case == 'EMPTY':
        #    liste_cases.append( self.get_case(x-1,y-1))
        if x > 0 and y > 0 and self.get_case(x,y-1).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x,y-1))
        #if x < self.width and y > 0 and self.get_case(x+1,y-1).type_case == 'EMPTY':
        #    liste_cases.append( self.get_case(x+1,y-1))
        if x > 0 and y > 0 and self.get_case(x-1,y).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x-1,y))
        if x < self.width and y < self.height and self.get_case(x+1,y).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x+1,y))
        #if x < self.width and y < self.height and self.get_case(x+1,y+1).type_case == 'EMPTY':
        #    liste_cases.append( self.get_case(x+1,y+1))
        if x < self.width and y < self.height and self.get_case(x,y+1).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x,y+1))
        #if x > 0 and y < self.height and self.get_case(x-1,y+1).type_case == 'EMPTY':
        #    liste_cases.append( self.get_case(x-1,y+1))

        empty_case_nearest = self.get_case_nearest(self.cases[target_x][target_y], liste_cases)

        return empty_case_nearest

    def get_free_protein_nearest(self, root, type_protein):
        protein_nearest = None
        liste_proteins = self.get_proteins(type_protein)
        for protein in liste_proteins:
            print(f"DEBUG liste_proteins:{protein}", file=sys.stderr, flush=True)
        liste_free_proteins = self.get_free_proteins_nearest_root(root, liste_proteins)
        for protein in liste_free_proteins:
            print(f"DEBUG liste_free_proteins:{protein}", file=sys.stderr, flush=True)        
        protein_nearest = self.get_case_nearest(root, liste_free_proteins)
        return protein_nearest

    def get_my_organ_nearest(self, case) :#-> (Case | None):
        my_organ_nearest = None
        my_organs = self.get_my_organs()
        my_organ_nearest = self.get_case_nearest(case, my_organs)
        return my_organ_nearest

    def get_case_nearest(self, organ, liste_cases):
        distance_case_nearest = 1000
        case_nearest = None
        for case in liste_cases:
            dist = distance(organ.x, organ.y, case.x, case.y)
            if distance_case_nearest > dist:
                case_nearest = case
                distance_case_nearest = dist

        return case_nearest


def distance(point1_x, point1_y, point2_x, point2_y):
    return math.sqrt((point2_x - point1_x)**2 + (point2_y - point1_y)**2)

#def distance(point1, point2):
#    return math.sqrt((point2['x'] - point1['x'])**2 + (point2['y'] - point1['y'])**2)

#def get_protein_nearest(organ, proteins):
#    distance_protein_nearest = 1000
#    protein_nearest = None
#    for protein in proteins:
#        dist = distance(organ, protein)
#        if distance_protein_nearest > dist:
#            protein_nearest = protein
#            distance_protein_nearest = dist
#
#    return protein_nearest

# width: columns in the game grid
# height: rows in the game grid
width, height = [int(i) for i in input().split()]
game_grid = GameGrid(width, height)

# game loop
while True:
    my_organisms = []
    proteins = []
    entity_count = int(input())
    for i in range(entity_count):
        inputs = input().split()
        x = int(inputs[0])
        y = int(inputs[1])  # grid coordinate
        _type = inputs[2]  # WALL, ROOT, BASIC, TENTACLE, HARVESTER, SPORER, A, B, C, D
        owner = int(inputs[3])  # 1 if your organ, 0 if enemy organ, -1 if neither
        organ_id = int(inputs[4])  # id of this entity if it's an organ, 0 otherwise
        organ_dir = inputs[5]  # N,E,S,W or X if not an organ
        organ_parent_id = int(inputs[6])
        organ_root_id = int(inputs[7])
        
        game_grid.cases[x][y].type_case = _type
        game_grid.cases[x][y].owner = owner
        game_grid.cases[x][y].id = organ_id
        game_grid.cases[x][y].parent_id = organ_parent_id
        game_grid.cases[x][y].root_id = organ_root_id
        if owner == 1 and _type == 'ROOT':
            game_grid.set_origin_root(x, y)
        if owner == 1:
            my_organ = {'id': organ_id, 'x': x, 'y': y, 'type': _type}
            my_organisms.append(my_organ)
        if _type == 'A' or _type == 'B' or _type == 'C' or _type == 'D':
            proteins.append({'id': organ_id, 'x': x, 'y': y, 'type': _type})
    # my_d: your protein stock
    my_a, my_b, my_c, my_d = [int(i) for i in input().split()]
    game_grid.set_my_protein(my_a, my_b, my_c, my_d)
    # opp_d: opponent's protein stock
    opp_a, opp_b, opp_c, opp_d = [int(i) for i in input().split()]
    required_actions_count = int(input())  # your number of organisms, output an action for each one in any order
    
    for root in game_grid.get_my_roots():
        print(f"DEBUG root:{root}", file=sys.stderr, flush=True)
    #for i in range(required_actions_count):


        #### DECISION

        # 1/ je veux viser les proteines "libres"
        #       il existe des cases libres autour de la proteine la plus proche
        #       il existe un root qui est à portée de la protéine la plus proche 
        #       (a/ soit en construisant des basic + un harvester de l'organ le plus proche jusqu'à la proteine 
        #        b/ soit avec un spore )
        # 2/ je veux viser les roots adverses à détruire
        # 3/ je veux occuper un maximum de cases libres


        #####
        


        # je veux connaitre la proteine A la plus proche qui soit libre, 
        # pour laquelle il n'y a pas de root plus proche
        # si au moins une protein A existe 
        # et si la plus proche protein A libre est la plus proche du root
        #   alors on essaie d'atteindre la proteine A soit par BASIC soit par SPORE
        # sinon si les ressources sont suffisantes (possible de grow BASIC et les autres root peuvent atteindre leurs objectifs)
        #   alors grow BASIC des cases autour du root
        # sinon 
        #   alors WAIT 

        free_protein_a_nearest_root = game_grid.get_free_protein_nearest(root, 'A')
        print(f"DEBUG free_protein_a_nearest_root:{free_protein_a_nearest_root}", file=sys.stderr, flush=True)
        if free_protein_a_nearest_root:

            last_organ = game_grid.get_my_organ_nearest(free_protein_a_nearest_root)
            empty_case_nearest = game_grid.get_empty_case_nearest(last_organ.x, last_organ.y)

            # stratégie 1/ a/
            if distance(last_organ.x, last_organ.y, free_protein_a_nearest_root.x, free_protein_a_nearest_root.y) < game_grid.my_a: 
                empty_case_nearest = game_grid.get_empty_cases_nearest_target(last_organ.x, last_organ.y, free_protein_a_nearest_root.x, free_protein_a_nearest_root.y)
                print(f"DEBUG 1a empty_case_nearest:{empty_case_nearest}", file=sys.stderr, flush=True)
                if (free_protein_a_nearest_root 
                    and empty_case_nearest
                    and free_protein_a_nearest_root.x == empty_case_nearest.x
                    and free_protein_a_nearest_root.y == empty_case_nearest.y + 1
                ) :
                    print(f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER S") 
                elif (free_protein_a_nearest_root 
                    and empty_case_nearest
                    and free_protein_a_nearest_root.x == empty_case_nearest.x 
                    and free_protein_a_nearest_root.y == empty_case_nearest.y - 1
                ) :
                    print(f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER N")
                elif (free_protein_a_nearest_root 
                    and empty_case_nearest
                    and free_protein_a_nearest_root.x == empty_case_nearest.x + 1
                    and free_protein_a_nearest_root.y == empty_case_nearest.y
                ) :
                    print(f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER E")
                elif (free_protein_a_nearest_root 
                    and empty_case_nearest
                    and free_protein_a_nearest_root.x == empty_case_nearest.x - 1 
                    and free_protein_a_nearest_root.y == empty_case_nearest.y
                ) :
                    print(f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER O")                    
                else :
                    empty_case_nearest = game_grid.get_empty_cases_nearest_target(last_organ.x, last_organ.y, free_protein_a_nearest_root.x, free_protein_a_nearest_root.y)
                    if empty_case_nearest:
                        print(f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC")
                    else:
                        print("WAIT 1a")
                #elif my_organ['x'] < width and my_organ['y'] < height and game_grid.get_case(my_organ['x'] + 1, my_organ['y']).type_case != 'WALL':
                #    print(f"GROW {last_organ.id} {my_organ['x'] + 1 } {my_organ['y']} BASIC")
                #elif my_organ['y'] < height:
                #    print(f"GROW {last_organ.id} {my_organ['x'] -1 } {my_organ['y'] - 1} BASIC")
                #else:
                #    print(f"GROW {last_organ.id} {my_organ['x']} {my_organ['y'] - 1} BASIC")
            # stratégie 1/ b/
            #elif protein_nearest: 
            else:
                sporer = game_grid.get_sporer_near(root.x, root.y)
                if not sporer:
                    dir = 'E'
                    if free_protein_a_nearest_root.x < root.x:
                        dir = 'O'

                    if root.y == free_protein_a_nearest_root.y:
                        print(f"GROW {root.id} {root.x + 1} {root.y} SPORER {dir}")
                    elif root.y < free_protein_a_nearest_root.y:
                        print(f"GROW {root.id} {root.x} {root.y + 1} SPORER {dir}")
                    elif root.y > free_protein_a_nearest_root.y:
                        print(f"GROW {root.id} {root.x} {root.y - 1} SPORER {dir}")
                #and (
                #    protein_nearest['y'] >= my_organ['y']
                #) :
                    #print(f"GROW {my_organ['id']} {my_organ['x'] } {my_organ['y'] + 1} SPORER E") 
                     
                #elif protein_nearest and protein_nearest['x'] > my_organ['x'] + 1 and (
                #    protein_nearest['y'] == my_organ['y'] - 1
                #) :
                    #print(f"GROW {my_organ['id']} {my_organ['x'] } {my_organ['y'] - 1} SPORER E")
                #elif protein_nearest and protein_nearest['x'] > my_organ['x'] + 1 and (
                #    protein_nearest['y'] == my_organ['y'] 
                #) :
                    #print(f"GROW {my_organ['id']} {my_organ['x'] - 1} {my_organ['y']} SPORER N")
                elif (free_protein_a_nearest_root.x > last_organ.x + 1 
                    and sporer 
                    and not game_grid.is_root(free_protein_a_nearest_root.x - 2, sporer.y)):
                    print(f"SPORE {sporer.id} {free_protein_a_nearest_root.x - 2} {sporer.y} ")
                elif (free_protein_a_nearest_root.x < last_organ.x + 1 
                    and sporer 
                    and not game_grid.is_root(free_protein_a_nearest_root.x + 2, sporer.y)):
                    print(f"SPORE {sporer.id} {free_protein_a_nearest_root.x + 2} {sporer.y} ")

                else : 
                    print("WAIT 1b")
        # stratégie 3
        elif game_grid.my_a > 4:
            last_organ = game_grid.get_my_last_organ(root)
            empty_case_nearest = game_grid.get_empty_case_nearest(last_organ.x, last_organ.y)
            if empty_case_nearest:
                print(f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC")
            else : 
                print("WAIT 3")
        else : 
            print("WAIT 4")



        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr, flush=True)
        #my_organisms[0]['x']
        #my_organ = [organ for organ in my_organisms if organ['type'] == 'ROOT'][0]
        #my_organ = my_organisms[-1]
        #protein_nearest = get_protein_nearest(my_organ, proteins)
        #print(f"last my_organ : {my_organ['x']}, {my_organ['y']} {my_organ['type']}", file=sys.stderr, flush=True)

        
        #print("WAIT")
