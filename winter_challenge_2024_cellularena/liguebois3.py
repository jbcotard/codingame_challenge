import sys
import math

# Grow and multiply your organisms to end up larger than your opponent.

class Case:
    def __init__(self, x, y, type_case):
        self.x = x
        self.y = y
        self.type_case = type_case  # WALL, ROOT, BASIC, TENTACLE, HARVESTER, SPORER, A, B, C, D, EMPTY

    def __str__(self):
        return f"Case({self.x}, {self.y}): {self.type_case}"

# Création d'un plateau de jeu
class GameGrid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.cases = [[Case(x, y, 'EMPTY') for y in range(height)] for x in range(width)]

    def get_case(self, x, y):
        return self.cases[x][y]



def distance(point1, point2):
    return math.sqrt((point2['x'] - point1['x'])**2 + (point2['y'] - point1['y'])**2)

def get_protein_nearest(organ, proteins):
    distance_protein_nearest = 1000
    protein_nearest = None
    for protein in proteins:
        dist = distance(organ, protein)
        if distance_protein_nearest > dist:
            protein_nearest = protein
            distance_protein_nearest = dist

    return protein_nearest


# width: columns in the game grid
# height: rows in the game grid
width, height = [int(i) for i in input().split()]
game_grid = GameGrid(width, height)

# game loop
while True:
    my_organisms = []
    proteins = []
    entity_count = int(input())
    for i in range(entity_count):
        inputs = input().split()
        x = int(inputs[0])
        y = int(inputs[1])  # grid coordinate
        _type = inputs[2]  # WALL, ROOT, BASIC, TENTACLE, HARVESTER, SPORER, A, B, C, D
        owner = int(inputs[3])  # 1 if your organ, 0 if enemy organ, -1 if neither
        organ_id = int(inputs[4])  # id of this entity if it's an organ, 0 otherwise
        organ_dir = inputs[5]  # N,E,S,W or X if not an organ
        organ_parent_id = int(inputs[6])
        organ_root_id = int(inputs[7])
        game_grid.cases[x][y].type_case = _type
        if owner == 1:
            my_organ = {'id': organ_id, 'x': x, 'y': y, 'type': _type}
            my_organisms.append(my_organ)
        if _type == 'A' or _type == 'B' or _type == 'C' or _type == 'D':
            proteins.append({'id': organ_id, 'x': x, 'y': y, 'type': _type})
    # my_d: your protein stock
    my_a, my_b, my_c, my_d = [int(i) for i in input().split()]
    # opp_d: opponent's protein stock
    opp_a, opp_b, opp_c, opp_d = [int(i) for i in input().split()]
    required_actions_count = int(input())  # your number of organisms, output an action for each one in any order
    for i in range(required_actions_count):

        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr, flush=True)
        #my_organisms[0]['x']
        #my_organ = [organ for organ in my_organisms if organ['type'] == 'ROOT'][0]
        my_organ = my_organisms[-1]
        protein_nearest = get_protein_nearest(my_organ, proteins)
        if protein_nearest['x'] == my_organ['x'] + 1 and (
            protein_nearest['y'] == my_organ['y'] + 1
        ) :
            print(f"GROW {my_organ['id']} {my_organ['x'] + 1 } {my_organ['y']} HARVESTER S") 
        elif protein_nearest['x'] == my_organ['x'] + 1 and (
            protein_nearest['y'] == my_organ['y'] - 1
        ) :
            print(f"GROW {my_organ['id']} {my_organ['x'] + 1 } {my_organ['y']} HARVESTER N")
        elif protein_nearest['x'] == my_organ['x'] + 1 and (
            protein_nearest['y'] == my_organ['y'] 
        ) :
            print(f"GROW {my_organ['id']} {my_organ['x'] + 1 } {my_organ['y']} HARVESTER E")
        elif my_organ['x'] < width and my_organ['y'] < height and game_grid.get_case(my_organ['x'] + 1, my_organ['y']).type_case != 'WALL':
            print(f"GROW {my_organ['id']} {my_organ['x'] + 1 } {my_organ['y']} BASIC")
        elif my_organ['y'] < height:
            print(f"GROW {my_organ['id']} {my_organ['x'] -1 } {my_organ['y'] - 1} BASIC")
        else:
            print(f"GROW {my_organ['id']} {my_organ['x']} {my_organ['y'] - 1} BASIC")
        #print("WAIT")
