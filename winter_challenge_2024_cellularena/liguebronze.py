import sys
import math

# Grow and multiply your organisms to end up larger than your opponent.

class Case:
    def __init__(self, x, y, type_case, owner):
        self.x = x
        self.y = y
        self.type_case = type_case  # WALL, ROOT, BASIC, TENTACLE, HARVESTER, SPORER, A, B, C, D, EMPTY
        self.owner = owner # 1 if your organ, 0 if enemy organ, -1 if neither
        self.id = 0
        self.root_id = -1
        self.parent_id = -1
        self.dir = 'X' # N,E,S,W or X if not an organ

    def __str__(self):
        return f"{self.type_case}-{self.id}-({self.x}, {self.y})"

# Création d'un plateau de jeu
class GameGrid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.cases = [[Case(x, y, 'EMPTY', -1) for y in range(height)] for x in range(width)]
        self.my_a = 0
        self.my_b = 0
        self.my_c = 0
        self.my_d = 0
        self.roots = []

    def get_case(self, x, y):
        return self.cases[x][y]

    def set_origin_root(self, x, y):
        self.roots.append(self.cases[x][y])

    def set_my_protein(self, my_a, my_b, my_c, my_d):
        self.my_a = my_a
        self.my_b = my_b
        self.my_c = my_c
        self.my_d = my_d

    def is_sporer(self, x, y) -> bool:
        return self.cases[x][y].type_case == 'SPORER'

    def is_root(self, x, y) -> bool:
        return self.cases[x][y].type_case == 'ROOT'

    def get_walls(self) :#-> list(Case):
        return [case for row in self.cases for case in row if case.type_case == 'WALL']

    def get_all_proteins(self):#-> list(Case):
        return [case for row in self.cases for case in row if case.type_case == 'A' or case.type_case == 'B' or case.type_case == 'C' or case.type_case == 'D']

    def get_my_roots(self) : #-> []Case:
        return [case for row in self.cases for case in row if case.type_case == 'ROOT' and case.owner == 1]

    def get_my_organs(self) : #-> []Case:
        return [case for row in self.cases for case in row if case.id > -1 and case.owner == 1]

    def get_my_organs_from_root(self, root) : #-> []Case:
        return [case for row in self.cases for case in row if case.id > -1 and case.owner == 1 and case.root_id == root.id]

    def get_proteins(self, type_protein) : #-> []Case:
        return [case for row in self.cases for case in row if case.type_case == type_protein]       

    def get_list_protein_cases_nearest(self, organ: Case):
        liste_cases = []
        x = organ.x
        y = organ.y
        if y > 0 and self.get_case(x,y-1).type_case == 'A':
            liste_cases.append( self.get_case(x,y-1))
        if x > 0 and self.get_case(x-1,y).type_case == 'A':
            liste_cases.append( self.get_case(x-1,y))
        if x < self.width-1 and self.get_case(x+1,y).type_case == 'A':
            liste_cases.append( self.get_case(x+1,y))
        if y < self.height-1 and self.get_case(x,y+1).type_case == 'A':
            liste_cases.append( self.get_case(x,y+1))
        return liste_cases

    def get_list_empty_cases_nearest(self, organ: Case):
        liste_cases = []
        x = organ.x
        y = organ.y
        if y > 0 and self.get_case(x,y-1).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x,y-1))
        if x > 0 and self.get_case(x-1,y).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x-1,y))
        if x < self.width-1 and self.get_case(x+1,y).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x+1,y))
        if y < self.height-1 and self.get_case(x,y+1).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x,y+1))
        return liste_cases

    def get_my_organ_with_protein_near(self, root):
        my_organ = None
        my_organ_nb_protein_cases = 0
        for organ in self.get_my_organs():
            organ_nb_protein_cases = len(self.get_list_protein_cases_nearest(organ))
            if  organ_nb_protein_cases > my_organ_nb_protein_cases and organ.root_id == root.root_id:
                my_organ = organ
                my_organ_nb_protein_cases = organ_nb_protein_cases
                print(f"DEBUG get_my_organ_with_protein_near:{organ} - {organ_nb_protein_cases}", file=sys.stderr, flush=True)
            else:
                print(f"DEBUG get_my_organ_with_protein_near:{organ} - {organ_nb_protein_cases} - not", file=sys.stderr, flush=True)
        return my_organ

    def get_my_organ_with_max_empty_cases(self, root):
        my_organ = None
        my_organ_nb_empty_cases = 0
        for organ in self.get_my_organs():
            organ_nb_empty_cases = len(self.get_list_empty_cases_nearest(organ))
            if  organ_nb_empty_cases > my_organ_nb_empty_cases and organ.root_id == root.root_id:
                my_organ = organ
                my_organ_nb_empty_cases = organ_nb_empty_cases
            #    print(f"DEBUG get_my_organ_with_max_empty_cases:{organ} - {organ_nb_empty_cases}", file=sys.stderr, flush=True)
            #else:
            #    print(f"DEBUG get_my_organ_with_max_empty_cases:{organ} - {organ_nb_empty_cases} - not", file=sys.stderr, flush=True)
        return my_organ

    def get_my_last_organ(self, root):
        my_last_organ = None
        my_last_organ_id = -1
        for organ in self.get_my_organs():
            if organ.id > my_last_organ_id and organ.root_id == root.root_id:
                my_last_organ = organ
                my_last_organ_id = organ.id
        return my_last_organ

    def get_free_proteins_nearest_root(self, root, liste_proteins):
        free_proteins_nearest_root = []
        
        for protein in liste_proteins:
            root_nearest_protein = self.get_case_nearest(protein, self.get_my_roots())
            x = protein.x
            y = protein.y
            #print(f"DEBUG get_free_proteins_nearest_root :{protein} - nearest root {root_nearest_protein}", file=sys.stderr, flush=True)
            if (
                ( self.cases[x-1][y-1].type_case != 'HARVESTER' if x>0 and y>0 else True)
                and (self.cases[x][y-1].type_case != 'HARVESTER' if y>0 else True)
                and (self.cases[x+1][y-1].type_case != 'HARVESTER' if x<self.width-1 and y>0 else True)
                and (self.cases[x-1][y].type_case != 'HARVESTER' if x>0 else True)
                and (self.cases[x+1][y].type_case != 'HARVESTER' if x<self.width-1 else True)
                and (self.cases[x-1][y+1].type_case != 'HARVESTER' if x>0 and y<self.height-1 else True)
                and (self.cases[x][y+1].type_case != 'HARVESTER' if y<self.height-1 else True)
                and (self.cases[x+1][y+1].type_case != 'HARVESTER' if x<self.width-1 and y<self.height-1 else True)
                and root_nearest_protein.id == root.id
            ):
                free_proteins_nearest_root.append(protein)
        return free_proteins_nearest_root

    def get_sporer_near(self, x, y):
        sporer = None
        if x > 0 and y > 0 and self.get_case(x-1,y-1).type_case == 'SPORER':
            sporer = self.get_case(x-1,y-1)
        elif x > 0 and y > 0 and self.get_case(x,y-1).type_case == 'SPORER':
            sporer = self.get_case(x,y-1)
        elif x < self.width and y > 0 and self.get_case(x+1,y-1).type_case == 'SPORER':
            sporer = self.get_case(x+1,y-1)
        elif x > 0 and y > 0 and self.get_case(x-1,y).type_case == 'SPORER':
            sporer = self.get_case(x-1,y)
        elif x < self.width and y < self.height and self.get_case(x+1,y).type_case == 'SPORER':
            sporer = self.get_case(x+1,y)
        elif x < self.width and y < self.height and self.get_case(x+1,y+1).type_case == 'SPORER':
            sporer = self.get_case(x+1,y+1)
        elif x < self.width and y < self.height and self.get_case(x,y+1).type_case == 'SPORER':
            sporer = self.get_case(x,y+1)
        elif x > 0 and y < self.height and self.get_case(x-1,y+1).type_case == 'SPORER':
            sporer = self.get_case(x-1,y+1)
        return sporer

    def get_sporer_to_target_near_root(self, organ, protein_targeted):
        sporer = None
        x = organ.x
        y = organ.y
        dir = 'E'
        if protein_targeted.x < x:
            dir = 'W'
        if y > 0 and self.get_case(x,y-1).type_case == 'SPORER' and self.get_case(x,y-1).dir == dir:
            sporer = self.get_case(x,y-1)
        elif x > 0 and self.get_case(x-1,y).type_case == 'SPORER' and self.get_case(x-1,y).dir == dir:
            sporer = self.get_case(x-1,y)
        elif x < self.width-1 and self.get_case(x+1,y).type_case == 'SPORER' and self.get_case(x+1,y).dir == dir:
            sporer = self.get_case(x+1,y)
        elif y < self.height-1 and self.get_case(x,y+1).type_case == 'SPORER' and self.get_case(x,y+1).dir == dir:
            sporer = self.get_case(x,y+1)

        return sporer


    # si la proteine la plus proche est plus éloignée que le nombre de ressources de basic pour y aller
    #    alors on veut spore à 2 cases de la proteine pour grow un harvester à côté de la protéine
    # et/ou si le root adverse le plus proche est "à portée", 
    #    alors on veut spore le plus proche possible pour grow une tentacle à côté du root adverse 
    def can_grow_sporer(self) -> bool:
        # ressources ok pour sporer et root
        # et placement possible c-a-d le sporer peut envoyer un root à l'autre côté de la grid
        return ((self.my_a > 0 and self.my_b > 1 and self.my_c > 0 and self.my_d > 1) 
            #and self.get_empty_case_nearest(self.roots[0].x,self.roots[0].y)
            )
    
    def can_grow_sporer_root_harvester(self) -> bool:
        # ressources ok pour sporer, root et harvester
        return ((self.my_a > 2 and self.my_b > 2 and self.my_c > 2 and self.my_d > 3))

    def can_grow_harvester(self) -> bool:
        # ressources ok 
        # et une proteine est à portée 
        # et pas de havester deja grow sur cette proteine
        return (self.my_c > 0 and self.my_d > 0) 

    def can_grow_tentacle(self) -> bool:
        # ressources ok 
        # et un root adverse est à proximité
        # et pas de tentacle deja grow pour ce root
        return (self.my_b > 0 and self.my_c > 0) 

    def can_grow_root(self):
        # ressources ok 
        # et sporer existant
        return (self.my_a > 0 and self.my_b > 0 and self.my_c > 0 and self.my_d > 0) 

    def can_grow_basic(self):
        # ressources ok 
        return (self.my_a > 0)

    def is_moveable_case(self, case) -> bool:
        return case.type_case != 'WALL' and case.owner != 1

    def get_move_case(self, x, y):
        move_case = None
        if y > 0 and self.is_moveable_case(self.get_case(x,y-1)):
            move_case = self.get_case(x,y-1)
        if x > 0 and self.is_moveable_case(self.get_case(x-1,y)):
            move_case = self.get_case(x-1,y)
        if x < self.width-1 and self.is_moveable_case(self.get_case(x+1,y)):
            move_case = self.get_case(x+1,y)
        if y < self.height-1 and self.is_moveable_case(self.get_case(x,y+1)):
            move_case = self.get_case(x,y+1)
        return move_case

    def get_empty_case_nearest(self, x, y):
        empty_case_nearest = None

        if x < self.width-1 and self.get_case(x+1,y).type_case == 'EMPTY':
            empty_case_nearest = self.get_case(x+1,y)
        elif y < self.height-1 and self.get_case(x,y+1).type_case == 'EMPTY':
            empty_case_nearest = self.get_case(x,y+1)
        elif y > 0 and self.get_case(x,y-1).type_case == 'EMPTY':
            empty_case_nearest = self.get_case(x,y-1)
        elif x > 0 and self.get_case(x-1,y).type_case == 'EMPTY':
            empty_case_nearest = self.get_case(x-1,y)
        return empty_case_nearest

    def get_empty_cases_nearest_target(self, x, y, target_x, target_y):
        empty_case_nearest = None
        liste_cases = []
        if y > 0 and self.get_case(x,y-1).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x,y-1))
        if x > 0 and self.get_case(x-1,y).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x-1,y))
        if x < self.width-1 and self.get_case(x+1,y).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x+1,y))
        if y < self.height-1 and self.get_case(x,y+1).type_case == 'EMPTY':
            liste_cases.append( self.get_case(x,y+1))

        empty_case_nearest = self.get_case_nearest(self.cases[target_x][target_y], liste_cases)

        return empty_case_nearest

    def get_free_protein_nearest(self, root, type_protein):
        protein_nearest = None
        liste_proteins = self.get_proteins(type_protein)
        #for protein in liste_proteins:
        #    print(f"DEBUG liste_proteins:{protein}", file=sys.stderr, flush=True)
        liste_free_proteins = self.get_free_proteins_nearest_root(root, liste_proteins)
        #for protein in liste_free_proteins:
        #    print(f"DEBUG liste_free_proteins:{protein}", file=sys.stderr, flush=True)        
        protein_nearest = self.get_case_nearest(root, liste_free_proteins)
        return protein_nearest

    def get_my_organ_nearest(self, case) :#-> (Case | None):
        my_organ_nearest = None
        my_organs = self.get_my_organs()
        my_organ_nearest = self.get_case_nearest(case, my_organs)
        return my_organ_nearest

    def get_case_nearest(self, organ, liste_cases):
        distance_case_nearest = 1000
        case_nearest = None
        for case in liste_cases:
            dist = distance(organ.x, organ.y, case.x, case.y)
            if distance_case_nearest > dist:
                case_nearest = case
                distance_case_nearest = dist

        return case_nearest

    def is_organ_near_free_protein(self, organ, type_protein):
        is_organ_near_protein = False
        #liste_proteins = self.get_proteins(type_protein)
        #for protein in liste_proteins:
        x = organ.x
        y = organ.y
        #print(f"DEBUG {self.cases[x][y+2]}  {(self.cases[x][y+2].type_case == type_protein and self.cases[x][y+1].type_case == 'EMPTY') if y<self.height-2 else False}", file=sys.stderr, flush=True)
        if (self.cases[x][y-2].type_case == type_protein if y>1 else False
            and self.cases[x][y-1].type_case == 'EMPTY'):
            is_organ_near_protein = True
        if (self.cases[x-2][y].type_case == type_protein if x>1 else False
            and self.cases[x-1][y].type_case == 'EMPTY'):
            is_organ_near_protein = True
        if (self.cases[x][y+2].type_case == type_protein and self.cases[x][y+1].type_case == 'EMPTY') if y<self.height-2 else False:
            is_organ_near_protein = True
        if (self.cases[x+2][y].type_case == type_protein if x<width-2 else False
            and self.cases[x+1][y].type_case == 'EMPTY'):
            is_organ_near_protein = True

        return is_organ_near_protein

    def is_organ_near_free_root_ennemy(self, organ):
        is_organ_near_root_ennemy = False
        #liste_proteins = self.get_proteins(type_protein)
        #for protein in liste_proteins:
        x = organ.x
        y = organ.y
        if ((self.cases[x][y-2].type_case == 'ROOT' and self.cases[x][y-2].owner == 0) if y>1 else False
            and self.cases[x][y-1].type_case == 'EMPTY'):
            is_organ_near_root_ennemy = True
        if ((self.cases[x-2][y].type_case == 'ROOT' and self.cases[x-2][y].owner == 0) if x>1 else False
            and self.cases[x-1][y].type_case == 'EMPTY'):
            is_organ_near_root_ennemy = True
        if ((self.cases[x][y+2].type_case == 'ROOT' and self.cases[x][y+2].owner == 0) if y<self.height-2 else False
            and self.cases[x][y+1].type_case == 'EMPTY'):
            is_organ_near_root_ennemy = True
        if ((self.cases[x+2][y].type_case == 'ROOT' and self.cases[x+2][y].owner == 0) if x<self.width-2 else False
            and self.cases[x+1][y].type_case == 'EMPTY'):
            is_organ_near_root_ennemy = True
        return is_organ_near_root_ennemy

    def is_tentaculable(self, root):
        is_case_tentaculable = False
        organs_from_root = self.get_my_organs_from_root(root)
        for organ in organs_from_root:
            if self.is_organ_near_free_root_ennemy(organ) and self.can_grow_tentacle():
                is_case_tentaculable = True
        return is_case_tentaculable

    def get_tentaculable(self, root):
        case_tentaculable = None
        organ_from = None
        dir_tentaculable = 'N'
        organs_from_root = self.get_my_organs_from_root(root)
        for organ in organs_from_root:
            x = organ.x
            y = organ.y
            if ((self.cases[x][y-2].type_case == 'ROOT' and self.cases[x][y-2].owner == 0) if y>1 else False
            and self.cases[x][y-1].type_case == 'EMPTY'):
                case_tentaculable = self.cases[x][y-1]
                organ_from = organ
                dir_tentaculable = 'N'
            if ((self.cases[x-2][y].type_case == 'ROOT' and self.cases[x-2][y].owner == 0) if x>1 else False
            and self.cases[x-1][y].type_case == 'EMPTY'):
                case_tentaculable = self.cases[x-1][y]
                organ_from = organ
                dir_tentaculable = 'W'
            if ((self.cases[x][y+2].type_case == 'ROOT' and self.cases[x][y+2].owner == 0) if y<self.height-2 else False
            and self.cases[x][y+1].type_case == 'EMPTY'):
                case_tentaculable = self.cases[x][y+1]
                organ_from = organ
                dir_tentaculable = 'S'
            if ((self.cases[x+2][y].type_case == 'ROOT' and self.cases[x+2][y].owner == 0) if x<self.width-2 else False
            and self.cases[x+1][y].type_case == 'EMPTY'):
                case_tentaculable = self.cases[x+1][y]
                organ_from = organ
                dir_tentaculable = 'E'
        return case_tentaculable, dir_tentaculable, organ_from


    def is_harvesterable_protein(self, root, type_protein):
        is_case_havesterable = False
        organs_from_root = self.get_my_organs_from_root(root)
        for organ in organs_from_root:
            if self.is_organ_near_free_protein(organ, type_protein) and self.can_grow_harvester():
                is_case_havesterable = True
        return is_case_havesterable

    def get_harvesterable_protein(self, root, type_protein):  
        case_harvesterable = None
        organ_from = None
        dir_harvester = 'N'
        organs_from_root = self.get_my_organs_from_root(root)
        for organ in organs_from_root:
            x = organ.x
            y = organ.y
            if (self.cases[x][y-2].type_case == type_protein if y>1 else False
                and self.cases[x][y-1].type_case == 'EMPTY'):
                case_harvesterable = self.cases[x][y-1]
                organ_from = organ
                dir_harvester = 'N'
            if (self.cases[x-2][y].type_case == type_protein if x>1 else False
                and self.cases[x-1][y].type_case == 'EMPTY'):
                case_harvesterable = self.cases[x-1][y]
                organ_from = organ
                dir_harvester = 'W'
            if (self.cases[x][y+2].type_case == type_protein and self.cases[x][y+1].type_case == 'EMPTY') if y<self.height-2 else False:
                print(f"DEBUG {self.cases[x][y+1]} {self.cases[x][y+1].type_case == 'EMPTY'} {(self.cases[x][y+2].type_case == type_protein and self.cases[x][y+1].type_case == 'EMPTY') if y<self.height-2 else False :}", file=sys.stderr, flush=True)
                case_harvesterable = self.cases[x][y+1]
                organ_from = organ
                dir_harvester = 'S'
            if (self.cases[x+2][y].type_case == type_protein if x<width-2 else False
                and self.cases[x+1][y].type_case == 'EMPTY'):
                case_harvesterable = self.cases[x+1][y]
                organ_from = organ
                dir_harvester = 'E'
        return case_harvesterable, dir_harvester, organ_from

def distance(point1_x, point1_y, point2_x, point2_y):
    return math.sqrt((point2_x - point1_x)**2 + (point2_y - point1_y)**2)

#def distance(point1, point2):
#    return math.sqrt((point2['x'] - point1['x'])**2 + (point2['y'] - point1['y'])**2)

#def get_protein_nearest(organ, proteins):
#    distance_protein_nearest = 1000
#    protein_nearest = None
#    for protein in proteins:
#        dist = distance(organ, protein)
#        if distance_protein_nearest > dist:
#            protein_nearest = protein
#            distance_protein_nearest = dist
#
#    return protein_nearest

def run_strategie_0(loop_counter, game_grid, root) :#-> str:
    print(f"DEBUG strategy 0 - {root}", file=sys.stderr, flush=True)
    last_organ = game_grid.get_my_organ_with_max_empty_cases(root)
    empty_case_nearest = game_grid.get_empty_case_nearest(last_organ.x, last_organ.y)
    if empty_case_nearest:
        print(f"DEBUG strategy 0 - grow empty_case_nearest", file=sys.stderr, flush=True)
        return f"GROW {root.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC"
    else:
        move_case = game_grid.get_move_case(last_organ.x, last_organ.y)
        if move_case:
            print(f"DEBUG strategy 0 - grow move_case", file=sys.stderr, flush=True)
            return f"GROW {root.id} {move_case.x} {move_case.y} BASIC"
        else:
            return "WAIT blocked"    
    

def run_strategie_1(loop_counter, game_grid, root):
    print(f"DEBUG strategy 1 - {root}", file=sys.stderr, flush=True)
    run_strategie_0(loop_counter, game_grid, root)

def run_strategie_2(loop_counter, game_grid, root):

    print(f"DEBUG strategy 2 - {root}", file=sys.stderr, flush=True)
    #### DECISION

    # 1/ je veux viser les proteines "libres"
    #       il existe des cases libres autour de la proteine la plus proche
    #       il existe un root qui est à portée de la protéine la plus proche 
    #       (a/ soit en construisant des basic + un harvester de l'organ le plus proche jusqu'à la proteine 
    #        b/ soit avec un spore )
    # 2/ je veux viser les roots adverses à détruire
    # 3/ je veux occuper un maximum de cases libres


    #####
    


    # je veux connaitre la proteine A la plus proche qui soit libre, 
    # pour laquelle il n'y a pas de root plus proche
    # si au moins une protein A existe 
    # et si la plus proche protein A libre est la plus proche du root
    #   alors on essaie d'atteindre la proteine A soit par BASIC soit par SPORE
    # sinon si les ressources sont suffisantes (possible de grow BASIC et les autres root peuvent atteindre leurs objectifs)
    #   alors grow BASIC des cases autour du root
    # sinon 
    #   alors WAIT 

    empty_case_nearest = game_grid.get_empty_case_nearest(root.x, root.y)
    if (loop_counter == 1 and not empty_case_nearest):
        print(f"DEBUG strategy 2 - {root} - no empty near root ", file=sys.stderr, flush=True)
        move_case = game_grid.get_move_case(root.x, root.y)
        if move_case:
            return f"GROW {root.id} {move_case.x} {move_case.y} BASIC "
        else:
            return "WAIT blocked"
    else:
        free_protein_a_nearest_root = game_grid.get_free_protein_nearest(root, 'A')

        print(f"DEBUG strategy 2 - {root} - search free_protein_a:{free_protein_a_nearest_root}", file=sys.stderr, flush=True)
        if free_protein_a_nearest_root:

            last_organ = game_grid.get_my_organ_nearest(free_protein_a_nearest_root)
            empty_case_nearest = game_grid.get_empty_case_nearest(last_organ.x, last_organ.y)

            # stratégie 1/ a/ - proche -> by BASIC
            if distance(last_organ.x, last_organ.y, free_protein_a_nearest_root.x, free_protein_a_nearest_root.y) < game_grid.my_a: 
                empty_case_nearest = game_grid.get_empty_cases_nearest_target(last_organ.x, last_organ.y, free_protein_a_nearest_root.x, free_protein_a_nearest_root.y)
                print(f"DEBUG strategy 2 - {root} - 1a free_protein_a by basic - empty_case_nearest:{empty_case_nearest}", file=sys.stderr, flush=True)
                if (free_protein_a_nearest_root 
                    and empty_case_nearest
                    and free_protein_a_nearest_root.x == empty_case_nearest.x
                    and free_protein_a_nearest_root.y == empty_case_nearest.y + 1
                ) :
                    return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER S"
                elif (free_protein_a_nearest_root 
                    and empty_case_nearest
                    and free_protein_a_nearest_root.x == empty_case_nearest.x 
                    and free_protein_a_nearest_root.y == empty_case_nearest.y - 1
                ) :
                    return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER N"
                elif (free_protein_a_nearest_root 
                    and empty_case_nearest
                    and free_protein_a_nearest_root.x == empty_case_nearest.x + 1
                    and free_protein_a_nearest_root.y == empty_case_nearest.y
                ) :
                    return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER E"
                elif (free_protein_a_nearest_root 
                    and empty_case_nearest
                    and free_protein_a_nearest_root.x == empty_case_nearest.x - 1 
                    and free_protein_a_nearest_root.y == empty_case_nearest.y
                ) :
                    return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER W"
                else :
                    empty_case_nearest = game_grid.get_empty_cases_nearest_target(last_organ.x, last_organ.y, free_protein_a_nearest_root.x, free_protein_a_nearest_root.y)
                    if empty_case_nearest:
                        return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC"

                    # check opportunité pour spore tentacule
                    elif game_grid.is_tentaculable(root):
                        case_tentaculable, dir_tentaculable, organ_from = game_grid.get_tentaculable(root)
                        print(f"DEBUG strategy 2 - {root} - 1abis exist opportunity tentacle ", file=sys.stderr, flush=True)
                        return f"GROW {organ_from.id} {case_tentaculable.x} {case_tentaculable.y} HARVESTER {dir_tentaculable}"

                    # check opportunité pour havester A
                    elif game_grid.my_b > 3 and game_grid.my_c > 3 and game_grid.my_d > 3 and game_grid.is_harvesterable_protein(root,'A'):
                        case_harvesterable_protein, dir_harvester, organ_from = game_grid.get_harvesterable_protein(root,'A')
                        print(f"DEBUG strategy 2 - {root} - 1abis exist opportunity havester protein A", file=sys.stderr, flush=True)
                        return f"GROW {organ_from.id} {case_harvesterable_protein.x} {case_harvesterable_protein.y} HARVESTER {dir_harvester}"

                    # check opportunité pour havester B
                    elif game_grid.my_b < 3 and game_grid.is_harvesterable_protein(root,'B'):
                        case_harvesterable_protein, dir_harvester, organ_from = game_grid.get_harvesterable_protein(root,'B')
                        print(f"DEBUG strategy 2 - {root} - 1abis exist opportunity havester protein B", file=sys.stderr, flush=True)
                        return f"GROW {organ_from.id} {case_harvesterable_protein.x} {case_harvesterable_protein.y} HARVESTER {dir_harvester}"

                    # check opportunité pour havester C
                    elif game_grid.my_c < 3 and game_grid.is_harvesterable_protein(root,'C'):
                        case_harvesterable_protein, dir_harvester, organ_from = game_grid.get_harvesterable_protein(root,'C')
                        print(f"DEBUG strategy 2 - {root} - 1abis exist opportunity havester protein C", file=sys.stderr, flush=True)
                        return f"GROW {organ_from.id} {case_harvesterable_protein.x} {case_harvesterable_protein.y} HARVESTER {dir_harvester}"

                    # check opportunité pour havester D
                    elif game_grid.my_d < 3 and game_grid.is_harvesterable_protein(root,'D'):
                        case_harvesterable_protein, dir_harvester, organ_from = game_grid.get_harvesterable_protein(root,'D')
                        print(f"DEBUG strategy 2 - {root} - 1abis exist opportunity havester protein D", file=sys.stderr, flush=True)
                        return f"GROW {organ_from.id} {case_harvesterable_protein.x} {case_harvesterable_protein.y} HARVESTER {dir_harvester}"

                    
                    else:
                        last_organ = game_grid.get_my_organ_with_max_empty_cases(root)
                        empty_case_nearest = game_grid.get_empty_case_nearest(last_organ.x, last_organ.y) if last_organ else None
                        if empty_case_nearest:
                            return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC"
                        else : 
                            return "WAIT 1a"
            # stratégie 1/ b/ - loin -> by spore
            else:

                print(f"DEBUG strategy 2 - {root} - 1b free_protein_a by sporer", file=sys.stderr, flush=True)
                #sporer = game_grid.get_sporer_near(root.x, root.y)
                sporer = game_grid.get_sporer_to_target_near_root(root, free_protein_a_nearest_root)

                if not sporer and game_grid.can_grow_sporer_root_harvester():# check des ressources
                    dir = 'E'
                    if free_protein_a_nearest_root.x < root.x:
                        dir = 'W'

                    if root.y == free_protein_a_nearest_root.y:
                        return f"GROW {root.id} {root.x + 1} {root.y} SPORER {dir}"
                    elif root.y < free_protein_a_nearest_root.y:
                        return f"GROW {root.id} {root.x} {root.y + 1} SPORER {dir}"
                    elif root.y > free_protein_a_nearest_root.y:
                        return f"GROW {root.id} {root.x} {root.y - 1} SPORER {dir}"
                elif (free_protein_a_nearest_root.x > last_organ.x + 1 
                    and sporer 
                    and not game_grid.is_root(free_protein_a_nearest_root.x - 2, sporer.y)):
                    return f"SPORE {sporer.id} {free_protein_a_nearest_root.x - 2} {sporer.y} "
                elif (free_protein_a_nearest_root.x < last_organ.x + 1 
                    and sporer 
                    and not game_grid.is_root(free_protein_a_nearest_root.x + 2, sporer.y)):
                    return f"SPORE {sporer.id} {free_protein_a_nearest_root.x + 2} {sporer.y} "
                
                # check opportunité pour havester D
                elif game_grid.is_harvesterable_protein(root,'D'):
                    case_harvesterable_protein, dir_harvester, organ_from = game_grid.get_harvesterable_protein(root,'D')
                    print(f"DEBUG strategy 2 - {root} - 1e exist opportunity havester protein D", file=sys.stderr, flush=True)
                    return f"GROW {organ_from.id} {case_harvesterable_protein.x} {case_harvesterable_protein.y} HARVESTER {dir_harvester}"

                elif not game_grid.can_grow_sporer_root_harvester() :
                    print(f"DEBUG strategy 2 - {root} - 1c not enough resources to build sporer", file=sys.stderr, flush=True)
                    free_protein_d_nearest_root = game_grid.get_free_protein_nearest(root, 'D')
                    print(f"DEBUG strategy 2 - {root} - search free_protein_d:{free_protein_d_nearest_root}", file=sys.stderr, flush=True)
                    if free_protein_d_nearest_root:
                        last_organ = game_grid.get_my_organ_nearest(free_protein_d_nearest_root)
                        empty_case_nearest = game_grid.get_empty_cases_nearest_target(last_organ.x, last_organ.y, free_protein_d_nearest_root.x, free_protein_d_nearest_root.y)
                        print(f"DEBUG strategy 2 - {root} - 1a free_protein_d by basic - empty_case_nearest:{empty_case_nearest}", file=sys.stderr, flush=True)
                        if (free_protein_d_nearest_root 
                            and empty_case_nearest
                            and free_protein_d_nearest_root.x == empty_case_nearest.x
                            and free_protein_d_nearest_root.y == empty_case_nearest.y + 1
                        ) :
                            return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER S"
                        elif (free_protein_d_nearest_root 
                            and empty_case_nearest
                            and free_protein_d_nearest_root.x == empty_case_nearest.x 
                            and free_protein_d_nearest_root.y == empty_case_nearest.y - 1
                        ) :
                            return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER N"
                        elif (free_protein_d_nearest_root 
                            and empty_case_nearest
                            and free_protein_d_nearest_root.x == empty_case_nearest.x + 1
                            and free_protein_d_nearest_root.y == empty_case_nearest.y
                        ) :
                            return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER E"
                        elif (free_protein_d_nearest_root 
                            and empty_case_nearest
                            and free_protein_d_nearest_root.x == empty_case_nearest.x - 1 
                            and free_protein_d_nearest_root.y == empty_case_nearest.y
                        ) :
                            return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} HARVESTER O"
                        else :
                            empty_case_nearest = game_grid.get_empty_cases_nearest_target(last_organ.x, last_organ.y, free_protein_d_nearest_root.x, free_protein_d_nearest_root.y)
                            if empty_case_nearest:
                                return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC"
                            else:
                                last_organ = game_grid.get_my_organ_with_max_empty_cases(root)
                                empty_case_nearest = game_grid.get_empty_case_nearest(last_organ.x, last_organ.y) if last_organ else None
                                if empty_case_nearest:
                                    return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC"
                                else : 
                                    return "WAIT 1c"
                    # stratégie 3 pour 1c
                    elif game_grid.my_a > 4:
                        print(f"DEBUG strategy 2 - {root} - 3 no free_protein_d - expansion", file=sys.stderr, flush=True)
                        #last_organ = game_grid.get_my_last_organ(root)
                        last_organ = game_grid.get_my_organ_with_max_empty_cases(root)
                        empty_case_nearest = game_grid.get_empty_case_nearest(last_organ.x, last_organ.y) if last_organ else None
                        if empty_case_nearest:
                            return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC"
                        else : 
                            last_organ = game_grid.get_my_organ_with_protein_near(root)
                            move_case = game_grid.get_move_case(last_organ.x, last_organ.y) if last_organ else None
                            if move_case:
                                return f"GROW {root.id} {move_case.x} {move_case.y} BASIC"
                            else:
                                return "WAIT 3"
                    else:
                        return "WAIT 1c"
                else : 
                    return "WAIT 1b"


        # stratégie 3
        elif game_grid.my_a > 4:
            print(f"DEBUG strategy 2 - {root} - 3 no free_protein_a - expansion", file=sys.stderr, flush=True)
            #last_organ = game_grid.get_my_last_organ(root)
            last_organ = game_grid.get_my_organ_with_max_empty_cases(root)
            empty_case_nearest = game_grid.get_empty_case_nearest(last_organ.x, last_organ.y) if last_organ else None
            if empty_case_nearest:
                return f"GROW {last_organ.id} {empty_case_nearest.x} {empty_case_nearest.y} BASIC"
            else : 
                last_organ = game_grid.get_my_organ_with_protein_near(root)
                move_case = game_grid.get_move_case(last_organ.x, last_organ.y) if last_organ else None
                if move_case:
                    return f"GROW {root.id} {move_case.x} {move_case.y} BASIC"
                else:
                    return "WAIT 3"
        else : 
            print(f"DEBUG strategy 2 - {root} - 3 no free_protein_a - not enough resources", file=sys.stderr, flush=True)
            return "WAIT 4"




# width: columns in the game grid
# height: rows in the game grid
width, height = [int(i) for i in input().split()]
game_grid = GameGrid(width, height)

# game loop
loop_counter = 0
GAME_STRATEGY = 0 # 0 strategie par défaut, 1 stratégie quand beaucoup de WALL, 2 stratégie quand proteines atteignables
while True:
    loop_counter += 1
    my_organisms = []
    proteins = []
    entity_count = int(input())
    for i in range(entity_count):
        inputs = input().split()
        x = int(inputs[0])
        y = int(inputs[1])  # grid coordinate
        _type = inputs[2]  # WALL, ROOT, BASIC, TENTACLE, HARVESTER, SPORER, A, B, C, D
        owner = int(inputs[3])  # 1 if your organ, 0 if enemy organ, -1 if neither
        organ_id = int(inputs[4])  # id of this entity if it's an organ, 0 otherwise
        organ_dir = inputs[5]  # N,E,S,W or X if not an organ
        organ_parent_id = int(inputs[6])
        organ_root_id = int(inputs[7])
        
        game_grid.cases[x][y].type_case = _type
        game_grid.cases[x][y].owner = owner
        game_grid.cases[x][y].id = organ_id
        game_grid.cases[x][y].dir = organ_dir
        game_grid.cases[x][y].parent_id = organ_parent_id
        game_grid.cases[x][y].root_id = organ_root_id
        if owner == 1 and _type == 'ROOT':
            game_grid.set_origin_root(x, y)
        if owner == 1:
            my_organ = {'id': organ_id, 'x': x, 'y': y, 'type': _type}
            my_organisms.append(my_organ)
        if _type == 'A' or _type == 'B' or _type == 'C' or _type == 'D':
            proteins.append({'id': organ_id, 'x': x, 'y': y, 'type': _type})
    # my_d: your protein stock
    my_a, my_b, my_c, my_d = [int(i) for i in input().split()]
    game_grid.set_my_protein(my_a, my_b, my_c, my_d)
    # opp_d: opponent's protein stock
    opp_a, opp_b, opp_c, opp_d = [int(i) for i in input().split()]
    required_actions_count = int(input())  # your number of organisms, output an action for each one in any order
    

    # Analyse du terrain pour choisir la meilleure stratégie par rapport au contexte
    #   -> si beaucoup de WALL (à déterminer le ratio ou le calcul), 
    #      alors on cherche les plus grandes distances et on spore puis expansion
    #   -> sinon si les proteines A sont à portée, 
    #      alors on cherche les protéines A et expansion la plus lointaine possible
    #   -> sinon expansion de proche en proche

    # dans quelle stratégie, on va essayer de bouffer l'adversaire ?
    
    if loop_counter == 1:
        nb_wall = len(game_grid.get_walls())
        if nb_wall > (width*height/4):
            GAME_STRATEGY = 1
        elif len(game_grid.get_all_proteins()) > 4:
            GAME_STRATEGY = 2
    
    print(f"DEBUG GAME_STRATEGY:{GAME_STRATEGY}", file=sys.stderr, flush=True)


    for root in game_grid.get_my_roots():
        print(f"DEBUG root:{root}", file=sys.stderr, flush=True)
    #for i in range(required_actions_count):

        msg_action = "WAIT default"

        if GAME_STRATEGY == 2:
            msg_action = run_strategie_2(loop_counter, game_grid, root)
        elif GAME_STRATEGY == 1:
            msg_action = run_strategie_1(loop_counter, game_grid, root)
        else:
            msg_action = run_strategie_0(loop_counter, game_grid, root)

        print(msg_action)



# TODO: si proteine à proximité immédiate d'un organ, alors il faut construire un harvester
# TODO: si proteine a un harvester ennemy alors on le bouffe
# TODO: si root ennemy proche, alors on va construire un tentacule