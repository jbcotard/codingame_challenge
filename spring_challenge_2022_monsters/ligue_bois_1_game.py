import sys
import math




def _sort_entite_with_distance_to_base(e):
    return e['distance']

class Game:
    ''' Represente une Game '''

    def __init__(self, x, y):
        self.base_x = x 
        self.base_y = y
        self.health = 0
        self.mana = 0
        self.monsters = []
        self.heroes = []
        self.opponent_heroes = [] 
    
    def set_health(self, health):
        self.health = health

    def set_mana(self, mana):
        self.mana = mana
    
    def register_monster(self, cls):
        self.monsters.append(cls)

    def register_hero(self, cls):
        self.heroes.append(cls)

    def register_opponent_hero(self, cls):
        self.opponent_heroes.append(cls)

    def reset_turn(self):
        self.monsters = []
        self.heroes = []
        self.opponent_heroes = [] 

    def do_action_hero(self):
        
        _monsters_with_distance_to_base = []
        #print(f" {len(self.monsters)}", file=sys.stderr, flush=True)
        for monster in self.monsters:
            #print(f" {monster}", file=sys.stderr, flush=True)
            #print(f" {self}", file=sys.stderr, flush=True)
            distance_base_monster = math.dist((self.base_x,self.base_y), (monster.get_x(), monster.get_y()))
            _monsters_with_distance_to_base.append({'monster': monster, 'distance': distance_base_monster})

        _monsters_with_distance_to_base.sort(key=_sort_entite_with_distance_to_base)


        if len(_monsters_with_distance_to_base) == 2:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print("WAIT")

            print(f"MOVE {_monsters_with_distance_to_base[1]['monster'].get_x()} {_monsters_with_distance_to_base[1]['monster'].get_y()}")

        elif len(_monsters_with_distance_to_base) == 1:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print("WAIT")
            print("WAIT")

        elif len(_monsters_with_distance_to_base) == 0:
            print("WAIT")
            print("WAIT")
            print("WAIT")
        else:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print("WAIT")
            print(f"MOVE {_monsters_with_distance_to_base[1]['monster'].get_x()} {_monsters_with_distance_to_base[1]['monster'].get_y()}")
            #print(f"MOVE {_monsters_with_distance_to_base[2]['monster'].get_x()} {_monsters_with_distance_to_base[2]['monster'].get_y()}")
            


class Entite:
    ''' Represente une Entité '''
    def __init__(self, _id, _type, x, y, shieldLife, isControlled):
        self._id = _id
        self._type = _type
        self.x = x
        self.y = y
        self.shieldLife = shieldLife
        self.isControlled = isControlled




class Monstre(Entite):
    ''' Represente un Monstre '''
    def __init__(self, _id, _type, x, y, shieldLife, isControlled, health, vx, vy, nearBase, threatFor):
        super().__init__(_id, _type, x, y, shieldLife, isControlled)
        self.health = health
        self.vx = vx
        self.vy = vy
        self.nearBase = nearBase
        self.threatFor = threatFor
        
    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    

class Hero(Entite):
    ''' Represente un Hero '''

class OpponentHero(Entite):
    ''' Represente un Opponent Hero '''

# base_x: The corner of the map representing your base
base_x, base_y = [int(i) for i in input().split()]
heroes_per_player = int(input())  # Always 3
game = Game(base_x, base_y)
# game loop
while True:

    game.reset_turn()

    for i in range(2):
        # health: Your base health
        # mana: Ignore in the first league; Spend ten mana to cast a spell
        health, mana = [int(j) for j in input().split()]
        game.set_health(health)
        game.set_mana(mana)
    entity_count = int(input())  # Amount of heros and monsters you can see
    for i in range(entity_count):
        # _id: Unique identifier
        # _type: 0=monster, 1=your hero, 2=opponent hero
        # x: Position of this entity
        # shield_life: Ignore for this league; Count down until shield spell fades
        # is_controlled: Ignore for this league; Equals 1 when this entity is under a control spell
        # health: Remaining health of this monster
        # vx: Trajectory of this monster
        # near_base: 0=monster with no target yet, 1=monster targeting a base
        # threat_for: Given this monster's trajectory, is it a threat to 1=your base, 2=your opponent's base, 0=neither
        _id, _type, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for = [int(j) for j in input().split()]
        #print(f" {_type}", file=sys.stderr, flush=True)
        if _type == 0:
            game.register_monster(Monstre(_id, _type, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for))

            #print(f" {len(game.monsters)}", file=sys.stderr, flush=True)
        elif _type == 1:
            game.register_hero(Hero(_id, _type, x, y, shield_life, is_controlled))
        elif _type == 2:
            game.register_opponent_hero(OpponentHero(_id, _type, x, y, shield_life, is_controlled))
    #for i in range(heroes_per_player):

        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr, flush=True)


        # In the first league: MOVE <x> <y> | WAIT; In later leagues: | SPELL <spellParams>;
        #print("WAIT")
    #print("MOVE 1500 350")
    #print("MOVE 1200 1200")
    #print("MOVE 350 1500")
    game.do_action_hero()
