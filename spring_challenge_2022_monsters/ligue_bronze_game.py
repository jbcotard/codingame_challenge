import sys
import math



def _sort_entite_with_distance_to_base(e):
    return e['distance']

def _is_no_hero_near_base(opponent_heroes, base_opponent_x, base_opponent_y):
    _is_far_base = True
    for opponent_hero in opponent_heroes:
        if math.dist((opponent_hero.x, opponent_hero.y), (base_opponent_x, base_opponent_y)) < 4000:
            _is_far_base = False
    return _is_far_base

class Game:
    ''' Represente une Game '''

    def __init__(self, x, y):
        self.base_x = x 
        self.base_y = y
        if x == 0:
            self.base_opponent_x = 17600 
            self.base_opponent_y = 8990
        else :
            self.base_opponent_x = 0 
            self.base_opponent_y = 0
        self.base_health = 0
        self.base_mana = 0
        self.monsters = []
        self.heroes = []
        self.opponent_heroes = [] 
        self.hero1_x_prec = 0
        self.hero1_y_prec = 0
        self.count_turn = 0
    
    def set_base_health(self, base_health):
        self.base_health = base_health

    def set_base_mana(self, base_mana):
        self.base_mana = base_mana
    
    def register_monster(self, cls):
        self.monsters.append(cls)

    def register_hero(self, cls):
        self.heroes.append(cls)

    def register_opponent_hero(self, cls):
        self.opponent_heroes.append(cls)

    def reset_turn(self):
        self.monsters = []
        self.heroes = []
        self.opponent_heroes = [] 

    def do_action_hero(self):
        
      # stratégie attaque :
        #     si base health  = 3 , alors heros 1 va à l'attaque (à une distance de n de la base adverse, il envoie des sorts de control et de shield sur la base adverse)
        #.                           et heros 2 en defensive à repousser avec des wind vers le centre de la map
        #                            et heros 3 detruit le monstre le plus proche de la base
        
        # stratégie mi défensive :
        #     si base health  = 2, alors hero 1 et heros 3 detruisent le monstre le plus proche de la base
        #.                           et heros 2 en defensive à repousser avec des wind vers le centre de la map


        # stratégie ultra défensive :
        #     si base health  = 1, alors hero 1 detruit le monstre le plus proche de la base
        #.                           et heros 2 et 3 en defensive à repousser avec des wind vers le centre de la map

        if self.base_health == 3:
            #if self.base_mana < 50:
            #    print(f" {self.base_health} => stratégie mi defensive", file=sys.stderr, flush=True)    
            #    self.do_strategie_mi_defensive()
            #else:
            print(f" {self.base_health} => stratégie attaque", file=sys.stderr, flush=True)
            self.do_strategie_attack() 
        elif self.base_health == 2:    
            #print(f" {self.base_health} => stratégie mi defensive", file=sys.stderr, flush=True)    
            #self.do_strategie_mi_defensive()
            print(f" {self.base_health} => stratégie attaque", file=sys.stderr, flush=True)
            self.do_strategie_attack() 
        else:
            #print(f" {self.base_health} => stratégie ulra defensive", file=sys.stderr, flush=True)
            #self.do_strategie_ultra_defensive() 
            print(f" {self.base_health} => stratégie mi defensive", file=sys.stderr, flush=True)    
            self.do_strategie_mi_defensive()

    def do_strategie_ultra_defensive(self):

        """ ACTION ULTRA DEFENSIVE """

        _monsters_with_distance_to_base = []
        for monster in self.monsters:
            distance_base_monster = math.dist((self.base_x,self.base_y), (monster.get_x(), monster.get_y()))
            _monsters_with_distance_to_base.append({'monster': monster, 'distance': distance_base_monster})

        _monsters_with_distance_to_base.sort(key=_sort_entite_with_distance_to_base)


        if len(_monsters_with_distance_to_base) == 2:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print("WAIT")
            
            if math.dist((self.heroes[2].x,self.heroes[2].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print(f"MOVE {self.base_x} {self.base_y} ")

        elif len(_monsters_with_distance_to_base) == 1:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print("WAIT")
            if math.dist((self.heroes[2].x,self.heroes[2].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print(f"MOVE {self.base_x} {self.base_y} ")

        elif len(_monsters_with_distance_to_base) == 0:
            print(f"MOVE {self.base_x} {self.base_y} ")
            print("WAIT")
            print(f"MOVE {self.base_x} {self.base_y} ")
        else:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print("WAIT")
            if math.dist((self.heroes[2].x,self.heroes[2].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                print(f"MOVE {self.base_x} {self.base_y} ")

    def do_strategie_attack(self):

        """ ACTION ATTACK """
        _X_pos_1 = 11700
        _Y_pos_1 = 6200
        _X_pos_2 = 14000
        _Y_pos_2 = 3700

        _attack_pos_start_x = 11700 if self.base_x == 0 else 3000
        _attack_pos_start_y = 7000 if self.base_x == 0 else 5000
        _attack_pos_end_x = 14000 if self.base_x == 0 else 6000
        _attack_pos_end_y = 3700 if self.base_x == 0 else 1700
        _middle_pos_start_x = 10000
        _middle_pos_start_y = 2150
        _middle_pos_end_x = 6600
        _middle_pos_end_y = 6400

        _monsters_with_distance_to_base = []
        _monsters_with_distance_to_hero1 = []
        for monster in self.monsters:
            distance_base_monster = math.dist((self.base_x,self.base_y), (monster.get_x(), monster.get_y()))
            _monsters_with_distance_to_base.append({'monster': monster, 'distance': distance_base_monster})
            distance_hero1_monster = math.dist((self.heroes[0].x,self.heroes[0].y), (monster.get_x(), monster.get_y()))
            _monsters_with_distance_to_hero1.append({'monster': monster, 'distance': distance_hero1_monster})

        _monsters_with_distance_to_base.sort(key=_sort_entite_with_distance_to_base)
        _monsters_with_distance_to_hero1.sort(key=_sort_entite_with_distance_to_base)

        # action hero1

        print(f" nb monstre {len(self.monsters)}", file=sys.stderr, flush=True)
        if self.count_turn < 50:

            _monsters_near_with_hero1 = [x for x in _monsters_with_distance_to_hero1 if not x['distance'] < 1280 ]

            if self.hero1_x_prec != 0 and len(_monsters_near_with_hero1) > 0:
                print(f"MOVE {_monsters_near_with_hero1[0]['monster'].x} {_monsters_near_with_hero1[0]['monster'].y} ")
            else :

                if self.hero1_x_prec == 0 or self.hero1_x_prec == _middle_pos_end_x:
                    if self.heroes[0].x != _middle_pos_start_x and self.heroes[0].y != _middle_pos_start_y:
                        print(f"MOVE {_middle_pos_start_x} {_middle_pos_start_y}")
                    else : 
                        self.hero1_x_prec = _middle_pos_start_x
                        self.hero1_y_prec = _middle_pos_start_y
                        print(f"MOVE {_middle_pos_end_x} {_middle_pos_end_y}")
                else:
                    if self.heroes[0].x != _middle_pos_end_x and self.heroes[0].y != _middle_pos_end_y:
                        print(f"MOVE {_middle_pos_end_x} {_middle_pos_end_y}")
                    else : 
                        self.hero1_x_prec = _middle_pos_end_x
                        self.hero1_y_prec = _middle_pos_end_y
                        print(f"MOVE {_middle_pos_start_x} {_middle_pos_start_y}")
        else:

            if self.hero1_x_prec == 0:
                if self.heroes[0].x != _attack_pos_start_x and self.heroes[0].y != _attack_pos_start_y:
                    print(f"MOVE {_attack_pos_start_x} {_attack_pos_start_y}")
                else : 
                    self.hero1_x_prec = _attack_pos_start_x
                    self.hero1_y_prec = _attack_pos_start_y
                    print(f"MOVE {_attack_pos_end_x} {_attack_pos_end_y}")
            else:
                if self.base_mana > 50 :
                    _monsters_near_with_hero1 = [x for x in _monsters_with_distance_to_hero1 if x['distance'] < 1280 and math.dist((x['monster'].x,x['monster'].y),(self.base_opponent_x,self.base_opponent_y)) < 7000]
                    if len(_monsters_near_with_hero1) > 0:
                        print(f"SPELL WIND {self.base_opponent_x} {self.base_opponent_y} ")
                    else :
                        if  self.hero1_x_prec == _attack_pos_start_x:
                            if self.heroes[0].x != _attack_pos_end_x and self.heroes[0].y != _attack_pos_end_y:
                                print(f"MOVE {_attack_pos_end_x} {_attack_pos_end_y}")
                            else : 
                                self.hero1_x_prec = _attack_pos_end_x
                                self.hero1_y_prec = _attack_pos_end_y
                                print(f"MOVE {_attack_pos_start_x} {_attack_pos_start_y}")
                        else:
                            if self.heroes[0].x != _attack_pos_start_x and self.heroes[0].y != _attack_pos_start_y:
                                print(f"MOVE {_attack_pos_start_x} {_attack_pos_start_y}")
                            else : 
                                self.hero1_x_prec = _attack_pos_start_x
                                self.hero1_y_prec = _attack_pos_start_y
                                print(f"MOVE {_attack_pos_end_x} {_attack_pos_end_y}")
                else :
                    _monsters_near_with_hero1 = [x for x in _monsters_with_distance_to_hero1 if  x['distance'] < 1280 and math.dist((x['monster'].x,x['monster'].y),(self.base_opponent_x,self.base_opponent_y)) < 7000]
                    if len(_monsters_near_with_hero1) > 0:
                        if _is_no_hero_near_base(self.opponent_heroes, self.base_opponent_x, self.base_opponent_y) and self.base_mana >= 10:
                            print(f" {_monsters_near_with_hero1[0]['monster']._id} : {_monsters_near_with_hero1[0]['distance']}", file=sys.stderr, flush=True)
            
                            print(f"SPELL WIND {self.base_opponent_x} {self.base_opponent_y} ")
                        else:
                            print(f"MOVE {_monsters_near_with_hero1[0]['monster'].x} {_monsters_near_with_hero1[0]['monster'].y} ")
                    else :
                        if  self.hero1_x_prec == _attack_pos_start_x:
                            if self.heroes[0].x != _attack_pos_end_x and self.heroes[0].y != _attack_pos_end_y:
                                print(f"MOVE {_attack_pos_end_x} {_attack_pos_end_y}")
                            else : 
                                self.hero1_x_prec = _attack_pos_end_x
                                self.hero1_y_prec = _attack_pos_end_y
                                print(f"MOVE {_attack_pos_start_x} {_attack_pos_start_y}")
                        else:
                            if self.heroes[0].x != _attack_pos_start_x and self.heroes[0].y != _attack_pos_start_y:
                                print(f"MOVE {_attack_pos_start_x} {_attack_pos_start_y}")
                            else : 
                                self.hero1_x_prec = _attack_pos_start_x
                                self.hero1_y_prec = _attack_pos_start_y
                                print(f"MOVE {_attack_pos_end_x} {_attack_pos_end_y}")



        #if len(_monsters_with_distance_to_hero1) > 0 and self.base_mana < 50:
        #    _monsters_uncontrol_with_distance_to_hero1 = [x for x in _monsters_with_distance_to_hero1 if not x['monster'].isControlled]
        #    if len(_monsters_uncontrol_with_distance_to_hero1) > 0 and math.dist((self.base_x,self.base_y), (monster.get_x(), monster.get_y())) > math.dist((self.base_x,self.base_y), (self.heroes[0].x,self.heroes[0].y)):
        #        print(f"MOVE {_monsters_uncontrol_with_distance_to_hero1[0]['monster'].x} {_monsters_uncontrol_with_distance_to_hero1[0]['monster'].y} 4500")
        #    else:
        #        if abs(self.heroes[0].x - self.base_opponent_x) > 2000 :
        #            print("MOVE 8500 4500")
        #        else :
        #            print(f"MOVE {self.base_opponent_x} {self.base_opponent_y} ")
        #elif len(_monsters_with_distance_to_hero1) > 0 :
        #    _monsters_near_with_hero1 = [x for x in _monsters_with_distance_to_hero1 if not x['distance'] < 1280]
        #    if len(_monsters_near_with_hero1) > 0:
        #        print(f"SPELL WIND {self.base_opponent_x} {self.base_opponent_y} ")
        #    else:
        #        print(f"MOVE {self.base_opponent_x} {self.base_opponent_y} ")
            
            #_monsters_control_with_distance_to_hero1 = [x for x in _monsters_with_distance_to_hero1 if x['monster'].isControlled]
            #_monsters_uncontrol_with_distance_to_hero1 = [x for x in _monsters_with_distance_to_hero1 if not x['monster'].isControlled]
            #if len(_monsters_control_with_distance_to_hero1) > 0:
            #    print(f"SPELL SHIELD {_monsters_control_with_distance_to_hero1[0]['monster']._id} ")
            #else :
            #    if len(_monsters_uncontrol_with_distance_to_hero1) > 0 and _monsters_uncontrol_with_distance_to_hero1[0]['monster'].nearBase == 0:
            #        print(f"SPELL CONTROL {_monsters_uncontrol_with_distance_to_hero1[0]['monster']._id} {self.base_opponent_x} {self.base_opponent_y} ")
            #    else :
            #        if self.heroes[0].x != 8500 :
            #            print("MOVE 8500 4500")
            #        else :
            #            print(f"MOVE {self.base_opponent_x} {self.base_opponent_y} ")





            #elif _monsters_with_distance_to_hero1[0]['monster'].nearBase == 0:
            #    print(f"SPELL CONTROL {_monsters_with_distance_to_hero1[0]['monster']._id} {self.base_opponent_x} {self.base_opponent_y} ")
            #else :
            #    print("MOVE 8500 4500")

            #if _monsters_with_distance_to_hero1[0]['monster'].isControlled:
            #    if self.base_mana > 20 and _monsters_with_distance_to_hero1[0]['monster'].shieldLife > 0:
            #        print(f"SPELL SHIELD {_monsters_with_distance_to_hero1[0]['monster']._id} ")
            #    else:
            #        if len(_monsters_with_distance_to_hero1) > 1 and not _monsters_with_distance_to_hero1[1]['monster'].isControlled:
            #            print(f"MOVE {_monsters_with_distance_to_hero1[1]['monster'].x} {_monsters_with_distance_to_hero1[1]['monster'].y} 4500")
            #        else: 
            #            print("MOVE 8500 4500")
            #elif self.base_mana > 20 : 
            #    if _monsters_with_distance_to_hero1[0]['monster'].nearBase == 0:
            #        print(f"SPELL CONTROL {_monsters_with_distance_to_hero1[0]['monster']._id} {self.base_opponent_x} {self.base_opponent_y} ")
            #    else :
            #        print("MOVE 8500 4500")
            #else :
            #    if len(_monsters_with_distance_to_hero1) > 1 and not _monsters_with_distance_to_hero1[1]['monster'].isControlled:
            #        print(f"MOVE {_monsters_with_distance_to_hero1[1]['monster'].x} {_monsters_with_distance_to_hero1[1]['monster'].y} 4500")
            #    else: 
            #        print("MOVE 8500 4500")
        #else:
        #    if self.heroes[0].x != 8500 :
        #        print("MOVE 8500 4500")
        #    else :
        #        print(f"MOVE {self.base_opponent_x} {self.base_opponent_y} ")

        # action hero 2 et 3
        if len(_monsters_with_distance_to_base) == 2:
            print(f" {_monsters_with_distance_to_base[0]['monster']._id} : {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                #print("WAIT")
                print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")

            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")

        elif len(_monsters_with_distance_to_base) == 1:
            print(f" {_monsters_with_distance_to_base[0]['monster']._id} : {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                #print("WAIT")
                print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            #print("WAIT")
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")


        elif len(_monsters_with_distance_to_base) == 0:
            #print("WAIT")
            #print("WAIT")
            if self.base_x == 0:
                print("MOVE 2500 4800")
                print("MOVE 5400 2200")
            else :
                print("MOVE 12000 7000")
                print("MOVE 14700 4000")
        else:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                #print("WAIT")
                print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            




    def do_strategie_mi_defensive(self):

        """ ACTION MI DEFENSIVE """


        _defensive_pos_start_x = 200 if self.base_x == 0 else 17000
        _defensive_pos_start_y = 700 if self.base_x == 0 else 8000

        _monsters_with_distance_to_base = []
        #print(f" {len(self.monsters)}", file=sys.stderr, flush=True)
        for monster in self.monsters:
            #print(f" {monster}", file=sys.stderr, flush=True)
            #print(f" {self}", file=sys.stderr, flush=True)
            distance_base_monster = math.dist((self.base_x,self.base_y), (monster.get_x(), monster.get_y()))
            _monsters_with_distance_to_base.append({'monster': monster, 'distance': distance_base_monster})

        _monsters_with_distance_to_base.sort(key=_sort_entite_with_distance_to_base)


        if len(_monsters_with_distance_to_base) == 2:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                #print("WAIT")
                print(f"MOVE {_defensive_pos_start_x} {_defensive_pos_start_y}")

            print(f"MOVE {_monsters_with_distance_to_base[1]['monster'].get_x()} {_monsters_with_distance_to_base[1]['monster'].get_y()}")

        elif len(_monsters_with_distance_to_base) == 1:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                #print("WAIT")
                print(f"MOVE {_defensive_pos_start_x} {_defensive_pos_start_y}")
            print(f"MOVE {self.heroes[1].x} {self.heroes[1].y} ")

        elif len(_monsters_with_distance_to_base) == 0:
            print(f"MOVE {self.heroes[1].x} {self.heroes[1].y} ")
            #print("WAIT")
            print(f"MOVE {_defensive_pos_start_x} {_defensive_pos_start_y}")
            print(f"MOVE {self.heroes[1].x} {self.heroes[1].y} ")
        else:
            print(f" {_monsters_with_distance_to_base[0]['distance']}", file=sys.stderr, flush=True)
            print(f"MOVE {_monsters_with_distance_to_base[0]['monster'].get_x()} {_monsters_with_distance_to_base[0]['monster'].get_y()}")
            if math.dist((self.heroes[1].x,self.heroes[1].y), (_monsters_with_distance_to_base[0]['monster'].get_x(), _monsters_with_distance_to_base[0]['monster'].get_y())) < 1280:
                print("SPELL WIND 8500 4500 ")
            else :
                #print("WAIT")
                print(f"MOVE {_defensive_pos_start_x} {_defensive_pos_start_y}")
            print(f"MOVE {_monsters_with_distance_to_base[1]['monster'].get_x()} {_monsters_with_distance_to_base[1]['monster'].get_y()}")
            #print(f"MOVE {_monsters_with_distance_to_base[2]['monster'].get_x()} {_monsters_with_distance_to_base[2]['monster'].get_y()}")
            


class Entite:
    ''' Represente une Entité '''
    def __init__(self, _id, _type, x, y, shieldLife, isControlled):
        self._id = _id
        self._type = _type
        self.x = x
        self.y = y
        self.shieldLife = shieldLife
        self.isControlled = isControlled




class Monstre(Entite):
    ''' Represente un Monstre '''
    def __init__(self, _id, _type, x, y, shieldLife, isControlled, health, vx, vy, nearBase, threatFor):
        super().__init__(_id, _type, x, y, shieldLife, isControlled)
        self.health = health
        self.vx = vx
        self.vy = vy
        self.nearBase = nearBase
        self.threatFor = threatFor
        
    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    

class Hero(Entite):
    ''' Represente un Hero '''

class OpponentHero(Entite):
    ''' Represente un Opponent Hero '''

# base_x: The corner of the map representing your base
base_x, base_y = [int(i) for i in input().split()]
heroes_per_player = int(input())  # Always 3
game = Game(base_x, base_y)
# game loop
while True:

    game.reset_turn()

    #for i in range(2):
        # health: Your base health
        # mana: Ignore in the first league; Spend ten mana to cast a spell
    health, mana = [int(j) for j in input().split()]
    game.set_base_health(health)
    game.set_base_mana(mana)
    opponent_health, opponent_mana = [int(j) for j in input().split()]
    entity_count = int(input())  # Amount of heros and monsters you can see
    for i in range(entity_count):
        # _id: Unique identifier
        # _type: 0=monster, 1=your hero, 2=opponent hero
        # x: Position of this entity
        # shield_life: Ignore for this league; Count down until shield spell fades
        # is_controlled: Ignore for this league; Equals 1 when this entity is under a control spell
        # health: Remaining health of this monster
        # vx: Trajectory of this monster
        # near_base: 0=monster with no target yet, 1=monster targeting a base
        # threat_for: Given this monster's trajectory, is it a threat to 1=your base, 2=your opponent's base, 0=neither
        _id, _type, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for = [int(j) for j in input().split()]
        #print(f" {_type}", file=sys.stderr, flush=True)
        if _type == 0:
            game.register_monster(Monstre(_id, _type, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for))

            #print(f" {len(game.monsters)}", file=sys.stderr, flush=True)
        elif _type == 1:
            game.register_hero(Hero(_id, _type, x, y, shield_life, is_controlled))
        elif _type == 2:
            game.register_opponent_hero(OpponentHero(_id, _type, x, y, shield_life, is_controlled))
    #for i in range(heroes_per_player):

        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr, flush=True)


        # In the first league: MOVE <x> <y> | WAIT; In later leagues: | SPELL <spellParams>;
        #print("WAIT")
    #print("MOVE 1500 350")
    #print("MOVE 1200 1200")
    #print("MOVE 350 1500")
    game.do_action_hero()
    game.count_turn += 1
