import sys
import math

# Bring data on patient samples from the diagnosis machine to the laboratory with enough molecules to produce medicine!

def isMissingStorageAForCompleteSample(sample, my_robot):
    if my_robot['storage_a'] + my_robot['expertise_a'] < sample['cost_a']:
        return True
    else: 
        return False
        
def isMissingStorageBForCompleteSample(sample, my_robot):
    if my_robot['storage_b'] + my_robot['expertise_b']  < sample['cost_b']:
        return True
    else: 
        return False
        
def isMissingStorageCForCompleteSample(sample, my_robot):
    if my_robot['storage_c'] + my_robot['expertise_c']  < sample['cost_c']:
        return True
    else: 
        return False
        
def isMissingStorageDForCompleteSample(sample, my_robot):
    if my_robot['storage_d'] + my_robot['expertise_d']  < sample['cost_d']:
        return True
    else: 
        return False
        
def isMissingStorageEForCompleteSample(sample, my_robot):
    if my_robot['storage_e'] + my_robot['expertise_e']  < sample['cost_e']:
        return True
    else: 
        return False

def calculateNbMissingMoleculesForSample(sample, my_robot):
    return ((sample['cost_a']  - my_robot['storage_a'] - my_robot['expertise_a']) + 
    (sample['cost_b'] - my_robot['storage_b'] - my_robot['expertise_b']) + 
    (sample['cost_c'] - my_robot['storage_c'] - my_robot['expertise_c']) +  
    (sample['cost_d'] - my_robot['storage_d'] - my_robot['expertise_d']) +  
    (sample['cost_e'] - my_robot['storage_e'] - my_robot['expertise_e']))

def getMySampleMostCompleted(my_samples, my_robot):
    nb_missing_molecules_most_completed = 100
    sample_most_completed = {}
    for sample in my_samples:
        nb_missing_molecules = calculateNbMissingMoleculesForSample(sample, my_robot)
        if nb_missing_molecules_most_completed > nb_missing_molecules:
            sample_most_completed = sample
            nb_missing_molecules_most_completed = nb_missing_molecules
    return sample_most_completed

def isAllMoleculesAvailableForSample(sample, my_robot, molecules_availables):
    nb_molecules_carry_by_my_robot = my_robot['storage_a'] + my_robot['storage_b'] + my_robot['storage_c'] + my_robot['storage_d'] + my_robot['storage_e']
    if ((nb_molecules_carry_by_my_robot < 10)
    and (sample['cost_a'] - my_robot['storage_a'] - my_robot['expertise_a']) <=  molecules_availables['available_a']
    and (sample['cost_b'] - my_robot['storage_b'] - my_robot['expertise_b']) <=  molecules_availables['available_b'] 
    and (sample['cost_c'] - my_robot['storage_c'] - my_robot['expertise_c']) <=  molecules_availables['available_c']  
    and (sample['cost_d'] - my_robot['storage_d'] - my_robot['expertise_d']) <=  molecules_availables['available_d']  
    and (sample['cost_e'] - my_robot['storage_e'] - my_robot['expertise_e']) <=  molecules_availables['available_e']):
        return True
    elif ((nb_molecules_carry_by_my_robot == 10)
    and (sample['cost_a'] <= my_robot['storage_a'] + my_robot['expertise_a'])
    and (sample['cost_b'] <= my_robot['storage_b'] + my_robot['expertise_b']) 
    and (sample['cost_c'] <= my_robot['storage_c'] + my_robot['expertise_c']) 
    and (sample['cost_d'] <= my_robot['storage_d'] + my_robot['expertise_d']) 
    and (sample['cost_e'] <= my_robot['storage_e'] + my_robot['expertise_e'])):
        return True
    return False

def getSampleCanBeCompleted(my_samples, my_robot, molecules_availables):
    sample_can_be_completed_with_available_molecule = {}
    for sample in my_samples: 
        if isAllMoleculesAvailableForSample(sample, my_robot, molecules_availables):
            sample_can_be_completed_with_available_molecule = sample
    return sample_can_be_completed_with_available_molecule
    
def isSampleCannotBeCompleted(my_samples, my_robot, molecules_availables):
    for sample in my_samples:
         if not isAllMoleculesAvailableForSample(sample, my_robot, molecules_availables):
            return True
    return False 

def isSampleIsComplete(sample, my_robot):
    if ((my_robot['storage_a'] + my_robot['expertise_a']) >= sample['cost_a'] 
    and (my_robot['storage_b'] + my_robot['expertise_b']) >= sample['cost_b'] 
    and (my_robot['storage_c'] + my_robot['expertise_c']) >= sample['cost_c'] 
    and (my_robot['storage_d'] + my_robot['expertise_d']) >= sample['cost_d'] 
    and (my_robot['storage_e'] + my_robot['expertise_e']) >= sample['cost_e']):
        return True
    else:
        return False

def getSampleIsComplete(my_samples, my_robot):
    sample_completed = {}
    liste_samples_complete = [sample for sample in my_samples if isSampleIsComplete(sample, my_robot)]
    if len(liste_samples_complete) > 0:
        sample_completed = liste_samples_complete[0]
    return sample_completed    

def isSampleInMySamplesIsComplete(my_samples, my_robot):
    liste_samples_complete = [sample for sample in my_samples if isSampleIsComplete(sample, my_robot)]
    return len(liste_samples_complete) > 0
    
def sumExpertise(my_robot):
    sum_expertise_of_my_robot = my_robot['expertise_a'] + my_robot['expertise_b'] + my_robot['expertise_c'] + my_robot['expertise_d'] + my_robot['expertise_e']
    return sum_expertise_of_my_robot
    
def getListeExpertiseNeedToBeImproved(my_robot):
    liste_expertise_to_improve = []
    if not ((my_robot['expertise_a'] == my_robot['expertise_b'])
    and (my_robot['expertise_b'] == my_robot['expertise_c'])
    and (my_robot['expertise_c'] == my_robot['expertise_d'])
    and (my_robot['expertise_d'] == my_robot['expertise_e'])):
        if ((my_robot['expertise_a'] +2 < my_robot['expertise_b'])
        or (my_robot['expertise_a'] +2 < my_robot['expertise_c'])
        or (my_robot['expertise_a'] +2 < my_robot['expertise_d'])
        or (my_robot['expertise_a'] +2 < my_robot['expertise_e'])):
            liste_expertise_to_improve.append('expertise_a')
            
        if ((my_robot['expertise_b'] +2 < my_robot['expertise_a'])
        or (my_robot['expertise_b'] +2 < my_robot['expertise_c'])
        or (my_robot['expertise_b'] +2 < my_robot['expertise_d'])
        or (my_robot['expertise_b'] +2 < my_robot['expertise_e'])):
            liste_expertise_to_improve.append('expertise_b')
            
        if ((my_robot['expertise_c'] +2 < my_robot['expertise_a'])
        or (my_robot['expertise_c'] +2 < my_robot['expertise_b'])
        or (my_robot['expertise_c'] +2 < my_robot['expertise_d'])
        or (my_robot['expertise_c'] +2 < my_robot['expertise_e'])):
            liste_expertise_to_improve.append('expertise_c')
            
        if ((my_robot['expertise_d'] +2 < my_robot['expertise_a'])
        or (my_robot['expertise_d'] +2 < my_robot['expertise_b'])
        or (my_robot['expertise_d'] +2 < my_robot['expertise_c'])
        or (my_robot['expertise_d'] +2 < my_robot['expertise_e'])):
            liste_expertise_to_improve.append('expertise_d')    
            
        if ((my_robot['expertise_e'] +2 < my_robot['expertise_a'])
        or (my_robot['expertise_e'] +2 < my_robot['expertise_b'])
        or (my_robot['expertise_e'] +2 < my_robot['expertise_c'])
        or (my_robot['expertise_e'] +2 < my_robot['expertise_d'])):
            liste_expertise_to_improve.append('expertise_e')
    return liste_expertise_to_improve
    
def isMySamplesNotGoodForMyRobotExpertise(my_samples, my_robot):
    isOneAtLeastSampleNotGood = False
    liste_expertise_to_improve = getListeExpertiseNeedToBeImproved(my_robot)
    if len(liste_expertise_to_improve) > 0:
        for sample in my_samples:
            if ((sample['cost_a'] > 0 and 'expertise_a' not in liste_expertise_to_improve)
            or (sample['cost_b'] > 0 and 'expertise_b' not in liste_expertise_to_improve)
            or (sample['cost_c'] > 0 and 'expertise_c' not in liste_expertise_to_improve)
            or (sample['cost_d'] > 0 and 'expertise_d' not in liste_expertise_to_improve)
            or (sample['cost_e'] > 0 and 'expertise_e' not in liste_expertise_to_improve)):
                isOneAtLeastSampleNotGood = True
    return isOneAtLeastSampleNotGood            
    
def getSampleNotGoodForMyRobotExpertise(my_samples, my_robot):
    sample_not_good = {}
    liste_expertise_to_improve = getListeExpertiseNeedToBeImproved(my_robot)
    for sample in my_samples:
        if ((sample['cost_a'] > 0 and 'expertise_a' not in liste_expertise_to_improve)
        or (sample['cost_b'] > 0 and 'expertise_b' not in liste_expertise_to_improve)
        or (sample['cost_c'] > 0 and 'expertise_c' not in liste_expertise_to_improve)
        or (sample['cost_d'] > 0 and 'expertise_d' not in liste_expertise_to_improve)
        or (sample['cost_e'] > 0 and 'expertise_e' not in liste_expertise_to_improve)):
            sample_not_good = sample 
    return sample_not_good

def chooseRankSample(my_samples, my_robot):
    expertise_of_my_robot = sumExpertise(my_robot)
    if expertise_of_my_robot <= 4:
        my_rank_sample_chosen = 1
    elif expertise_of_my_robot <= 8:
        my_rank_sample_chosen = 2
    else: 
        my_rank_sample_chosen = 3
        
    if ((len(my_samples) == 0) 
    and (expertise_of_my_robot > 2) and (expertise_of_my_robot <= 12)):
        my_rank_sample_chosen = 2
    
    return my_rank_sample_chosen

project_count = int(input())
for i in range(project_count):
    a, b, c, d, e = [int(j) for j in input().split()]

# game loop
nb_wait = 0
rediag = False
while True:
    robots = []
    for i in range(2):
        target, eta, score, storage_a, storage_b, storage_c, storage_d, storage_e, expertise_a, expertise_b, expertise_c, expertise_d, expertise_e = input().split()
        eta = int(eta)
        score = int(score)
        storage_a = int(storage_a)
        storage_b = int(storage_b)
        storage_c = int(storage_c)
        storage_d = int(storage_d)
        storage_e = int(storage_e)
        expertise_a = int(expertise_a)
        expertise_b = int(expertise_b)
        expertise_c = int(expertise_c)
        expertise_d = int(expertise_d)
        expertise_e = int(expertise_e)
        robots.append({'id': i, 'target': target, 'eta': eta,
        'storage_a': storage_a, 'storage_b': storage_b, 'storage_c': storage_c, 'storage_d': storage_d, 'storage_e': storage_e,
        'expertise_a': expertise_a, 'expertise_b': expertise_b, 'expertise_c': expertise_c, 'expertise_d': expertise_d, 'expertise_e': expertise_e})
        
    available_a, available_b, available_c, available_d, available_e = [int(i) for i in input().split()]
    molecules_availables = {'available_a': available_a, 'available_b': available_b, 'available_c': available_c, 'available_d': available_d, 'available_e': available_e}
    sample_count = int(input())
    samples = []
    for i in range(sample_count):
        sample_id, carried_by, rank, expertise_gain, health, cost_a, cost_b, cost_c, cost_d, cost_e = input().split()
        sample_id = int(sample_id)
        carried_by = int(carried_by)
        rank = int(rank)
        health = int(health)
        cost_a = int(cost_a)
        cost_b = int(cost_b)
        cost_c = int(cost_c)
        cost_d = int(cost_d)
        cost_e = int(cost_e)
        samples.append({'sample_id':sample_id, 'carried_by':carried_by, 'health':health, 
        'cost_a':cost_a, 'cost_b':cost_b, 'cost_c':cost_c, 'cost_d':cost_d, 'cost_e':cost_e})
        # print(f"{sample_id}:{carried_by},{health}", file=sys.stderr)

    #index_samples_par_health = {sample['health']: sample for sample in samples}
    #print(f"sample health 10 {index_samples_par_health[10]}", file=sys.stderr)
    #print(f"health 10 sampleid = {index_samples_par_health[10]['sample_id']}", file=sys.stderr)
    #index_samples_par_carriedby = {sample['carried_by']: sample for sample in samples}
    #print (f"my sample :{index_samples_par_carriedby[0]}", file=sys.stderr)
   
   
    my_robot = robots[0]
   
    if sample_count > 0:
        #samples_super_health = [sample for sample in samples if sample['health'] == 10]
        #data = samples_super_health[0]['sample_id'] if len(samples_super_health) > 0 else sample_id
        my_samples = [sample for sample in samples if sample['carried_by'] == 0]
        data = my_samples[0]['sample_id'] if len(my_samples) > 0 else 0
    else:
        my_samples = []
    
    expertise_of_my_robot = sumExpertise(my_robot)
    
    my_rank_sample_chosen = chooseRankSample(my_samples, my_robot)
    
    liste_expertise_to_improve = getListeExpertiseNeedToBeImproved(my_robot)
    
    print(my_robot, file=sys.stderr)
    print(my_samples, file=sys.stderr)
    print(nb_wait, file=sys.stderr)
    print(liste_expertise_to_improve, file=sys.stderr)
    
    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)
    
    my_sample_most_completed = getMySampleMostCompleted(my_samples, my_robot)
    my_sample_can_be_completed = getSampleCanBeCompleted(my_samples, my_robot, molecules_availables)
    
    nb_molecules_carry_by_my_robot = my_robot['storage_a'] + my_robot['storage_b'] + my_robot['storage_c'] + my_robot['storage_d'] + my_robot['storage_e']
    
    
    
    # GO TO MOLECULES pour recolte initiale de molecules
    
    
    if my_robot['target'] == 'START_POS':
        print("GOTO MOLECULES")  
    elif  my_robot['eta'] > 0:
        print(f"ETA {my_robot['eta']} ")
    elif my_robot['target'] == 'MOLECULES' and len(my_samples) == 0 and my_robot['storage_a'] < 1 and available_a > 0 and nb_molecules_carry_by_my_robot < 10:
    #elif my_robot['target'] == 'MOLECULES' and nb_molecules_carry_by_my_robot < 10 and my_robot['storage_a'] < 2 and available_a > 0 and nb_molecules_carry_by_my_robot < 10:    
         print("CONNECT A")  
    elif my_robot['target'] == 'MOLECULES' and len(my_samples) == 0 and my_robot['storage_b'] < 0 and available_b > 0 and nb_molecules_carry_by_my_robot < 10:
    #elif my_robot['target'] == 'MOLECULES' and nb_molecules_carry_by_my_robot < 10 and my_robot['storage_b'] < 1 and available_b > 0 and nb_molecules_carry_by_my_robot < 10:
         print("CONNECT B")  
    elif my_robot['target'] == 'MOLECULES' and len(my_samples) == 0 and my_robot['storage_c'] < 5 and available_c > 0 and nb_molecules_carry_by_my_robot < 10:
    #elif my_robot['target'] == 'MOLECULES' and nb_molecules_carry_by_my_robot < 10 and my_robot['storage_c'] < 3 and available_c > 0 and nb_molecules_carry_by_my_robot < 10:
         print("CONNECT C")  
    elif my_robot['target'] == 'MOLECULES' and len(my_samples) == 0 and my_robot['storage_d'] < 0 and available_d > 0 and nb_molecules_carry_by_my_robot < 10:
    #elif my_robot['target'] == 'MOLECULES' and nb_molecules_carry_by_my_robot < 10 and my_robot['storage_d'] < 2 and available_d > 0 and nb_molecules_carry_by_my_robot < 10:
         print("CONNECT D")  
    elif my_robot['target'] == 'MOLECULES' and len(my_samples) == 0 and my_robot['storage_e'] < 0 and available_e > 0 and nb_molecules_carry_by_my_robot < 10:
    #elif my_robot['target'] == 'MOLECULES' and nb_molecules_carry_by_my_robot < 10 and my_robot['storage_e'] < 2 and available_e > 0 and nb_molecules_carry_by_my_robot < 10:
         print("CONNECT E") 
    elif my_robot['target'] == 'MOLECULES' and len(my_samples) == 0:
        print("GOTO SAMPLES")
    elif my_robot['target'] == 'SAMPLES' and len(my_samples) == 3:
        print("GOTO DIAGNOSIS")
    elif my_robot['target'] == 'SAMPLES':
        print(f"CONNECT {my_rank_sample_chosen}")   
    elif my_robot['target'] == 'DIAGNOSIS' and len(my_samples) > 0 and (isSampleCannotBeCompleted(my_samples, my_robot, molecules_availables) ) and robots[1]['target'] != 'LABORATORY':
        last_index = len(my_samples) -1
        print(f"CONNECT {my_samples[last_index]['sample_id']}")  
    elif my_robot['target'] == 'DIAGNOSIS' and len(my_samples) > 0 and my_samples[0]['health'] == -1:
        print(f"CONNECT {my_samples[0]['sample_id']}")  
    elif my_robot['target'] == 'DIAGNOSIS' and len(my_samples) > 1 and my_samples[1]['health'] == -1:
        print(f"CONNECT {my_samples[1]['sample_id']}")  
    elif my_robot['target'] == 'DIAGNOSIS' and len(my_samples) > 2 and my_samples[2]['health'] == -1:
        print(f"CONNECT {my_samples[2]['sample_id']}")  
    elif my_robot['target'] == 'DIAGNOSIS' and len(my_samples) < 1: 
        print("GOTO SAMPLES") 
    elif my_robot['target'] == 'DIAGNOSIS' and len(my_samples) > 2 and expertise_of_my_robot < 6 and isMySamplesNotGoodForMyRobotExpertise(my_samples, my_robot): 
        sample_not_good_for_my_robot_expertise = getSampleNotGoodForMyRobotExpertise(my_samples, my_robot)
        print(f"CONNECT {sample_not_good_for_my_robot_expertise['sample_id']}")  
    elif my_robot['target'] == 'DIAGNOSIS' and isSampleInMySamplesIsComplete(my_samples, my_robot): 
        print("GOTO LABORATORY")
    elif my_robot['target'] == 'DIAGNOSIS': 
        print("GOTO MOLECULES")
    elif my_robot['target'] == 'MOLECULES' and available_a > 0 and nb_molecules_carry_by_my_robot < 10 and isMissingStorageAForCompleteSample(my_sample_most_completed, my_robot):
        print("CONNECT A")  
    elif my_robot['target'] == 'MOLECULES' and available_b > 0 and nb_molecules_carry_by_my_robot < 10 and isMissingStorageBForCompleteSample(my_sample_most_completed, my_robot):
        print("CONNECT B")  
    elif my_robot['target'] == 'MOLECULES' and available_c > 0 and nb_molecules_carry_by_my_robot < 10 and isMissingStorageCForCompleteSample(my_sample_most_completed, my_robot):
        print("CONNECT C")  
    elif my_robot['target'] == 'MOLECULES' and available_d > 0 and nb_molecules_carry_by_my_robot < 10 and isMissingStorageDForCompleteSample(my_sample_most_completed, my_robot):
        print("CONNECT D")  
    elif my_robot['target'] == 'MOLECULES' and available_e > 0 and nb_molecules_carry_by_my_robot < 10 and isMissingStorageEForCompleteSample(my_sample_most_completed, my_robot):
        print("CONNECT E")  
    elif my_robot['target'] == 'MOLECULES' and available_a > 0 and nb_molecules_carry_by_my_robot < 10 and my_sample_can_be_completed and isMissingStorageAForCompleteSample(my_sample_can_be_completed, my_robot):
        print("CONNECT A")  
    elif my_robot['target'] == 'MOLECULES' and available_b > 0 and nb_molecules_carry_by_my_robot < 10 and my_sample_can_be_completed and isMissingStorageBForCompleteSample(my_sample_can_be_completed, my_robot):
        print("CONNECT B")  
    elif my_robot['target'] == 'MOLECULES' and available_c > 0 and nb_molecules_carry_by_my_robot < 10 and my_sample_can_be_completed and isMissingStorageCForCompleteSample(my_sample_can_be_completed, my_robot):
        print("CONNECT C")  
    elif my_robot['target'] == 'MOLECULES' and available_d > 0 and nb_molecules_carry_by_my_robot < 10 and my_sample_can_be_completed and isMissingStorageDForCompleteSample(my_sample_can_be_completed, my_robot):
        print("CONNECT D")  
    elif my_robot['target'] == 'MOLECULES' and available_e > 0 and nb_molecules_carry_by_my_robot < 10 and my_sample_can_be_completed and isMissingStorageEForCompleteSample(my_sample_can_be_completed, my_robot):
        print("CONNECT E")                  
    #elif my_robot['target'] == 'MOLECULES' and isSampleInMySamplesIsComplete(my_samples, my_robot) and nb_molecules_carry_by_my_robot < 10 and available_c > 0: 
        #print("CONNECT C")                  
    elif my_robot['target'] == 'MOLECULES' and isSampleInMySamplesIsComplete(my_samples, my_robot): 
        print("GOTO LABORATORY")
    elif my_robot['target'] == 'MOLECULES' and nb_wait < 4:
        nb_wait += 1
        print("WAIT")
    elif my_robot['target'] == 'MOLECULES' and robots[1]['target'] != 'LABORATORY':
        nb_wait = 0
        print("GOTO DIAGNOSIS")    
    elif my_robot['target'] == 'LABORATORY' and isSampleInMySamplesIsComplete(my_samples, my_robot):
        print(f"CONNECT {getSampleIsComplete(my_samples, my_robot)['sample_id']}")  
    elif my_robot['target'] == 'LABORATORY' and len(my_samples) > 0:
        print("GOTO MOLECULES")  
    else:
        print("GOTO SAMPLES")

    