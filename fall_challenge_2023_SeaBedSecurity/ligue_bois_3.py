import sys
import math

# Score points by scanning valuable fish faster than your opponent.

creature_count = int(input())
for i in range(creature_count):
    creature_id, color, _type = [int(j) for j in input().split()]

index_move = 0

# game loop
while True:
    my_score = int(input())
    foe_score = int(input())
    my_scan_count = int(input())
    for i in range(my_scan_count):
        creature_id = int(input())
    foe_scan_count = int(input())
    for i in range(foe_scan_count):
        creature_id = int(input())
    my_drone_count = int(input())
    for i in range(my_drone_count):
        my_drone_id, my_drone_x, my_drone_y, my_emergency, my_battery = [int(j) for j in input().split()]
    foe_drone_count = int(input())
    for i in range(foe_drone_count):
        drone_id, drone_x, drone_y, emergency, battery = [int(j) for j in input().split()]
    drone_scan_count = int(input())
    for i in range(drone_scan_count):
        drone_id, creature_id = [int(j) for j in input().split()]
    visible_creature_count = int(input())
    for i in range(visible_creature_count):
        creature_id, creature_x, creature_y, creature_vx, creature_vy = [int(j) for j in input().split()]
    radar_blip_count = int(input())
    for i in range(radar_blip_count):
        inputs = input().split()
        drone_id = int(inputs[0])
        creature_id = int(inputs[1])
        radar = inputs[2]

    
    for i in range(my_drone_count):

        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr, flush=True)

        # MOVE <x> <y> <light (1|0)> | WAIT <light (1|0)>
        #print("WAIT 1")

        if index_move == 0:
            print("MOVE 1500 3500 1")
            if my_drone_x == 1500 and my_drone_y == 3500:
                print("position 0 atteinte", file=sys.stderr, flush=True)
                index_move = 1
        elif index_move == 1:
            #print("MOVE 8000 5000 1")
            print("WAIT 1")
            if my_drone_x == 1500 and my_drone_y >= 9000:
                index_move = 2
        elif index_move == 2:
            print("MOVE 9000 9000 1")
            if my_drone_x == 9000 and my_drone_y == 9000:
                index_move = 3
        elif index_move == 3:
            print("MOVE 2000 6000 1")
            if my_drone_x == 2000 and my_drone_y == 6000:
                index_move = 4
        elif index_move == 4:
            print("MOVE 8700 3500 1")
            if my_drone_x == 8700 and my_drone_y == 3500:
                index_move = 0
        else:
            print("WAIT 1")
