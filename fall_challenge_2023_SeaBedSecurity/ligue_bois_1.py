import sys
import math

# Score points by scanning valuable fish faster than your opponent.


drones_path = [
    [
        {'x': 1500, 'y': 3500},
        {'x': 1500, 'y': 9000},
        {'x': 9000, 'y': 9000},
        {'x': 2000, 'y': 6000},
        {'x': 8700, 'y': 3500},
        {'x': 8700, 'y': 500}
    ],
    [
        {'x': 8500, 'y': 3500},
        {'x': 6000, 'y': 4500},
        {'x': 3000, 'y': 6000},
        {'x': 7500, 'y': 7000},
        {'x': 2000, 'y': 5000},
        {'x': 2000, 'y': 500}
    ]
]

drones_index_move = [0, 0]

creature_count = int(input())
for i in range(creature_count):
    creature_id, color, _type = [int(j) for j in input().split()]


# game loop
while True:
    my_drones = []
    my_score = int(input())
    foe_score = int(input())
    my_scan_count = int(input())
    for i in range(my_scan_count):
        creature_id = int(input())
    foe_scan_count = int(input())
    for i in range(foe_scan_count):
        creature_id = int(input())
    my_drone_count = int(input())
    for i in range(my_drone_count):
        my_drone_id, my_drone_x, my_drone_y, my_emergency, my_battery = [int(j) for j in input().split()]
        my_drones.append({'my_drone_id': my_drone_id, 'my_drone_x': my_drone_x, 'my_drone_y': my_drone_y, 'my_emergency': my_emergency, 'my_battery': my_battery})
    foe_drone_count = int(input())
    for i in range(foe_drone_count):
        drone_id, drone_x, drone_y, emergency, battery = [int(j) for j in input().split()]
    drone_scan_count = int(input())
    for i in range(drone_scan_count):
        drone_id, creature_id = [int(j) for j in input().split()]
    visible_creature_count = int(input())
    for i in range(visible_creature_count):
        creature_id, creature_x, creature_y, creature_vx, creature_vy = [int(j) for j in input().split()]
    radar_blip_count = int(input())
    for i in range(radar_blip_count):
        inputs = input().split()
        drone_id = int(inputs[0])
        creature_id = int(inputs[1])
        radar = inputs[2]

    
    for i in range(my_drone_count):

        # Write an action using print
        # To debug: print("Debug messages...", file=sys.stderr, flush=True)

        # MOVE <x> <y> <light (1|0)> | WAIT <light (1|0)>
        #print("WAIT 1")

        index_move = drones_index_move[i]
        print(f"MOVE {drones_path[i][index_move]['x']} {drones_path[i][index_move]['y']} 1")
        if my_drones[i]['my_drone_x'] == drones_path[i][index_move]['x'] and my_drones[i]['my_drone_y'] == drones_path[i][index_move]['y']:
            if drones_index_move[i] < (len(drones_path[i]) - 1):
                drones_index_move[i] += 1
            else: 
                drones_index_move[i] = 0