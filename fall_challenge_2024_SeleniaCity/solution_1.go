package main

import "fmt"
import "os"
import "bufio"

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

func main() {
    scanner := bufio.NewScanner(os.Stdin)
    scanner.Buffer(make([]byte, 1000000), 1000000)

    for {
        var resources int
        scanner.Scan()
        fmt.Sscan(scanner.Text(),&resources)
        
        var numTravelRoutes int
        scanner.Scan()
        fmt.Sscan(scanner.Text(),&numTravelRoutes)
        
        for i := 0; i < numTravelRoutes; i++ {
            var buildingId1, buildingId2, capacity int
            scanner.Scan()
            fmt.Sscan(scanner.Text(),&buildingId1, &buildingId2, &capacity)
        }
        var numPods int
        scanner.Scan()
        fmt.Sscan(scanner.Text(),&numPods)
        
        for i := 0; i < numPods; i++ {
            scanner.Scan()
            podProperties := scanner.Text()
            _ = podProperties // to avoid unused error
        }
        var numNewBuildings int
        scanner.Scan()
        fmt.Sscan(scanner.Text(),&numNewBuildings)
        for i := 0; i < numNewBuildings; i++ {
            scanner.Scan()
            buildingProperties := scanner.Text()
            _ = buildingProperties // to avoid unused error
        }
        
        // fmt.Fprintln(os.Stderr, "Debug messages...")
        fmt.Println("TUBE 0 1;TUBE 0 2;POD 42 0 1 0 2 0 1 0 2") // TUBE | UPGRADE | TELEPORT | POD | DESTROY | WAIT
    }
}